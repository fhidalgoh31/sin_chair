/***************************************************
  This sketch is derived from an example sketch for the Adafruit 1.8" SPI display.
  Written by Limor Fried/Ladyada for Adafruit Industries.
  MIT license, all text above must be included in any redistribution

  Written 2013 by L.G. Robbins for the display node of a multi-node CANbus
  system.
 
  Re-written to use SDfat library because it is less memory-demanding
 ****************************************************/
/*
NOTES:

  There are various sections of code that are for DEMO or TEST purposes
  only.  To fit in NANO with Nano bootloader, at least one of them MUST be
  commented out.  NANO with Uno bootloader has ca. 2k more PROGMEM
  and can tolerate having some of these.

ChangeVoltMeter is called in DisplayMsg and DisplayBaseScreen as well as when _DVOLT msg. is received.
May be source of premature painting during startup of Master (i.e. when please wait message recieved).
*/

#include <DS3231.h>
DS3231 Clock;
#include <Wire.h>
#include  <avr/pgmspace.h> // PROGMEM (flash) handling
#include <TFT.h>
#include <SPI.h>
#include <SdFat.h>
SdFat sd;
#include <CAN.h>
#include <avr/sleep.h>
#include <constants.h>
#include "Defines.h"
#include <MsgIDs.h>
#include "Globals.h"

#include <EEPROM.h>

// using the hardware SPI pins
Adafruit_ST7735 tft = Adafruit_ST7735(cs, dc, rst);

// tables for rotating Speed Pot pointer
// double check these value by re-doing the calculations in QuattroPro
const byte SIN[] PROGMEM = {179, 187, 195, 203, 210, 216, 222, 228, 
           233, 237, 241, 244, 247, 250, 251, 253, 253, 253, 253, 
           252, 250, 248, 246, 242, 238, 234, 229, 224, 218, 212, 
           205, 198, 190, 182, 173, 164, 155, 146, 136, 125, 115, 
           104, 93, 82, 70, 59, 47, 35, 23, 11, 0};
const byte COS[] PROGMEM = {179, 170, 161, 152, 142, 132, 122, 111, 
           100, 89, 78, 67, 55, 43, 31, 19, 7, 3, 15, 27, 39, 51, 
           63, 74, 86, 97, 108, 118, 129, 139, 149, 158, 167, 176, 
           185, 193, 200, 207, 214, 220, 226, 231, 236, 240, 243, 
           246, 249, 251, 252, 253, 254};
unsigned long SendFrame_id;
byte currentYear, currentMonth;

// warning and information messages in Constants.h
// there are ca. 42 CAN messages, some with multiple values

void setup (void) {
  pinMode (PowerOnPin, INPUT);
  pinMode (XcvrRxDPin, INPUT);
  pinMode (XcvrSleepPin, INPUT);
  pinMode (BKLT, OUTPUT);
  byte Backlight;
  Backlight = ScreenBright;
  if (Backlight == 0) {
    digitalWrite (XcvrSleepPin, LOW);
  }
  analogWrite (BKLT, ScreenBright);
  SETUP_CAN ();
  DisplayStart[0] = 1;
  DisplayStart[1] = ~DisplayStart[0];
  for (byte i = 1; i<=10; i++){
    SEND_MESSAGE (2, NodeLive_id, DisplayStart);
    delay (10);
  }
  // If your TFT's plastic wrap has a Black Tab, use the following:
  tft.initR(INITR_BLACKTAB);   // initialize a ST7735S chip, black tab
  // If your TFT's plastic wrap has a Red Tab, use the following:
  //tft.initR(INITR_REDTAB);   // initialize a ST7735R chip, red tab
  // If your TFT's plastic wrap has a Green Tab, use the following:
  //tft.initR(INITR_GREENTAB); // initialize a ST7735R chip, green tab
  sd.begin(SD_CS, SPI_FULL_SPEED); //Adafruit example uses half-speed, but
                                   // no problems found with full speed.
  tft.setRotation (Landscape);
  tft.fillScreen(Background);
// for placement of graphics
  CtrX = tft.width()/2;
  CtrY = tft.height()/2;
// for Speed Pot graphic
// 2 pixels removed to keep from erasing arc
  PtrTriangleApexRadius = (CtrY*28)/73-3;
  PtrTriangleBase = charheight;
  PtrTriangleBaseRadius = (CtrY*7)/26-1;
  DisplayMsg (paint, _STARTUP);
  LastMode = None;
// Serial commands removed to reduce program size; with use of SdFat
// library, there is enough PROGMEM available to use serial sparingly 
// for debugging.
// Serial.begin (115200);
//  Serial.println(__FILE__ " - " __DATE__ " " __TIME__);
//xTestAndDemo ();
// no analog inputs used in Display, so turn it off
  ADCSRA = 0;
// ByteDots (0b10101010); // use this as needed to use dots to read contents of a byte
} // end setup

void loop (void) {
  static boolean EEPROMlogged = false;
  static boolean ErrorLogged = false;
  static boolean MOTOPENlogged = false;
  static boolean SensorFaultMsgReceived = false;
  byte PotValue;
  if (XcvrSleep){
    boolean RxD = digitalRead (XcvrRxDPin);
    if (!RxD){
      detachInterrupt (1);
      digitalWrite (XcvrSleepPin, LOW);
      pinMode (XcvrSleepPin, INPUT);
      SETUP_CAN ();
      XcvrSleep = false;
// DO ANYTHING HERE THAT NEEDS TO BE DONE WHEN AWAKENING FROM SLEEP
// _XCVR (wake) message may not be seen when awakened by one-shot msg
      if (ScreenBright == 0) {
        digitalWrite (XcvrSleepPin, LOW);
      }
      else {
        analogWrite (BKLT, ScreenBright);
      }
      //added in case Master has re-started
      for (byte i = 1; i<=10; i++){
        SEND_MESSAGE (2, NodeLive_id, DisplayStart);
        delay (10);
      }
    }
  } // was sleeping
  else {
    RECEIVE_MESSAGE ();
    static boolean ChargerShown = false;
    if (MsgReceived){
      PARSE_ID ();
      if (Target == toDisplay){
        if (ScreenBright != 0){
          analogWrite (BKLT, ScreenBright);
        }
        else {
          digitalWrite (BKLT, LOW);
        }
        if (Category == SetCommand){
// handle error and out-of-neutral messages
          if ((Name > _BRAKESET)||(Name==_MOTOPEN)){
         // persistant messages that don't get erased until system reset
            if (Name == _EEPROM){
              if (!EEPROMlogged){
                LogMsg (Name);
                EEPROMlogged = true;
              }
            }
            else if (Name == _MOTOPEN){
              if (!MOTOPENlogged){
                LogMsg (Name);
                MOTOPENlogged = true;
              }
            }
            else if (Name == _SENSORFLT) {
              if (!SensorFaultMsgReceived) {
                SensorFaultMsgReceived = true;
                LogMsg (Name);
              }
            }
            else {
              LogMsg (Name);
            }
            if ((Name == _MOTOPEN)&&(frame_data[0] == 3)){
              DisplayMsg (erase, Name);
              MOTOPENlogged = false;
            }
            else {
              DisplayMsg (paint, Name); // no bmp for _SFLAG
                  // because status messages are not faults
                  // and are only logged
            }
          }
          else if ((Name==_BRKOPEN)||(Name==_BRKSHRT)||(Name==_CONTACT)||(Name==_SWCLOSED)){
        // erase these messages if fault no longer present
            if (frame_data[0] != 0){
              DisplayMsg (paint, Name);
              if (!ErrorLogged){
                LogMsg (Name);
                ErrorLogged = true;
              }
            }
            else if (frame_data[0] == 0){
              DisplayMsg (erase, Name);
              ErrorLogged = false;
            }
          }
          else if (Name==_CHARGER){ // paint message just once, erase once resolved
            if (frame_data[0] != 0){
              if (!ChargerShown){
                ChargerShown = true;
                LogMsg (Name);
                DisplayMsg (paint, Name);
              }
            }
            else if (frame_data[0] == 0){
              DisplayMsg (erase, Name);
            }
          }
          else if (Name == _OON){ // not a fault: display but do not log
            if (frame_data[0] == 1){
              DisplayMsg (paint, Name);
            }
            else if (frame_data[0] == 0){
              DisplayMsg (erase, Name);
            }
          }
    // handle all other SetCommand messages
          else {
            switch (Name){
              byte SeatMove;
              byte LightsFlags;
              case _STARTUP:
                if (!StartupLogged){
                  StartupLogged = true;
                  LogMsg (Name);
                  byte Logged[2];
                  Logged[0] = 4;
                  Logged[1] = ~Logged[0];
                  for (byte i = 1; i<=10; i++){
                    SEND_MESSAGE (2, NodeLive_id, Logged);
                    delay (10);
                  }
                }
                break;
              case _AHR:
                AHr = frame_data[0];
                if (AHr != LastAHr){
                  ChangeAHrMeter (AHr);
                }
                break;
              case _DVOLTS:
                Volts = frame_data[0];
                if (Volts != LastVolts){
                  ChangeVoltMeter (Volts);
                }
                break;
              case _RAWVOLTS:
                InVolts = (int)(frame_data[0]<<8) | frame_data[1];
                OutVolts = (int)(frame_data[2]<<8) | frame_data[3];
                break;
              case _MODE:
                UsrToggle = frame_data[0]>>3 & 1;
                Mode = frame_data[0] & 0b111;
                if ((UsrToggle != LastUsrToggle) || (Mode != LastMode)){
                  PaintBaseScreen ();
                }
                break;
              case _SPEEDPOT:
                PotValue = frame_data[0];
                if (PotValue != LastSpeedPot){
                  SetSpeedPot (PotValue);
                }
                break;
              case _SETSLOW:
                SetSlow = frame_data[0];
                if (SetSlow != LastSetSlow){
                  DisplaySetSlowMsg (SetSlow);
                  LastSetSlow = SetSlow;
                }
                break;
              case _RUNTIME:
                RunHours = (unsigned long)(frame_data[0]<<16 | frame_data[1]<<8 | frame_data[2]);
                RunMinutes = frame_data[3];
                break;
              case _TOTTIME:
                TotalHours = (unsigned long)(frame_data[0]<<16 | frame_data[1]<<8 | frame_data[2]);
                TotalMinutes = frame_data[3];
                break;

// ************* DEMO ****************************************
// cases _JOYSTICK, _LIGHTS & _SEAT for DEMO or testing purposes 
// only - COMMENT OUT BEFORE USE
/*
              case _JOYSTICK:
                Throttle = frame_data[0];
                Steering = frame_data[1];
                PaintJoystickMarker (Throttle, Steering);
                break;
*/
// _LIGHTS case commented out because now have SS switches and LEDs
// working
/*
              case _LIGHTS:
                LightsFlags = frame_data[0];
                PaintLightsMarker (LightsFlags);
                break;
*/
/*
              case _SEAT:
                SeatMove = frame_data[0];
                PaintSeatMarker (SeatMove);
                break;
*/
// cases _JOYSTICK, _LIGHTS & _SEAT for demo or testing purposes 
// only - COMMENT OUT BEFORE USE
// *************** DEMO **************************************

            } // end switch
          } // end else { // handle all other SetCommand messages
        } // end SetCommand section
        else if (Category == GetValue) {
        } // end GetValue section
        else if (Category == Config){
          if (Name == _LOG){
            if (frame_data[0] == 1){
              byte Month = frame_data[1];
              int Year = (int)(frame_data[2]<<8|frame_data[3]);
              SEND_LogFile (Month, Year);
            }
          }
        else if (Name == _XCVR){
            XCVR ();
          }
        } // end of Config section
        else if (Category == Config2){
          Name = Name + 63;
        } // end of Config2 section
      } // Target = toDisplay
    } // MsgReceived == true
  } // end else (i.e. !XCVRSleep)
} // end loop

void SpPotPointer (boolean action){ // draw or erase speed pot pointer
  if (action == paint){
    tft.fillTriangle(TrApX, TrApY, TrLtX, TrLtY, TrRtX, TrRtY, ST7735_BLACK);
  }
  else { // erase
    tft.fillTriangle(TrApX, TrApY, TrLtX, TrLtY, TrRtX, TrRtY, Background);
  }
} // end SpPotPointer

void RotateSpeedPot (void){ // animate speed pot for testing or demo
  for (byte Setting = 0; Setting <= 100; Setting = Setting + 1){
    SetSpeedPot (Setting);
    delay (20);
  }
  for (byte Setting = 99; Setting >= 50; Setting = Setting - 1){
    SetSpeedPot (Setting);
    delay (20);
  }
} // end RotateSpeedPot

void SetSpeedPot (byte PotValue){ // rotate SpeedPot to PotValue
  SpPotPointer (erase);
  LastSpeedPot = PotValue;
  byte index = PotValue;
  if (PotValue > 50){
    index = 100 - PotValue;
  }
  byte dX = pgm_read_byte_near(SIN + index);
  byte dY =pgm_read_byte_near(COS + index);
  if (PotValue < 17){
    TrApX = CtrX - (PtrTriangleApexRadius*dX)/254;
    TrApY = CtrY + (PtrTriangleApexRadius*dY)/254;
    TrBaseX = CtrX - (PtrTriangleBaseRadius*dX)/254;
    TrBaseY = CtrY + (PtrTriangleBaseRadius*dY)/254;
    TrLtX = TrBaseX + (PtrTriangleBase/2*dY)/254;
    TrLtY = TrBaseY + (PtrTriangleBase/2*dX)/254;
    TrRtX = TrBaseX - (PtrTriangleBase/2*dY)/254;
    TrRtY = TrBaseY - (PtrTriangleBase/2*dX)/254;
  }
  if ((PotValue >= 17) && (PotValue <=50)){
    TrApX = CtrX - (PtrTriangleApexRadius*dX)/254;
    TrApY = CtrY - (PtrTriangleApexRadius*dY)/254;
    TrBaseX = CtrX - (PtrTriangleBaseRadius*dX)/254;
    TrBaseY = CtrY - (PtrTriangleBaseRadius*dY)/254;
    TrLtX = TrBaseX - (PtrTriangleBase/2*dY)/254;
    TrLtY = TrBaseY + (PtrTriangleBase/2*dX)/254;
    TrRtX = TrBaseX + (PtrTriangleBase/2*dY)/254;
    TrRtY = TrBaseY - (PtrTriangleBase/2*dX)/254;
  }
  if ((PotValue > 50)&& (PotValue <=83)){
    TrApX = CtrX + (PtrTriangleApexRadius*dX)/254;
    TrApY = CtrY - (PtrTriangleApexRadius*dY)/254;
    TrBaseX = CtrX + (PtrTriangleBaseRadius*dX)/254;
    TrBaseY = CtrY - (PtrTriangleBaseRadius*dY)/254;
    TrLtX = TrBaseX - (PtrTriangleBase/2*dY)/254;
    TrLtY = TrBaseY - (PtrTriangleBase/2*dX)/254;
    TrRtX = TrBaseX + (PtrTriangleBase/2*dY)/254;
    TrRtY = TrBaseY + (PtrTriangleBase/2*dX)/254;
  }
  if (PotValue > 83){
    TrApX = CtrX + (PtrTriangleApexRadius*dX)/254;
    TrApY = CtrY + (PtrTriangleApexRadius*dY)/254;
    TrBaseX = CtrX + (PtrTriangleBaseRadius*dX)/254;
    TrBaseY = CtrY + (PtrTriangleBaseRadius*dY)/254;
    TrLtX = TrBaseX - (PtrTriangleBase/2*dY)/254;
    TrLtY = TrBaseY + (PtrTriangleBase/2*dX)/254;
    TrRtX = TrBaseX + (PtrTriangleBase/2*dY)/254;
    TrRtY = TrBaseY - (PtrTriangleBase/2*dX)/254;
  }
  if ((UsrToggle==Attendant)&&((Mode==Drive1)||(Mode==Drive2)||(Mode==Drive3))){
    SpPotPointer (paint);
  }
} // end SetSpeedPot

void ExerciseMeters (void){ // animate meters for testing or demo
  for (int Volts=110; Volts >= 0; Volts = Volts-2){
    ChangeVoltMeter (Volts);
    delay (50);
    if (Volts > 100){
      delay (100);
    }
  }
  for (Volts=0; Volts <= 100; Volts = Volts+2){
    ChangeVoltMeter (Volts);
    delay (50);
  }
  for (int AHr=100; AHr >= 0; AHr = AHr-2){
    ChangeAHrMeter (AHr);
    delay (50);
  }
  for (AHr=0; AHr <= 100; AHr = AHr+2){
    ChangeAHrMeter (AHr);
    delay (50);
  }
} // end ExerciseMeters

void ChangeVoltMeter (byte Volts){
  boolean OverVolt = false;
  LastVolts = Volts;
  if (Volts > 140){ // 25.5 V if VoltsFull = 24.9 V, 25.6 V if VoltsFull = 25.0 V
    OverVolt = true;
  }
  Volts = constrain(Volts,0,100);
  int YellowBarY = BarY+BarHeight-1;
  int RedBarY = BarY+2*BarHeight-2;
  byte dVolts = map (Volts, 0, 100, 0, 120);
  tft.fillRect(0, BarY, BarWidth, 3*BarHeight, Background);
  if (dVolts <= BarHeight){ // in red zone
    tft.fillRect(0, RedBarY+(BarHeight-dVolts), BarWidth, dVolts, ST7735_RED);
  }
  else if ((dVolts > BarHeight) && (dVolts <= 2*BarHeight)){
    tft.fillRect(0, RedBarY, BarWidth, BarHeight, ST7735_RED);
    byte YellowSize = dVolts-BarHeight;
    tft.fillRect(0, YellowBarY + (BarHeight-YellowSize), BarWidth, YellowSize, ST7735_YELLOW);
  }
  else if (dVolts > 2*BarHeight){
    tft.fillRect(0, RedBarY, BarWidth, BarHeight, ST7735_RED);
    tft.fillRect(0, YellowBarY, BarWidth, BarHeight, ST7735_YELLOW);
    byte GreenSize = dVolts-2*BarHeight;
    tft.fillRect (0, BarY+(BarHeight-GreenSize), BarWidth, GreenSize, ST7735_GREEN);
  }
  if (OverVolt){
    bmpDraw("excl.bmp", 0, BarY);
  }
  tft.drawRect(0, BarY, BarWidth, 3*BarHeight, ST7735_BLACK);
} // end ChangeVoltMeter

void ChangeAHrMeter (byte AHr){
  int AHrBarX = tft.width()-BarWidth; 
  int YellowBarY = BarY+BarHeight-1;
  int RedBarY = BarY+2*BarHeight-2;
  LastAHr = AHr;
  AHr = constrain(AHr,0,100);
  byte dAHr = map (AHr, 0, 100, 0, 120);
  tft.fillRect(AHrBarX, BarY, BarWidth, 3*BarHeight, Background);
  if (dAHr <= BarHeight){ // in red zone
    tft.fillRect(AHrBarX, RedBarY+(BarHeight-dAHr), BarWidth, dAHr, ST7735_RED);
  }
  else if ((dAHr > BarHeight) && (dAHr <= 2*BarHeight)){
    tft.fillRect(AHrBarX, RedBarY, BarWidth, BarHeight, ST7735_RED);
    byte YellowSize = dAHr-BarHeight;
    tft.fillRect(AHrBarX, YellowBarY + (BarHeight-YellowSize), BarWidth, YellowSize, ST7735_YELLOW);
  }
  else if (dAHr > 2*BarHeight){
    tft.fillRect(AHrBarX, RedBarY, BarWidth, BarHeight, ST7735_RED);
    tft.fillRect(AHrBarX, YellowBarY, BarWidth, BarHeight, ST7735_YELLOW);
    byte GreenSize = dAHr-2*BarHeight;
    tft.fillRect (AHrBarX, BarY+(BarHeight-GreenSize), BarWidth, GreenSize, ST7735_GREEN);
  }
  tft.drawRect(AHrBarX, BarY, BarWidth, 3*BarHeight, ST7735_BLACK);
} // end ChangeAHrMeter

void DisplayMsg (boolean action, byte message){ // paint messages
  char FileName[13];
  if ((message >= 30) || (message == 0)){ // single-option messages
    snprintf (FileName, 13, "msg_%u.bmp", message);
  }
  else { // multi-option messages
    snprintf (FileName, 13, "msg_%u_%u.bmp", message, frame_data[0]);
  }
  if (action == paint) {
    bmpDraw(FileName, 0, 0);
  }
  else { // action == erase
    LastMode = None;
    PaintBaseScreen ();
    ChangeVoltMeter (LastVolts);
    ChangeAHrMeter (LastAHr);
  }
} // end DisplayMsg

void PaintJoystickMarker (byte Throttle, byte Steering){
  static byte lastThrottle = 50;
  static byte lastSteering = 50;
  int Y = CtrY + 50 - lastThrottle;
  int X = CtrX - 50 + lastSteering;
  tft.fillCircle (X, Y, 3, Background);
  Y = CtrY + 50 - Throttle;
  X = CtrX - 50 + Steering;
  tft.fillCircle (X, Y, 3, ST7735_RED);
  lastThrottle = Throttle;
  lastSteering = Steering;
} // end PaintJoystickMarker

void PaintLightsMarker (byte LightsFlags){
  static byte lastLightsFlags = 0b1111;
// bit0=lastHeadLight), bit1=lastFourWay), 
// bit2=lastLeftTurn), bit3=lastRightTurn);
  for (byte i = 0; i <= 3; i++){
    if (bitRead (lastLightsFlags, i) == ON) {
      switch (i) {
        case 0:
          tft.fillCircle (CtrX, CtrY-35, 5, Background);
          break;
        case 1:
          tft.fillCircle (CtrX, CtrY+55, 5, Background);
          break;
        case 2:
          tft.fillCircle (CtrX-50, CtrY+10, 5, Background);
          break;
        case 3:
          tft.fillCircle (CtrX+50, CtrY+10, 5, Background);
          break;
      }
    }
  }
  for (byte i = 0; i <= 3; i++){
    if (bitRead (LightsFlags, i) == ON) {
      switch (i) {
        case 0:
          tft.fillCircle (CtrX, CtrY-35, 5, ST7735_YELLOW);
          tft.drawCircle (CtrX, CtrY-35, 5, ST7735_BLACK);
          break;
        case 1:
          tft.fillCircle (CtrX, CtrY+55, 5, ST7735_YELLOW);
          tft.drawCircle (CtrX, CtrY+55, 5, ST7735_BLACK);
          break;
        case 2:
          tft.fillCircle (CtrX-50, CtrY+10, 5, ST7735_YELLOW);
          tft.drawCircle (CtrX-50, CtrY+10, 5, ST7735_BLACK);
          break;
        case 3:
          tft.fillCircle (CtrX+50, CtrY+10, 5, ST7735_YELLOW);
          tft.drawCircle (CtrX+50, CtrY+10, 5, ST7735_BLACK);
          break;
      }
    }
  }
  lastLightsFlags = LightsFlags;
} // end PaintLightsMarker

void PaintSeatMarker (byte SeatMove){
  static byte lastSeatMove = none;
  switch (lastSeatMove){
    case forward:
      tft.fillCircle (CtrX+50, CtrY, 5, Background);
      break;
    case back:
      tft.fillCircle (CtrX-50, CtrY, 5, Background);
      break;
    case down:
      tft.fillCircle (CtrX, CtrY+50, 5, Background);
      break;
    case up:
      tft.fillCircle (CtrX, CtrY-55, 5, Background);
      break;
  }
  switch (SeatMove){
    case forward:
      tft.fillCircle (CtrX+50, CtrY, 5, ST7735_GREEN);
      break;
    case back:
      tft.fillCircle (CtrX-50, CtrY, 5, ST7735_GREEN);
      break;
    case down:
      tft.fillCircle (CtrX, CtrY+50, 5, ST7735_GREEN);
      break;
    case up:
      tft.fillCircle (CtrX, CtrY-55, 5, ST7735_GREEN);
      break;
  }
  lastSeatMove = SeatMove;
}

void PaintBaseScreen (void){ // paint screen in accord with UsrToggle and Mode
// following will be procedure to respond to UsrToggle and Mode CAN messages
  if ((UsrToggle != LastUsrToggle) || (Mode != LastMode) || (Mode == None)){
    if (Mode == None){
      Mode = LastMode;
    }
    if (UsrToggle == Attendant){
      switch (Mode){
        case Drive1:
          bmpDraw("jsdrive.bmp", 0, 0);
          SetSpeedPot (LastSpeedPot);
          SpPotPointer (paint);
          break;
        case Drive2:
          bmpDraw("jsdrive.bmp", 0, 0);
          SetSpeedPot (LastSpeedPot);
          SpPotPointer (paint);
          break;
        case Drive3:
          bmpDraw("jsdrive.bmp", 0, 0);
          SetSpeedPot (LastSpeedPot);
          SpPotPointer (paint);
          break;
        case Seat:
          bmpDraw("jsseat.bmp", 0, 0);
          break;
        case Lights:
          bmpDraw("jslights.bmp", 0, 0);
          break;
      } // end switch (Mode) in Attendant
    }
    else { // UsrToggle = Driver
      switch (Mode){
        case Drive1:
          bmpDraw("Drive1.bmp", 0, 0);
          break;
        case Drive2:
          bmpDraw("Drive2.bmp", 0, 0);
          break;
        case Drive3:
          bmpDraw("Drive3.bmp", 0, 0);
          break;
        case Seat:
          bmpDraw("swseat.bmp", 0, 0);
          break;
        case Lights:
          bmpDraw("swlights.bmp", 0, 0);
          break;
      } // end switch (Mode) in Driver
    } // end if Attendant else Driver
    LastUsrToggle = UsrToggle;
    LastMode = Mode;
    ChangeVoltMeter (LastVolts);
    ChangeAHrMeter (LastAHr);
    DisplaySetSlowMsg (SetSlow);
  } // end if ((UsrToggle != LastUsrToggle) || (Mode != LastMode))
} // end PaintBaseScreen

// Open and display a Windows Bitmap (BMP) file.
// modified to use SdFat instead of SD.

// uint16_t read16(File f) and uint32_t read32(File f) functions
// that work fine with SD keep reading same 2 bytes under SdFat.
// Passing just the SdFile does not pass bmpFile.curPosition back to bmpDraw.
// Rather than finding and passing curPosition, I just put the appropriate
// bmpFile.read() calls here.  That also avoids calculating and returning
// a result for bytes that are just to be ignored.  
// Mike Stewart has suggested that SdFile is a class (rather than
// structure like FILE) and would work if passed by reference.
void bmpDraw(char *filename, uint8_t x, uint8_t y) {
  SdFile bmpFile;
  int      bmpWidth, bmpHeight;   // W+H in pixels
  uint8_t  bmpDepth;              // Bit depth (currently must be 24)
  uint32_t bmpImageoffset;        // Start of image data in file
  uint32_t rowSize;               // Not always = bmpWidth; may have padding
  uint8_t  sdbuffer[3*BUFFPIXEL]; // pixel buffer (R+G+B per pixel)
  uint8_t  buffidx = sizeof(sdbuffer); // Current position in sdbuffer
  int      row, col;
  uint8_t  r, g, b;
  uint32_t pos = 0;
// added for reading header data with SdFat.h
  byte Data16[2];
  byte Data32[4];
  uint16_t Read16;
  uint32_t Read32;
// WaitForPaint added to make sure bmpDraw is blocking
  while (WaitForPaint == true) {
  }
  WaitForPaint = true;
  if((x >= tft.width()) || (y >= tft.height())) return; // invalid location for graphic
// Open file on SD card
  bmpFile.open(filename, O_READ);
// Parse BMP header
  Data16[0] = bmpFile.read(); // LSB
  Data16[1] = bmpFile.read(); // MSB
  if (((uint16_t)Data16[1]<<8)|Data16[0] == 0x4D42){ // BMP signature
    for (byte i = 1; i <= 4; i++){ // Read & ignore File size
      bmpFile.read();
    }
    for (byte i = 1; i <= 4; i++){ // Read & ignore creator bytes
      bmpFile.read();
    }
    for (byte i = 0; i <= 3; i++){
      Data32[i] = bmpFile.read ();
    }
    bmpImageoffset = ((uint32_t)Data32[3]<<24)|((uint32_t)Data32[2]<<16)|((uint16_t)Data32[1]<<8)|Data32[0];
    for (byte i = 1; i <= 4; i++){ // Read & ignore Header size
      bmpFile.read();
    }
    for (byte i = 0; i <= 3; i++){
      Data32[i] = bmpFile.read ();
    }
    bmpWidth = ((uint32_t)Data32[3]<<24)|((uint32_t)Data32[2]<<16)|((uint16_t)Data32[1]<<8)|Data32[0];
    for (byte i = 0; i <= 3; i++){
      Data32[i] = bmpFile.read ();
    }
    bmpHeight = ((uint32_t)Data32[3]<<24)|((uint32_t)Data32[2]<<16)|((uint16_t)Data32[1]<<8)|Data32[0];
    Data16[0] = bmpFile.read(); // LSB
    Data16[1] = bmpFile.read(); // MSB
    if (((uint16_t)Data16[1]<<8)|Data16[0] == 1){ // # planes -- must be '1'
      Data16[0] = bmpFile.read(); // LSB
      Data16[1] = bmpFile.read(); // MSB
      bmpDepth = ((uint16_t)Data16[1]<<8)|Data16[0];
      for (byte i = 0; i <= 3; i++){
        Data32[i] = bmpFile.read ();
      }
      Read32 = ((uint32_t)Data32[3]<<24)|((uint32_t)Data32[2]<<16)|((uint16_t)Data32[1]<<8)|Data32[0];
      if((bmpDepth == 24) && (Read32 == 0)) { // 0 = uncompressed
// BMP rows are padded (if needed) to 4-byte boundary
        rowSize = (bmpWidth * 3 + 3) & ~3;
// code for top-down order files removed
// code for cropping bmp removed
// Set TFT address window to image bounds
        tft.setAddrWindow(x, y, x+bmpWidth-1, y+bmpHeight-1);
// scan lines
        for (row=0; row<bmpHeight; row++) { // For each scanline...
          pos = bmpImageoffset + (bmpHeight - 1 - row) * rowSize;
          if (bmpFile.curPosition() != pos){ // Need seek?
            bmpFile.seekSet(pos);
            buffidx = sizeof(sdbuffer); // Force buffer reload
          }
          for (col=0; col<bmpWidth; col++) { // For each pixel...
            if (buffidx >= sizeof(sdbuffer)) { // Indeed
              bmpFile.read(sdbuffer, sizeof(sdbuffer));
              buffidx = 0; // Set index to beginning
            }
// Convert pixel from BMP to TFT format, push to display
            b = sdbuffer[buffidx++];
            g = sdbuffer[buffidx++];
            r = sdbuffer[buffidx++];
            tft.pushColor(tft.Color565(r,g,b));
          } // end for each pixel
        } // end for each scanline
      } // end goodBmp
    } // end of check for 1 plane
  } // end if valid location for graphic
  bmpFile.close();
  WaitForPaint = false;
} // end bmpDraw

void SETUP_CAN (void) {
  // initialize CAN bus class
  // this class initializes SPI communications with MCP2515
  CAN.begin();
  CAN.setMode(CONFIGURATION);  // set to "NORMAL" for standard communications
  CAN.baudConfig(BUS_SPEED);
/*
   The lower the ID value, the higher the priority.
   Highest priority messages go first to buffer 0, then to buffer 1
      if buffer 0 is full (depends on MCP2151 register setting)
      or if buffer 0 filter not matched.
   If 2 messages arrive at the same time, higher priority msg goes to
      buffer 0, lower priority msg goes to buffer 1.
  "Category" in 2 msb of ID determines priority of Roboteq message groups: 
      0b00=SetCommand, 0b01=GetValue, 0b10 and 0b11 = configuration.
   Within a catagory, "Name" in next 6 bits determines priority.
   Thus, SetCommand is highest priority group, and _G (motor output setting = 0)
      has absolute highest priority.
*/
/* target = bottom 3 bits
fromAux B000  toAux B001  fromRoboteq B010  toRoboteq B011
fromMaster B100  toMaster B101  fromDisplay B110  toDisplay B111
For DisplayNode set masks and filters to only accept toDisplay messages
*/
  CAN.setMaskOrFilter(MASK_0,   0b00000000, 0b11100000, 0b00000000, 0b00000000);
  CAN.setMaskOrFilter(FILTER_0, 0b00000000, 0b11100000, 0b00000000, 0b00000000); // toDisplay messages
  CAN.setMaskOrFilter(FILTER_1, 0b00000000, 0b11100000, 0b00000000, 0b00000000); // toDisplay messages

  CAN.setMaskOrFilter(MASK_1,   0b00000000, 0b11100000, 0b00000000, 0b00000000); 
  CAN.setMaskOrFilter(FILTER_2, 0b00000000, 0b11100000, 0b00000000, 0b00000000); // toDisplay messages
  CAN.setMaskOrFilter(FILTER_3, 0b00000000, 0b11100000, 0b00000000, 0b00000000); // toDisplay messages
  CAN.setMaskOrFilter(FILTER_4, 0b00000000, 0b11100000, 0b00000000, 0b00000000); // toDisplay messages
  CAN.setMaskOrFilter(FILTER_5, 0b00000000, 0b11100000, 0b00000000, 0b00000000); // toDisplay messages
  CAN.setMode(NORMAL);  // set to "NORMAL" for standard com
} // end SETUP_CAN

void RECEIVE_MESSAGE (void){
  MsgReceived = false;
  byte pairs;
  boolean check = true;
// clear receive buffers, just in case.
  for (byte i = 0; i <= 7; i++){
    frame_data[i] = 0x00;
  }
  ReceiveFrame_id = 0x00;
  rx_status = CAN.readStatus();
  if (MSG_IN_BUFFER0){
    CAN.readDATA_ff_0(&length,frame_data,&ReceiveFrame_id, &ext, &filter);
//    printBuf(rx_status, length, ReceiveFrame_id, filter, 0, frame_data, ext);
    pairs = length/2;
    for (byte j=0; j<=(pairs-1); j++){
      if ((frame_data[j] ^ frame_data[j+pairs]) != 0xFF){
        check = false;
        break;
      }
    }
  }
/* in original code from which I lifted this, the following was
"if" rather than "else if".  Doing that means that a lower-priority
(buffer 1) message could replace a higher priority (buffer 0) message
if both buffers were full.  Using "else if" will cause buffer 1 to be
read on next pass if another buffer 0 message has not arrived in the
meantime.
*/
  else if (MSG_IN_BUFFER1){
    CAN.readDATA_ff_1(&length,frame_data,&ReceiveFrame_id, &ext, &filter);
//    printBuf(rx_status, length, ReceiveFrame_id, filter, 1, frame_data, ext); 
    pairs = length/2;
    for (byte j=0; j<=(pairs-1); j++){
      if ((frame_data[j] ^ frame_data[j+pairs]) != 0xFF){
        check = false;
        break;
      }
    }
  }
  if ((MSG_IN_BUFFER0) || (MSG_IN_BUFFER1)){
    if (check){
      MsgReceived = true;
    } // end check for corrupted message
    else {
      MsgReceived = false;
    }
  } // filtered message in receive buffer
  else {
    MsgReceived = false;
  } // no filtered message in receive buffer
} // end RECEIVE_MESSAGE ()

void SEND_MESSAGE (byte Number, unsigned int ID, byte *DATA){
//  if ((!XcvrSleep) || (Name = _XCVR)){
  if (!XcvrSleep){
    SendFrame_id = ID;
    /* void CANClass::load_ff_g(byte length, uint32_t *id, byte *data, bool ext) procedure
     added to CAN.cpp and CAN.h so that a CAN.load_ff_g call will send msg
     to Tx buffer 2 if free, then 1 then 0.  Buffer transmit priority is buffer 2, 1, 0
     unless register write is used to change priorities. 
     Lower priority messages can be sent by calling e.g. load_ff_0  
     
     2012_1_9: transmission on buffers 0 and 1 work fine, but on buffer
     2 only the ID seems to be sent, but data received are all 0x0 and 
     complements = 0xFF.  For now, CAN.load__ff_g changed to send only to
     buffers 0 and 1.
     */
    CAN.load_ff_g(Number,&SendFrame_id,DATA, false);
    //   CAN.load_ff_1(Number,&SendFrame_id,DATA, false);
  }
} // end SEND_MESSAGE

void PARSE_ID (void){
  Category = (ReceiveFrame_id >> 9) & 0b11;
  Name = (ReceiveFrame_id >> 3) & 0b111111;
  Target = ReceiveFrame_id & 0b111;
} // end PARSE\_ID

void XCVR (void){
  byte XcvrState = frame_data[0];
  if (XcvrState == Wake){
    digitalWrite (XcvrSleepPin, LOW);
    pinMode (XcvrSleepPin, INPUT);
    SETUP_CAN ();
    XcvrSleep = false;
    byte Backlight;
    //added in case Master has re-started
    for (byte i = 1; i<=10; i++){
      SEND_MESSAGE (2, NodeLive_id, DisplayStart);
      delay (10);
    }
/*
    Backlight = ScreenBright;
    if (Backlight == 0) {
      digitalWrite (XcvrSleepPin, LOW);
    }
    analogWrite (BKLT, ScreenBright);
*/
  }
  if (XcvrState == Sleep){
    digitalWrite (BKLT, HIGH); // completely shuts off back light
    do { // make sure CAN.setMode(SLEEP) isn't blocked by messages in buffer
      RECEIVE_MESSAGE ();
    } while (MsgReceived);
    pinMode (XcvrSleepPin, OUTPUT);
    delay (10);
    digitalWrite (XcvrSleepPin, HIGH);
    delay (10);
    XcvrSleep = true;
    do { // make sure CAN.setMode(SLEEP) isn't blocked by messages in buffer
      RECEIVE_MESSAGE ();
    } while (MsgReceived);
    CAN.setMode(SLEEP);
    delay (100);
    MCU_SLEEP ();
// returns here after interrupt
  }
  if (XcvrState == Shutoff){
    delay (100);
    digitalWrite (RESET_PIN, LOW);
// delay increased from 10 to ensure shutoff
    delay (100);
    pinMode (PowerOnPin, OUTPUT);
    delay (10);
    digitalWrite (PowerOnPin, HIGH);
    delay (1000);
  }
} // end XCVR

void MCU_SLEEP (void){
  set_sleep_mode (SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  // make sure we can't interrupt before we go to sleep, 
  noInterrupts ();
  // will be called when pin 3 goes low  
  attachInterrupt (1, MCU_WAKE, LOW);
  // turn off brown-out enable in software
  MCUCR = _BV (BODS) | _BV (BODSE);
  MCUCR = _BV (BODS); 
  // it is guaranteed that the sleep_cpu call will be done
  // as the processor executes the next instruction after
  // interrupts are turned on.
  interrupts ();
  sleep_cpu ();  
} // end of MCU_SLEEP

void MCU_WAKE (void){
// cancel sleep as a precaution
  sleep_disable();
  delay (25);
  SETUP_CAN ();
  delay (25);
  detachInterrupt (1);
  digitalWrite (XcvrSleepPin, LOW);
  pinMode (XcvrSleepPin, INPUT);
  if (ScreenBright == 0) {
    digitalWrite (XcvrSleepPin, LOW);
  }
  else {
    analogWrite (BKLT, ScreenBright);
  }
  //added in case Master has re-started
  for (byte i = 1; i<=10; i++){
    SEND_MESSAGE (2, NodeLive_id, DisplayStart);
    delay (10);
  }
}  // end of MCU_WAKE

void LogMsg (byte message){
  byte Year, Month, Day, Hour, Minute;
  byte lastYear = EEPROM.read (10);
  byte lastMonth = EEPROM.read (11);
  byte lastDay = EEPROM.read (12);
  byte lastHour = EEPROM.read (13);
  boolean badDateTime = false;
  if (((lastYear<0)||(lastYear>99))||((lastMonth<0)||(lastMonth>12))||
     ((lastDay<0)||(lastDay>31))||((lastHour<0)||(lastHour>24))){
    badDateTime = true;
  }
  bool Century = false;
  bool h12;
  bool PM;
// Start the I2C interface
  Wire.begin();
  Year = Clock.getYear();
  currentYear = Year;
  Month = Clock.getMonth(Century);
  currentMonth = Month;
  Day = Clock.getDate();
  Hour = Clock.getHour(h12, PM);
  Minute = Clock.getMinute();
  boolean FileOpened = false;
  char logName[13];
  char msgFileName[13];
  char msgText[60];
  if (message >= _WDGERR){ // single-option messages
    snprintf (msgFileName, 13, "log_%u.txt", message);
  }
  else if (message>0){ // multi-option messages
    snprintf (msgFileName, 13, "log_%u_%u.txt", message, frame_data[0]);
  }
// read text to be logged from msgFileName SD file
  byte i = 0;
  FileOpened = msgFile.open(msgFileName, O_READ);
  if (FileOpened) {
// read from the file until there's nothing else in it:
    while (((msgText[i] = msgFile.read() ) >= 0) && (i <= 58)) {
      i = i + 1;
    }
  }
  msgFile.close();
// write date/time header and text to monthly logFile  
  snprintf (logName, 13, "%u_%u.log", Year, Month);
//  logFile = SD.open (logName, FILE_WRITE);
  logFile.open(logName, O_RDWR | O_CREAT | O_AT_END);
  char logHeader[55];
  snprintf (logHeader, 54, "20%u/%u/%u %u:%02u  RunTime %u:%02u  TotalTime %lu:%02u", 
         Year,Month,Day,Hour,Minute,RunHours,RunMinutes,TotalHours,TotalMinutes);
  if (message == _STARTUP){
    if (lastYear > Year){
      lastYear = Year;
      lastMonth = Month;
    }
    if ((lastMonth > Month)&&(lastYear = Year)){
      lastMonth = Month;
    }
    if ((lastDay > Day)&&(lastMonth = Month)){
      lastDay = Day-1;
      if (lastDay < 1){
        lastDay = 1;
      }
    }
    if ((Year>lastYear)||(Month>lastMonth)||(Day>lastDay)||((Hour-lastHour)>=6)||badDateTime){
      logFile.print ("STARTUP: ");
      logFile.println (logHeader);
      logFile.println ();
      EEPROM.update (10,Year);
      EEPROM.update (11,Month);
      EEPROM.update (12,Day);
      EEPROM.update (13,Hour);
    }
  } // if _STARTUP
  if (i > 0){
    logFile.println (logHeader);
    for (byte j=0; j<=i-1; j++){
      logFile.write (msgText[j]);
    }
    if (message == _CONTACT){
      logFile.print ("     InVolts = ");
      logFile.print (InVolts);
      logFile.print ("    OutVolts = ");
      logFile.println (OutVolts);
    }
    logFile.println ();
  }
  logFile.close ();
} // end of LogMsg

void SEND_LogFile (byte Month, int Year){
  byte DATA[8];
  bool Century = false; // can not use key word "boolean"
  if (((Year==0) && (Month==0))||((Year-2000)>99)||(Month>12)){
    Wire.begin();
    Year = Clock.getYear()+2000;
    Month = Clock.getMonth(Century);
  }
  boolean FileOpened = false;
  char logName[13];
  snprintf (logName, 13, "%u_%u.log", Year-2000, Month);
  if (!logFile.open(logName, O_READ)){
    char NotFoundString[24] = "*** FILE NOT FOUND *** ";
    for (byte i = 0; i<=23; i=i+4){
      for (byte j = 0; j<=3; j++){
        DATA[j] = NotFoundString[i+j];
      }
      for (byte j = 0; j<=3; j++){
        DATA[j+4] =  ~DATA[j];
      }
      SEND_MESSAGE (8, LogFile_ret, DATA);
      delay (10);
    }
  }
  else {
// read from the file until there's an 0xFF byte
    byte data;
    byte index = 0;
    while ((data = logFile.read()) < 0xFF){
      DATA[index]=data;
      index = index + 1;
      if (index == 4){
        for (byte j = 0; j<=3; j++){
          DATA[j+4] =  ~DATA[j];
        }
        SEND_MESSAGE (8, LogFile_ret, DATA);
        delay (5);
        index = 0;
      }
    } // end while ((data = logFile.read()) >= 0)
    if (index != 0){ // fill last frame if partial
      for (byte i = index; i <= 3; i++){
        DATA[index] = 0;
      }
      for (byte j = 0; j<=3; j++){
        DATA[j+4] =  ~DATA[j];
      }
      SEND_MESSAGE (8, LogFile_ret, DATA);
        delay (5);
    } // end (index != 0)
  } // end if (!logFile.open(logName, O_READ) else
  for (byte j = 0; j<=3; j++){
    DATA[j] =  0xFF;
    DATA[j+4] =  ~DATA[j];
  } // end loading end of transmission frame
  SEND_MESSAGE (8, LogFile_ret, DATA);
  delay (100);
  // close the file:
  logFile.close();
} // end SEND_LogFile

void DisplaySetSlowMsg (byte SetSlow){
  switch (SetSlow){ 
    case 0:
      bmpDraw("stop.bmp", 115, 17);
      break;
    case 1:
      bmpDraw("vryslow.bmp", 115, 17);
      break;
    case 2:
      bmpDraw("slow.bmp", 115, 17);
      break;
    case 3:
      tft.fillRect(115, 17, 25, 35, Background);
      break;
  }
}

void TestAndDemo (void){
  UsrToggle = Attendant;
  Mode = Drive1;
  PaintBaseScreen ();
  delay(1000);
  DisplaySetSlowMsg (0);
  delay (2000);
  DisplaySetSlowMsg (1);
  delay (2000);
  DisplaySetSlowMsg (2);
  delay (2000);
  DisplaySetSlowMsg (3);
  delay (2000);
  Mode = Drive2;
  PaintBaseScreen ();
  delay (1000);
  Mode = Drive3;
  PaintBaseScreen ();
  delay (1000);
  Mode = Seat;
  PaintBaseScreen ();
  delay (1000);
  Mode = Lights;
  PaintBaseScreen ();
  delay (1000);
  UsrToggle = Attendant;
  Mode = Drive1;
  PaintBaseScreen ();
  RotateSpeedPot ();
  ExerciseMeters ();
  delay (1000);
// TYPING MESSAGES TAKES UP WAY TOO MUCH PROGMEM, USE BMPs INSTEAD
  for (byte msg = 0; msg <= 31; msg++) {
    if (msg == 8) {
      UsrToggle = Driver;
      Mode = Lights;
    }
    DisplayMsg (paint, msg);                                                                   
    frame_data[0] = 0;
    LogMsg (msg);
    if ((msg > 7) && (msg <= 19)){
      frame_data[0] = 1;
      LogMsg (msg);
      frame_data[0] = 0;
    }
    delay (500);
    LastMode = None;
    DisplayMsg (erase, msg); // erase msg
    delay (500);
  }
} // end TestAndDemo

void ByteDots (byte message){
  byte Xcoord;
// use 8 dots to show 8 bits of message, red = 1, blue = 0
  for (byte i = 10; i<=150; i = i+20){
    tft.fillCircle (i, 50, 3, ST7735_BLUE);
  }
  for (byte i = 7; i >= 0; i--){
    Xcoord = 150-20*i;
    if (bitRead(message,i)==1){
      tft.fillCircle (Xcoord, 50, 3, ST7735_RED);
    }
  }
  delay (500);
//  PaintBaseScreen ();
} // end of ByteDots


/*
2013_09_29
        First complete implementation of DisplayNODE.
        SpeedSlow display messages not working
        check for OON conditions should be moved into separate procedures
2013_09_30
         SpeedSlow display messages had been commented out for testing;
             fixed
         Volts and AHr meters were not updating except when base bmp was
             repainted.  Added line to fill a Background rectangle before
             filling updated bars.
2013_10_04
          In Master, display messaging removed from setup and periodic display update
             added to DO_MODE.  Presently updating every CyclesPerDisplayUpdate
             cycles through Loop, but may want to change to millis-based timing.
             In DisplayNode, check for StartupState removed from loop so that display
             overpaints Startup wait message as soon as first update arrives.
          Sometimes screen re-paints while in sleep: i.e. Display has
             received a CAN msg even though "offically" with transceiver
             in standby.  
               Added if ((!XcvrSleep)|| (Name = _XCVR)) in Master SEND_MESSAGE
               to avoid cueing any messages except XCVR when transceivers are
               in sleep.  This solved the problem of screen re-paint while asleep,
               but Roboteq emulator still doesn't always go to sleep.
             _STARTUP handling changed to just log the startup with re-paint left
               to Master sending a message.
          SEND_DISPLAY_MESSAGE1 (_SETSLOW, SetSlow); added in Master DO_MODE so that
             stop, very slow and slow symbols get repainted if UsrToggle or Mode change.
2013_10_06 Added Shutdown enumeration for non-Master XCVR messaging.  In the non.Master
             nodes, this pulls the PowerOnPin (currently A1) to HIGH.  Connected 
             base of transistor that pulls down pin Vr of the DC-DC converter
             for that node.  This gives a very positive power turn off.  
2013_10_12 Added:
               if (SetSlow != 3){
                 DisplaySetSlowMsg (SetSlow);
               }
             upon wakeup from sleep ti re-paint SetSlow icon.
2013_10_15 Attempted to pass bmpFile by reference (i.e. as  SdFile& ) to 16-bit and 
             32-bit read functions, but that did not paint the bmp file.
             I have not tried to figure out why this didn't work.
           Four-byte reads in bmpDraw changed to for loop rather than line-by-line
             filling of array.
           For readability, commented out lines from the Adafruit example that
             used SD rather than SdFat have been removed.
2013_10_24 SD_CS changed to pin D5.
             Added #define BKLT 6 for TFT with accessible backlight LED. For Sainsmart,
             added P-type MSOFET as high-side PWM switch, so LOW = LED on.
2013_11_02 Joystick, Seat and Lights display markers added for demo or 
             testing purposes.  COMMENT OUT BEFORE USE
           Stop switch (push on/push off) sends _SETSLOW 0 (STOP symbol)
             message to display node.
           DisplaySetSlowMsg (SetSlow) added to PaintBaseScreen.
2013_11_06 Comment bracketing of TestAndDemo procedure removed as PROGMEM
             use is the same (if not called) whether the procedure is there
             or not.  This way, can be run by uncommenting a single line.
2013_11_09 Switch OutOfNeutral message handling and bmp files added to display
             which switch, or >1 switch, is out of neutral.
           NOTE: demo markers for driving, seat and lights do not re-paint if
             UsrToggle is flipped.  As these markers are for demo only, and will
             be absent in actual use, will probably not add re-paint code.
2014_02_10 code added to dim screen when in sleep assuming high-side MOSFET
             on TFT board analogWrite controlled from BKLT pin (= pin 6).  
2014_02_16 corrected numbering of log messages and bmp files and void LogMsg (byte message)
             now uses _CONTACT constant instead of a number to divide between
             multiple-value and single-value messages.  Thus, changes in Constants.h
             should not require changing the procedure, as long as the messages are 
             in 2 distinct blocks.
2014_03_04 Added delay at two places in _XCVR shutdown.  If no delay,
             PowerOnPin does not stay high long enough to reliably shut
             Recom DC-DC converter. Minimum delay is 6 msec, set at 10.
2014_03_24 Completed coding of real time clock and PWM control of screen backlight 
             (P-channel high side MOSFET added to TFT board).  
             Fixed some bugs (in Master) that blocked showing OON messages.
2015_01_01   added:
             while ((!DisplayLive) || (!AuxLive) || (!RoboteqLive)){
               RECEIVE_MESSAGE ();
             }
             in Master and corresponding messaging in Aux, Display and Roboteq
               to hold Master in Setup until other nodes are live rather than
               using arbitrary delay.
             Handling of Masater's SEND_DISPLAY_MESSAGE0 (_STARTUP); modified to
               ensure Display gets this message: moved into Loop () after
               RoboteqStartTime has been set, then sent multiple times, with one-shot
               receive in Display.
2015_01_03 Added SEND_LogFile procedures and changed if to else if in SetCommand 
             section and put "handle all other SetCommand messages" switch 
             into an else if to avoid tests that aren't needed.
2015_02_14 Instead of trying to fix occassional false interrupt of MCU_SLEEP,
             leave screen dimmed on false wake, and set to bright only when
             CAN messages arrive.  (With BusSpeed at 250, false interrupts
             don't seem to happen.  Need to retry at faster speeds.)
           Syntax for data stored in PROGMEM changed to const as that is 
             compatible with both Arduino 1.0.x and 1.6.0
           DO NOT UPLOAD WITH Arduino 1.6.0 
             -1- TURN_OFF OF Display AND Aux FROM Master DOES NOT WORK
             -2- Programmer RE-BOOTS AFTER VOLTAGE CALIBRATION STEP
2015_03_28 Added message handling for MotorOpen messages from Roboteq - bmp and txt files don't yet
             exist.
2015_05_09 Display doesn't use any analog inputs, so set ADCSRA to 0 in Setup
             and removed turning it off in MCU_SLEEP and back on after interrupt.
           Added noInterrupts and interrupts in MCU_SLEEP to ensure that noise
             on pin 3 can't trigger interrupt before sleeping.
           (Need to do same in Aux, but not yet done because USB jack inaccessible)
2015_05_14 Noticed if ((!XcvrSleep) || (Name = _XCVR)) in SEND_MESSAGE which has no sense for 2
             reasons: (1) id's are now #defined, so Name is assigned only on incoming messages, and
             (2) there are no _XCVR outgoing messages in DisplayNode in any case.  Changed to:
             if (!XcvrSleep) to avoid risk of ever adding code that tries to send messages while
             asleep.
2015_05_15 Repeated logging of _STARTUP when driving was caused by Roboteq repeatedly sending "motor open resolved" 
             message with no matching log-text file.  File has been added to SD, but Roboteq script
             needs to be changed so that this message is not sent except after a _MOTOPEN state occurred.
           SEE DisplayNode15a_temp for various examples of painting dots as diagnostic tool in absence of Serial.
2015_05_16 added void ByteDots (byte message) procedure so that I can use painted dots to read any byte for diagnosis
             (substitutes for unusable Serial.print).
2015_05_23 logMsg modified to log _STARTUP only after minimum of 6 hours since last logging.
2015_10_07 XCVR ( changed to use RESET_PIN, LOW to turn off transceiver so it
             can't receive any extraneous messages after getting a Shutoff msg.
2015_10_12 Handling of _STARTUP changed both here and in Master
2015_10_13 Changed order of receive buffer emptying and added extra delay in XCVR ()
             to make going into sleep more reliable.
2015_12_30 Added handling for EEPROMfault message.  Logs once and displays "Using default Settings"
             repeatedly.
2016_02_04 Handling of Brake fault messages changed to log once but paint repeatedly so
             message from open coil on startup is not over-painted
2016_02_07 Handling of MotorOpen messages changed so that display is erased when joystick at
             neutral or motor open condition has been resolved.  May want to change Roboteq script
             so that resolved message is only sent if joystick out of neutral.
           changed handling of _CONTACT message to display and log once
2016_05_16 Corrected log file month and year handling
2017_07_09 adding:
                   delay (25);
                   SETUP_CAN ();
                   delay (25);
           in MCU_WAKE () here and in Aux cures wake from sleep problem
2017_10_25 Current sensors added back with fault detect in Roboteq script and sensor fault
              message to Display
2018_10_05 added:
    for (byte i = 1; i<=10; i++){
      SEND_MESSAGE (2, NodeLive_id, DisplayStart);
      delay (10);
     }
   in three places in case Master has re-started
2019_02_21 Parsing of _TOTALTIME and _RUNTIME messages changed to use
             3 bytes for hours and TotalTime changed to unsigned long
             to prevent rollover at 1096 hours.
*/



