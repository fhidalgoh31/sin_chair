#ifndef __Defines__
#define __Defines__

// Arduino defines:
// #define mosi 11
// #define miso 12
// #define sclk 13
// CAN.h defines:
// #define SS_PIN    10 // MCP2151 chip select
// #define RESET_PIN A2
// #define INT_PIN 3 // note: overloaded in Node programs

#define SD_CS 5  // Chip select line for SD card
#define BKLT 6 // for TFT with accessible backlight LED
#define cs 7 // TFT screen chip select
#define rst 8  // you can also connect this to the Arduino reset
#define dc 4
#define XcvrRxDPin 3
#define PowerOnPin 9 // OUTPUT HIGH to turn DC-DC converter OFF
// in Master PowerOnPin OUTPUT HIGH is used to hold DC-DC converter ON
// pin A2 used for MCP2151 reset by CAN.h
#define XcvrSleepPin A3
// RTC CONNECTIONS defined in Wire library:
// DS3231 SDA --> A4
// DS3231 SCL --> A5
// if these are stored in EEPROM so they can be changed via a 
// CAN bus programmer, or use these values
#define ScreenDim 251 // 0 = full brightness, 255 = fully dimmed
#define ScreenBright 0 // 0 = full brightness, 255 = fully dimmed

// for CAN messaging
// #define SetCommand B00
// #define GetValue B01
// #define Config B10 // SetConfig constants 0 to 63
// #define Config2 B11 // SetConfig constants >= 64

// NOT used in Display node
// #define fromAux B000
// #define toAux B001
// #define fromRoboteq B010
// #define toRoboteq B011
// #define fromMaster B100
// #define toMaster B101

// #define fromDisplay B110
// #define toDisplay B111
// sleep controls
#define Sleep 0
#define Wake 1
#define Shutoff 2
#define MSG_IN_BUFFER0 (rx_status & 0x40)
#define MSG_IN_BUFFER1 (rx_status & 0x80)

// for TFT screen
#define Portrait 0
#define LandscapeInv 1
#define PortraitInv 2
#define Landscape 3
#define Background ST7735_WHITE
// #define charwidth 6 // from AdaFruit GFX; not used
#define charheight 8 // from AdaFruit GFX
#define paint 1
#define erase 0

// for Volt and AHr meters; these values must change for
// different size displays, or calculate based on screen size
#define BarHeight 40
#define BarWidth 14
#define BarY 11

#define None 0 // fault message displayed 
// set LastMode = None to force re-paint of base image
// Mode and User values from Master
#define Drive1 1
#define Drive2 2
#define Drive3 3
#define Seat 4
#define Lights 5
#define Driver HIGH
#define Attendant LOW

// buffer for reading bmp from SD card
#define BUFFPIXEL 20
// time to re-paint full screen is 920 msec with 20 pixel buffer
// barely reduced to 890 msec with 80 pixel buffer

// seat movements
#define none 0
#define forward 1
#define back 2
#define down 3
#define up 4

// switch states from Master for general use
#define OFF 1
#define ON 0

#endif







