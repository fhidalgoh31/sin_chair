#include <CAN.h>
#include <SPI.h>
#include "constants.h"
#include "MsgIDs.h"

#define MSG_IN_BUFFER0 (rx_status & 0x40)
#define MSG_IN_BUFFER1 (rx_status & 0x80)

boolean MsgReceived;
unsigned long SendFrame_id;
unsigned long ReceiveFrame_id;
byte length,rx_status,filter,ext,Category,Name,Target;
byte frame_data[8]; // this is a buffer, a frame may have fewer bytes
unsigned long lastCANSendTime;
unsigned long lastCANReceiveTime;
boolean NoPrinting = false;
byte DATA [2];
unsigned int ID = 0b00000000010;
unsigned int ReceiveCalls = 0;
byte SentMsg = 1;
byte RecdMsg = 1;

void setup () {
  Serial.begin(115200);
  Serial.println(__FILE__ " - " __DATE__ " " __TIME__);
  Serial.println (F("WheelchairCAN - simple CAN monitor"));
  SETUP_CAN ();
  lastCANSendTime = millis ();
  lastCANReceiveTime = lastCANSendTime;
} // end setup

void loop () {
//  delay (1);
  boolean SendElapsed = ((millis () - lastCANSendTime)>100);
  if (SendElapsed){
    DATA[0] = SentMsg;
    DATA[1] = ~DATA[0];
    SEND_MESSAGE (2, ID, DATA);
    lastCANSendTime = millis ();
    SentMsg = SentMsg+1;
    if (SentMsg > 4){
      SentMsg = 1;
    }
  }
  delay (1);
  RECEIVE_MESSAGE ();
  ReceiveCalls = ReceiveCalls + 1;
  if (MsgReceived){
    Serial.print ("calls = ");
    Serial.print (ReceiveCalls);
    Serial.print ("  msg = ");
    Serial.print (RecdMsg);
    Serial.print ("  time = ");
    ReceiveCalls = 0;
    unsigned long TimeNow = millis ();
    Serial.println (TimeNow - lastCANReceiveTime);
    lastCANReceiveTime = TimeNow;
    RecdMsg = RecdMsg +1;
    if (RecdMsg > 4){
      RecdMsg = 1;
      Serial.println ("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
    }
  }
} // end loop

void RECEIVE_MESSAGE (){
  MsgReceived = false;
// clear receive buffers, just in case.
  for (byte i = 0; i <= 7; i++){
    frame_data[i] = 0x00;
  }
  ReceiveFrame_id = 0x00;
//  Serial.println ("RECEIVE_MESSAGE");
  rx_status = CAN.readStatus();
  if (MSG_IN_BUFFER0){
    CAN.readDATA_ff_0(&length,frame_data,&ReceiveFrame_id, &ext, &filter);
    printBuf(rx_status, length, ReceiveFrame_id, filter, 0, frame_data, ext); 
  }
/* in original code from which I lifted this, the following was
"if" rather than "else if".  Doing that means that a lower-priority
(buffer 1) message could replace a higher priority (buffer 0) message
if both buffers were full.  Using "else if" will cause buffer 1 to be
read on next pass if another buffer 0 message has not arrived in the
meantime.
*/
  else if (MSG_IN_BUFFER1){
    CAN.readDATA_ff_1(&length,frame_data,&ReceiveFrame_id, &ext, &filter);
    printBuf(rx_status, length, ReceiveFrame_id, filter, 1, frame_data, ext); 
  }
  if ((MSG_IN_BUFFER0) || (MSG_IN_BUFFER1)){
    MsgReceived = true;
  } // no filtered message in receive buffer
} // end RECEIVE_MESSAGE ()

void SEND_MESSAGE (byte Number, unsigned int ID, byte *DATA){
  SendFrame_id = ID;
  if (NoPrinting == false){
//    Serial.println ();
    Serial.print (F("SendFrame_id = "));
    Serial.print (SendFrame_id);
    Serial.print (F(" = 0x"));
    Serial.print (SendFrame_id, HEX);
    Serial.print (F(" = 0b"));
    Serial.print (SendFrame_id, BIN);
    if (Number > 0){
      Serial.print (F("   data bytes: "));
      for (byte i = 0; i<=(Number-1); i++){
        Serial.print (F("n. "));
        Serial.print (i);
        Serial.print (F(" = "));
        Serial.print (DATA[i]);
        Serial.print ("  ");
      }
      Serial.println ();
    }
  }
    /* void CANClass::load_ff_g(byte length, uint32_t *id, byte *data, bool ext) procedure
     added to CAN.cpp and CAN.h so that a CAN.load_ff_g call will send msg
     to Tx buffer 2 if free, then 1 then 0.  Buffer transmit priority is buffer 2, 1, 0
     unless register write is used to change priorities. 
     Lower priority messages can be sent by calling e.g. load_ff_0  
     
     2012_1_9: transmission on buffers 0 and 1 work fine, but on buffer
     2 only the ID seems to be sent, but data received are all 0x0 and 
     complements = 0xFF.  For now, CAN.load__ff_g changed to send only to
     buffers 0 and 1.
     */
  CAN.load_ff_g(Number,&SendFrame_id,DATA, false);
  //   CAN.load_ff_1(Number,&SendFrame_id,DATA, false);
} // end SEND_MESSAGE

void SETUP_CAN () {
  // initialize CAN bus class
  // this class initializes SPI communications with MCP2515
  CAN.begin();
  CAN.setMode(CONFIGURATION);  // set to "NORMAL" for standard communications
  CAN.baudConfig(BUS_SPEED);
/*
   The lower the ID value, the higher the priority.
   Highest priority messages go first to buffer 0, then to buffer 1
      if buffer 0 is full (depends on MCP2151 register setting)
      or if buffer 0 filter not matched.
   If 2 messages arrive at the same time, higher priority msg goes to
      buffer 0, lower priority msg goes to buffer 1.
  "Category" in 2 msb of ID determines priority of Roboteq message groups: 
      0b00=SetCommand, 0b01=GetValue, 0b10 and 0b11 = configuration.
   Within a catagory, "Name" in next 6 bits determines priority.
   Thus, SetCommand is highest priority group, and _G (motor output setting = 0)
      has absolute highest priority.
*/
  CAN.setMaskOrFilter(MASK_0,   0b00000000, 0b00000000, 0b00000000, 0b00000000);
  CAN.setMaskOrFilter(FILTER_0, 0b00000000, 0b00000000, 0b00000000, 0b00000000); // all messages
  CAN.setMaskOrFilter(FILTER_1, 0b00000000, 0b00000000, 0b00000000, 0b00000000); // all messages

  CAN.setMaskOrFilter(MASK_1,   0b00000000, 0b00000000, 0b00000000, 0b00000000);
  CAN.setMaskOrFilter(FILTER_2, 0b00000000, 0b00000000, 0b00000000, 0b00000000); // all messages
  CAN.setMaskOrFilter(FILTER_3, 0b00000000, 0b00000000, 0b00000000, 0b00000000); // all messages
  CAN.setMaskOrFilter(FILTER_4, 0b00000000, 0b00000000, 0b00000000, 0b00000000); // all messages
  CAN.setMaskOrFilter(FILTER_5, 0b00000000, 0b00000000, 0b00000000, 0b00000000); // all messages
  CAN.setMode(NORMAL);  // set to "NORMAL" for standard com
} // end SETUP_CAN

void printBuf(byte rx_status, byte length, uint32_t ReceiveFrame_id, byte filter, byte buffer, byte *frame_data, byte ext) {
  if (NoPrinting == false){
    Serial.print(F("[Rx] Status:"));
    Serial.print(rx_status,HEX);
    Serial.print(F(" Len:"));
    Serial.print(length,HEX);
    Serial.print(F(" ID:0x"));
    Serial.print(ReceiveFrame_id,HEX);
    Serial.print(F(" EXT:"));
    Serial.print(ext==1,HEX);
    Serial.print(F(" Filter:"));
    Serial.print(filter,HEX);
    Serial.print(F(" Buffer:"));
    Serial.print(buffer,HEX);
    Serial.print(F(" Data:[0x"));
    for (int i=0;i<8;i++) {
      if (i>0){
       Serial.print(F(" 0x"));
      }
      Serial.print(frame_data[i],HEX);
    }
    Serial.println(F("]"));
  } // end if NoPrinting == false
} // end printBuf

/*
2014_07_19 Simple CAN monitor program to see Roboteq messages

*/









