/*
DS3231_set.pde
Eric Ayars
4/11
Modified by L. Robbins, February, 2014

Reset time on DS3231 RTC and serial print results of reading clock

// CONNECTIONS:
// DS3231 SDA --> A4
// DS3231 SCL --> A5

*/

#include <DS3231.h>
#include <Wire.h>

DS3231 Clock;

byte Year;
byte Month;
byte Date;
byte DoW;
byte Hour;
byte Minute;
byte Second;

bool Century=false;
bool h12;
bool PM;
bool ClockSet = false;

void GetDateStuff(byte& Year, byte& Month, byte& Day, byte& DoW, 
  byte& Hour, byte& Minute, byte& Second) {
// Call this if you notice something coming in on 
// the serial port. The stuff coming in should be in 
// the order YYMMDDwHHMMSS, with an 'x' at the end.
  boolean GotString = false;
  char InChar;
  byte Temp1, Temp2;
  char InString[20];
  byte j=0;
  while (!GotString) {
    if (Serial.available()) {
      InChar = Serial.read();
      InString[j] = InChar;
      j += 1;
      if (InChar == 'x') {
        GotString = true;
      }
    }
  }
  Serial.println(InString);
// Read Year first
  Temp1 = (byte)InString[0] -48;
  Temp2 = (byte)InString[1] -48;
  Year = Temp1*10 + Temp2;
// now month
  Temp1 = (byte)InString[2] -48;
  Temp2 = (byte)InString[3] -48;
  Month = Temp1*10 + Temp2;
// now date
  Temp1 = (byte)InString[4] -48;
  Temp2 = (byte)InString[5] -48;
  Day = Temp1*10 + Temp2;
// now Day of Week
  DoW = (byte)InString[6] - 48;		
// now Hour
  Temp1 = (byte)InString[7] -48;
  Temp2 = (byte)InString[8] -48;
  Hour = Temp1*10 + Temp2;
// now Minute
  Temp1 = (byte)InString[9] -48;
  Temp2 = (byte)InString[10] -48;
  Minute = Temp1*10 + Temp2;
// now Second
  Temp1 = (byte)InString[11] -48;
  Temp2 = (byte)InString[12] -48;
  Second = Temp1*10 + Temp2;
  Serial.println ("Clock has been re-set");
  Serial.println ("***********************************");
}

void setup () {
// Start the serial port
  Serial.begin(115200);
// Start the I2C interface
  Wire.begin();
  ShowInfo ();
  Serial.println ();
  Serial.println ("If you want to reset the clock, enter date and time as:");
  Serial.println ("            YYMMDDdWHHMMSSx");
  Serial.println ("for dW, 1 = Sunday, 7 = Saturday - end with 'x'");
  Serial.println ();
}

void loop () {
// If clock has not yet bee set and something is coming in on the 
// serial line, it's a time correction so set the clock accordingly.
  if ((Serial.available()) && (!ClockSet)) {
    GetDateStuff(Year, Month, Date, DoW, Hour, Minute, Second);
    Clock.setClockMode(false);	// set to 24h
  //setClockMode(true);	// set to 12h
    Clock.setYear(Year);
    Clock.setMonth(Month);
    Clock.setDate(Date);
    Clock.setDoW(DoW);
    Clock.setHour(Hour);
    Clock.setMinute(Minute);
    Clock.setSecond(Second);
    ClockSet = true; // clock setting is read in just once
/*
  // Test of alarm functions
  // set A1 to one minute past the time we just set the clock
  // on current day of week.
    Clock.setA1Time(DoW, Hour, Minute+1, Second, 0x0, true, false, false);
  // set A2 to two minutes past, on current day of month.
    Clock.setA2Time(Date, Hour, Minute+2, 0x0, false, false, false);
  // Turn on both alarms, with external interrupt
    Clock.turnOnAlarm(1);
    Clock.turnOnAlarm(2);
*/
  }
  if (ClockSet){ // clock has been reset, now read RTC to check
    ShowInfo ();
  }
}

void ShowInfo (void) {// send what's going on to the serial monitor.
// Start with the year
  Serial.print("2");
  if (Century) {  // Won't need this for 89 years.
    Serial.print("1");
  } else {
    Serial.print("0");
  }
  Serial.print(Clock.getYear(), DEC);
  Serial.print("_");
// then the month
  Serial.print(Clock.getMonth(Century), DEC);
  Serial.print("_");
// then the date
  Serial.print(Clock.getDate(), DEC);
// and the day of the week
  Serial.print (" ");
  byte Day = Clock.getDoW();
  switch (Day){
    case 1:
      Serial.print (" Sunday ");
      break;
    case 2:
      Serial.print (" Monday ");
      break;
    case 3:
      Serial.print (" Tuesday ");
      break;
    case 4:
      Serial.print (" Wednesday ");
      break;
    case 5:
      Serial.print (" Thursday ");
      break;
    case 6:
      Serial.print (" Friday ");
      break;
    case 7:
      Serial.print (" Saturday ");
      break;
  }
  Serial.print(", hr: ");
// Finally the hour, minute, and second
  Serial.print(Clock.getHour(h12, PM), DEC);
  Serial.print(":");
  Serial.print(Clock.getMinute(), DEC);
  Serial.print(":");
  Serial.print(Clock.getSecond(), DEC);
// Add AM/PM indicator
  if (h12) {
    if (PM) {
      Serial.print(" PM ");
    } else {
      Serial.print(" AM ");
    }
  } else {
    Serial.print(" 24h ");
  }
  Serial.println ();
// Display the temperature
  Serial.print("T=");
  Serial.print(Clock.getTemperature(), 2);
  Serial.println ();
// Tell whether the time is (likely to be) valid
  if (Clock.oscillatorCheck()) {
    Serial.print(" Oscillator OK");
  } else {
    Serial.print(" Oscillator kaput");
  }
  Serial.println ();
  Serial.println ("***********************************");
  delay(1000);
} // end ShowInfo
