/*
2014_06_02 NEW PROGRAMMER NODE.
 So far only provides joystick and speed pot calibration over CAN.
 Next step to program: changing User Settings over CAN.
 2014_12_17 Routines added to update user settings and to verify match between
 user settings in Programmer and in Master
 2014_12_26 Programmer and Master coding changed so that Master's loop() is blocked
 while Programmer does its thing.  Calibration and User Settings updating
 then work without any dropouts or errors.
 
 However, connecting Programmer node caused failure of READ_RoboteqTIME 
 blocking bus.  Several things were tried in programming to eliminate this,
 among them:
 (1)   if (ID == Time_id) {
 delay (10);
 RECEIVE_MESSAGE ()
 }
 added in GET_LONG, but NO HELP WHATSOEVER.
 (2) delays added in Master Programmer tab Next_step () and increased delays in GET_UsrSettings
 Improved reliability of this messaging but didn't help with READ_RoboteqTime.
 (3) Master's receive masks changed to allow only toMaster: doesn't help with Programmer
 killing Master
 
 WHAT DOES COMPLETELY FIX THE PROBLEM IS ADDING DC-DC CONVERTER BOARD
 TO PROGRAMMER NODE!  Evidently, 4.5 V from USB leads to timing
 or other problem in CAN messaging.  Given that it's only a Roboteq
 message that fails, I suspect that the lower voltage allows the TI
 transceiver in the Roboteq to pull the bus too low.
 
2015_01_03 If CAN.begin is sent near beginning of Setup () when Programmer
             ends the other nodes can be blocked and stop proper communication
             among them if Programmer isn't quickly unplugged from the CAN bus.
             This is now avoided by putting CAN.begin after receiving the
             startup keystroke and CAN.setMode(CONFIGURATION); at the end of
             Setup().  There is no CAN.end in the library. bit setting Mode to
             CONFIGURATION blocks the CAN controller.
           Unblock Master" receive_msg loop" added right after CAN.begin to 
             unblock Master if it was stuck in a receive_msg loop.
2015_01_07 Reading log files added including reading either current file and/or
             log files by month and year.  ProgrammerNode31 used sscanf to get 
             date for reading log files, but used 1700 bytes more flash than 
             Serial.read of month and year into two variables and then using atoi.
           Added:
             StartStop = false;
             StartStop_Programmer (); // exits blocking READ_MESSAGE loop in Master
             delay (500);
           near end of Get_LogFiles to ensure that Master is unblocked even if no 'Y' 
             had been entered in response to any query.
2015_01_12 Added CalibrateVoltage () and corresponding functons in Master to adjust
             calibration of voltage measured by voltage divider in Aux node.
2015_01_19 Added state variables, Serial.print statements and a bit of code so that 
             Programmer autmatically resumes if it wasn't plugged in or if Master was 
             off or asleep.
                 delay (1000); is needed during restart if Master was OFF.  500 is not
             enough.
2015_02_03 Procedures added to adjust Roboteq acceleration and motorcompensation
             user settings
2015_02_14 SerialTime procedure added so that program will time out and relase Master
             from blocking loops if serial disconnected before replying to:
               "Do you want to read an earlier log file? (Y/N) "
           Constants.h, labels.h (Programmer node) and MsgIDs.h moved to WC_CAN library
           IF SYSTEM NOT ON WHEN RUN, MUST UNPLUG FROM CAN SOCKET, TURN ON, and
             THEN PLUG BACK IN - don't know why this behavior has changed.
           DO NOT UPLOAD WITH Arduino 1.6.0 
             -1- TURN_OFF FROM Master DOES NOT WORK
             -2- Programmer RE-BOOTS AFTER VOLTAGE CALIBRATION STEP
2015_02_16  Added Setup_CAN and delay and READ_MESSAGE when startup timed out
              to allow Master to turn on Aux and Display if it was turned off.
            Added extra 1000 ms delay to allow reliable resumption after turning
              on Master.
2015_04_06 Added LimpSpeed user setting for Roboteq so that a motor disconnect first causes 
             Throttle=Steering=0 until stick is centered and then allows LimpSpeed so that some power
             is available to help push the chair home.  (thanks to Mike Stewart for suggesting this.)
           
2015_04_14 Searched for and found several places where I had out-of-range array indices.  Having fixed
             these it runs properly when compiled with Arduino 1.6.2.  The changes are noted in:
                  SEND_MESSAGE
                  case Usr_ret:
                  GET_VoltCalibration
                  SEND_VoltCalibration
             
2015_05_10 EEPROM.write changed to EEPROM.update so that only changed user settings are
             written to EEPROM.  (New function in 1.6.2 EEPROM library.)
2015_05_19 AccelComp and DecelComp Roboteq driving parameters removed
2015_12_30 UserSettings changed to match new Roboteq script
           New UserSettings[] array has 1-byte values through UserSettings[43]
              and 2-byte values thereafter
2016_03_22 Changed VoltageDividerOffset from #define to EEPROM-stored global variable and
             added calibration of this to Programmer node.
           Added some Roboteq user settings to Programmer and expanded settings array(s)
             in Master and Programmer to include a number of unused cells for future 
             additions.  Among the additions are definitions of Roboteq pins and possiblity
             to reverse polarity of current sensors or motors or to swap motors. Also see 
             SET_Roboteqxxxx procedures in Master.
2017_07_02 Removed obsolete settings for AccelMix and current sensors - no longer used
           Settings for boosting compensation at low PWM added: MinI = minimum assumed motor current at PWM < 20%
                      LowBoost = motor current multiplier at PWM = 0, declining to zero at PWM = 20%
2017_08_07 UserSettings changed to match parameters in new Roboteq script with
              MotorCompensation changed to use only estimated motor current.
2017_09_22 UserSettings changed to match parameters in new Roboteq script with
              corrected MotorCompensation subroutine
2017_10_25 Current sensors added back with fault detect in Roboteq script and sensor fault
              message to Display
2018_08_25 New Roboteq LoadBoost algorithm that uses abs(Command-Power) to linearize
              acceleration.  LoadBoost_P, LoadBoost_I and LoadBoost_D replaced with just
              LoadBoost.
2018_10_06 default SpeedPot user setting removed and if SpeedPot not installed Master sets
              SpeedPot = SpeedPotFwdMin and TurnPot=TurnPotFwdMin
2018_10_30 CURVE_JOYSTICK now includes possiibility of logarithmic curving
2018_11_09 MinimumStick user setting added so that at total motor command<10*MinimumStick
              and Steering>0,Steering = map (Steering,0,1000,(10*MinimumStick-abs(Throttle),1000;
              to "unstick" casters for turn in place
2018_11_18 MinimumStick also applied to Throttle.  Chair starts to move as soon as stick
              moved beyond Deadband and feel is identical for all SpeedPot and TurnPot
              settings.
*/






