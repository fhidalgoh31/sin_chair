/* Extended fuse changed from:
  nano328.bootloader.extended_fuses=0x05 or 0xFD
  nano328.bootloader.to
extended_fuses=0xFC
to change brownout detection from 2.7V to 4.3V
*/
// averaging in CK_SETSLOW changed from 5 to 10 to try to reduce false triggers

/* MAKE SURE TO USE VERSION OF CAN.H WITH resetPin = A2
and board modified with connection to A2!

Includes messages only for AuxNode. 
RoboteqEmulator has become a Roboteq script.

TO DO:
port Accelerometer code to AuxNode (3 analog inputs)
read one analog input for microswitches for inhibit/very slow/slow
*/
#include <CAN.h>
#include <avr/sleep.h>
#include <SPI.h>
#include <constants.h>
#include <MsgIDs.h>
#include "Defines.h"

unsigned long ReceiveFrame_id;
unsigned long SendFrame_id;
byte length,rx_status,filter,ext,Category,Name,Target;
byte frame_data[8]; // this is a buffer, frame may have fewer bytes
boolean MsgReceived;
boolean NoPrinting = true; // change to false to turn on diagnostic Serial.print lines
boolean XcvrSleep = false;
byte old_ADCSRA;
byte SetSlow = 3; // 0=drive inhibit, 1=Slow1, 2=Slow2, 3=full speed
byte lastSetSlow = SetSlow;
boolean FourWay = false;
boolean Left = false;
boolean Right = false;
boolean HdTailLit = false;
boolean LeftLit = false;
boolean RightLit = false;
#define FourWayBlinkInterval 300
#define TurnBlinkInterval 500
unsigned long lastTimeMark; // used only for testing purposes
unsigned long FourWayBlinkTime;
unsigned long TurnBlinkTime;
boolean SpeedSlowArmed = true;
unsigned int loopCycles = 0;
unsigned long StartTime;
byte AuxStart[2];

void setup (void) {
  pinMode (BrakeLock, INPUT); // lock brakes during startup
  StartTime = millis ();
  Serial.begin(115200);
  Serial.println(__FILE__ " - " __DATE__ " " __TIME__);
  randomSeed(analogRead(0));
  SETUP_CAN ();
  pinMode (Brake1DetectPin, INPUT_PULLUP);
  pinMode (Brake2DetectPin, INPUT_PULLUP);
  pinMode (InVoltPin, INPUT);
  pinMode (OutVoltPin, INPUT);
  pinMode (PowerOnPin, INPUT);
  pinMode (XcvrRxDPin, INPUT);
  pinMode (XcvrSleepPin, INPUT);
  pinMode (SpeedSlowPin, INPUT);
  pinMode (AccelXPin, INPUT);
  pinMode (AccelYPin, INPUT);
  pinMode (AccelZPin, INPUT);
  pinMode (HdTailLtPin, OUTPUT);
  digitalWrite (HdTailLtPin, LOW);
  pinMode (LeftLtPin, OUTPUT);
  digitalWrite (LeftLtPin, LOW);
  pinMode (RightLtPin, OUTPUT);
  digitalWrite (RightLtPin, LOW);
  old_ADCSRA = ADCSRA;
  lastTimeMark = millis ();
  AuxStart[0] = 2;
  AuxStart[1] = ~AuxStart[0];
  SEND_MESSAGE (2, NodeLive_id, AuxStart);
  delay (10);
  Serial.println (F("setup done"));
} // end setup

void loop (void) {
  if (XcvrSleep){ // was sleeping
    boolean RxD = digitalRead (XcvrRxDPin);
    if (!RxD){
// DO ANYTHING THAT NEEDS TO BE DONE WHEN AWAKENING FROM SLEEP HERE
// _XCVR (wake) message may not be seen when awakened by one-shot msg
      detachInterrupt (1);
      digitalWrite (XcvrSleepPin, LOW);
      pinMode (XcvrSleepPin, INPUT);
      SETUP_CAN ();
      XcvrSleep = false;
      lastSetSlow = SetSlow;
      SpeedSlowArmed = true;
      // added in case Master has re-started
      for (byte i = 1; i<=10; i++){
        SEND_MESSAGE (2, NodeLive_id, AuxStart);
        delay (10);
      }
    }
  } // was sleeping
  else {
    static boolean BrakesLocked = true;
    static boolean AuxStarted = false;
    if (!AuxStarted){
      SEND_MESSAGE (2, NodeLive_id, AuxStart);
      delay (5);
    }
    if (loopCycles % 10 == 0){
      CK_SETSLOW ();
    }
    if (BrakesLocked && AuxStarted && ((millis ()-StartTime)>=1000)){
 // minimum time to avoid brake flicker on volt meter = 400 msec
      pinMode (BrakeLock, OUTPUT);
      digitalWrite (BrakeLock, LOW); // release brake after startup done
      BrakesLocked = false;
    }
    RECEIVE_MESSAGE ();
    if (MsgReceived){
      PARSE_ID ();
      if (Target == toAux){
        AuxStarted = true;
        if (Category == SetCommand){
          switch (Name){
            case _LIGHTSCMD:
              SET_LIGHTS ();
              break;
            case  _SLOW:
              lastSetSlow = SetSlow;
              SpeedSlowArmed = true;
              break;
          }
        } // end SetCommand section
        else if (Category == GetValue) {
          switch (Name){
            case _VOLTS:
              CK_VOLTS ();
              break;
            case _BRAKES:
              CK_BRAKES ();
              break;
          }
        } // end GetValue section
        else if (Category == Config){
          if (Name == _XCVR){ // also in Roboteq emulator
            XCVR ();
          }
        } // end of Config section
        else if (Category == Config2){                      
          Name = Name + 63;
// test for name here and call appropriate procedures
        } // end of Config2 section
      } // Target = toAux
    } // MsgReceived == true
// blink turn signals or four-way flashers
    if (FourWay) {
      if ((uint32_t)(millis ()-FourWayBlinkTime)>FourWayBlinkInterval) {
        if (LeftLit && RightLit){
            digitalWrite (LeftLtPin, LOW);
            digitalWrite (RightLtPin, LOW);
            LeftLit = false;
            RightLit = false;
            FourWayBlinkTime = millis ();
          }
        else {
          digitalWrite (LeftLtPin, HIGH);
          digitalWrite (RightLtPin, HIGH);
          LeftLit = true;
          RightLit = true;
          FourWayBlinkTime = millis ();
        } // end if (LeftLit && RightLit) else
      } // end if ((uint32_t)(millis ()-FourWayBlinkTime)>FourWayBlinkInterval)
    } // end if (FourWay)
    if (Left) {
      if ((uint32_t)(millis ()-TurnBlinkTime)>TurnBlinkInterval) {
        if (LeftLit){
          digitalWrite (LeftLtPin, LOW);
          LeftLit = false;
          TurnBlinkTime = millis ();
        }
        else {
          digitalWrite (LeftLtPin, HIGH);
          LeftLit = true;
          TurnBlinkTime = millis ();
        } // end if (LeftLit) else
      } // end if ((uint32_t)(millis ()-TurnBlinkTime)>TurnBlinkInterval)
    } // end if (Left)
    if (Right) {
      if ((uint32_t)(millis ()-TurnBlinkTime)>TurnBlinkInterval) {
        if (RightLit){
          digitalWrite (RightLtPin, LOW);
          RightLit = false;
          TurnBlinkTime = millis ();
        }
        else {
          digitalWrite (RightLtPin, HIGH);
          RightLit = true;
          TurnBlinkTime = millis ();
        } // end if (RightLit)
      } // end if ((uint32_t)(millis ()-TurnBlinkTime)>TurnBlinkInterval)
    } // end if (Right) else
  } // end else !XcvrSleep
  loopCycles = loopCycles + 1;
} // end loop

void RECEIVE_MESSAGE (void){
  MsgReceived = false;
  byte pairs;
  boolean check = true;
// clear receive buffers, just in case.
  for (byte i = 0; i <= 7; i++){
    frame_data[i] = 0x00;
  }
  ReceiveFrame_id = 0x00;
  rx_status = CAN.readStatus();
  if (MSG_IN_BUFFER0){
    CAN.readDATA_ff_0(&length,frame_data,&ReceiveFrame_id, &ext, &filter);
//    printBuf(rx_status, length, ReceiveFrame_id, filter, 0, frame_data, ext);
    pairs = length/2;
    for (byte j=0; j<=(pairs-1); j++){
      if ((frame_data[j] ^ frame_data[j+pairs]) != 0xFF){
// following works, BUT ~frame_data[j] returns unsigned int unless (byte) used
//      if ((byte)~frame_data[j] == frame_data[j+pairs]){
        check = false;
        break;
      }
    }
  }
/* in original code from which I lifted this, the following was
"if" rather than "else if".  Doing that means that a lower-priority
(buffer 1) message could replace a higher priority (buffer 0) message
if both buffers were full.  Using "else if" will cause buffer 1 to be
read on next pass if another buffer 0 message has not arrived in the
meantime.
*/
  else if (MSG_IN_BUFFER1){
    CAN.readDATA_ff_1(&length,frame_data,&ReceiveFrame_id, &ext, &filter);
//    printBuf(rx_status, length, ReceiveFrame_id, filter, 1, frame_data, ext); 
    pairs = length/2;
    for (byte j=0; j<=(pairs-1); j++){
      if ((frame_data[j] ^ frame_data[j+pairs]) != 0xFF){
        check = false;
        break;
      }
    }
  }
  if ((MSG_IN_BUFFER0) || (MSG_IN_BUFFER1)){
    if (check){
      MsgReceived = true;
    } // end check for corrupted message
    else {
      MsgReceived = false;
    }
  } // filtered message in receive buffer
  else {
    MsgReceived = false;
  } // no filtered message in receive buffer
} // end RECEIVE_MESSAGE ()

void PARSE_ID (void){
  Category = (ReceiveFrame_id >> 9) & 0b11;
  Name = (ReceiveFrame_id >> 3) & 0b111111;
  Target = ReceiveFrame_id & 0b111;
} // end PARSE\_ID

void SEND_MESSAGE (byte Number, unsigned int ID, byte *DATA){
  SendFrame_id = ID;
/* void CANClass::load_ff_g(byte lengtange priorities. 
   Lower priority messages can be sent by calling e.g. load_ff_0  
   
   2012_1_9: transmission on buffers 0 and 1 work fine, but on burrer
   2 only the ID seems to be sent, but data received are all 0x0 and 
   complements = 0xFF?  For now, CAN.load__ff_g changed to send only to
   buffers 0 and 1.
*/
 CAN.load_ff_g(Number,&SendFrame_id,DATA, false);
// CAN.load_ff_1(Number,&SendFrame_id,DATA, false);
} // end SEND_MESSAGE

void XCVR (void){
  byte XcvrState = frame_data[0];
  if (XcvrState == Wake){
    digitalWrite (XcvrSleepPin, LOW);
    pinMode (XcvrSleepPin, INPUT);
    SETUP_CAN ();
    XcvrSleep = false;
    // added in case Master has re-started
    for (byte i = 1; i<=10; i++){
      SEND_MESSAGE (2, NodeLive_id, AuxStart);
      delay (10);
    }
  }
  else if (XcvrState == Sleep){ // added "else" to avoid testing if already know XCvrState==Wake
    digitalWrite (HdTailLtPin, LOW);
    digitalWrite (LeftLtPin, LOW);
    digitalWrite (RightLtPin, LOW);
    LeftLit = false;
    RightLit = false;
    Left = false;
    Right = false;
    FourWay = false;
    do { // make sure CAN.setMode(SLEEP) isn't blocked by messages in buffer
      RECEIVE_MESSAGE ();
    } while (MsgReceived);
    pinMode (XcvrSleepPin, OUTPUT);
    delay (10);
    digitalWrite (XcvrSleepPin, HIGH);
    delay (10);
    XcvrSleep = true;
    do { // make sure CAN.setMode(SLEEP) isn't blocked by messages in buffer
      RECEIVE_MESSAGE ();
    } while (MsgReceived);
    CAN.setMode(SLEEP);
    delay (1); // delay needed so Serial.print not blocked
    MCU_SLEEP ();
    ADCSRA = old_ADCSRA;
// returns here after interrupt
  }
  if (XcvrState == Shutoff){
    delay (100);
    digitalWrite (RESET_PIN, LOW);
// delay increased from 10 to ensure shutoff
    delay (100);
    pinMode (PowerOnPin, OUTPUT);
    delay (10);
    digitalWrite (PowerOnPin, HIGH);
    delay (1000);
  }
} // end XCVR

void MCU_SLEEP (void){
  // disable ADC
  ADCSRA = 0;  
  set_sleep_mode (SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  noInterrupts ();
  // will be called when INT_PIN goes low  
  attachInterrupt (1, MCU_WAKE, LOW);
  // turn off brown-out enable in software
  MCUCR = _BV (BODS) | _BV (BODSE);
  MCUCR = _BV (BODS); 
  // it is guaranteed that the sleep_cpu call will be done
  // as the processor executes the next instruction after
  // interrupts are turned on.
  interrupts ();
  sleep_cpu ();  
} // end of MCU_SLEEP

void MCU_WAKE (void){
  // cancel sleep as a precaution
  sleep_disable();
  delay (25);
  SETUP_CAN ();
  delay (25);
  detachInterrupt (1);
  digitalWrite (XcvrSleepPin, LOW);
  pinMode (XcvrSleepPin, INPUT);
  lastSetSlow = SetSlow;
  SpeedSlowArmed = true;
  delay (1);
  // added in case Master has re-started
  for (byte i = 1; i<=10; i++){
    SEND_MESSAGE (2, NodeLive_id, AuxStart);
    delay (10);
  }
}  // end of MCU_WAKE

void SET_LIGHTS (void){
  if ((frame_data[0] & B0001) == 0) {
//    Serial.println (F("Headlights ON"));
    digitalWrite (HdTailLtPin, HIGH);
  }
  else {
//    Serial.println (F("Headlights OFF"));
    digitalWrite (HdTailLtPin, LOW);
  }
  if ((frame_data[0] & B0010) == 0) {
    digitalWrite (LeftLtPin, HIGH);
    digitalWrite (RightLtPin, HIGH);
    LeftLit = true;
    RightLit = true;
    FourWay = true;
    FourWayBlinkTime = millis ();
//    Serial.println (F("4-ways ON"));
  }
  else {
//    Serial.println (F("4-ways OFF"));
    digitalWrite (LeftLtPin, LOW);
    digitalWrite (RightLtPin, LOW);
    LeftLit = false;
    RightLit = false;
    FourWay = false;
  }
  if (((frame_data[0] & B0100) == 0) && !FourWay) {
//    Serial.println (F("Left Turn ON"));
    digitalWrite (LeftLtPin, HIGH);
    LeftLit = true;
    Left = true;
    TurnBlinkTime = millis ();
  }
  else {
//    Serial.println (F("Left Turn OFF"));
    digitalWrite (LeftLtPin, LOW);
    LeftLit = false;
    Left = false;
  }
  if (((frame_data[0] & B1000) == 0) && !FourWay) {
//    Serial.println (F("Right Turn ON"));
    digitalWrite (RightLtPin, HIGH);
    RightLit = true;
    Right = true;
    TurnBlinkTime = millis ();
  }
  else {
//    Serial.println (F("Right Turn OFF"));
    digitalWrite (RightLtPin, LOW);
    RightLit = false;
    Right = false;
  }
  SEND_MESSAGE (2, Lights_ret, frame_data);
} // end SET_LIGHTS

void CK_SETSLOW (void) {
// force periodic update of setslow to undo false limit triggers
  if (loopCycles % 2000 == 0){
    SpeedSlowArmed = true;
    lastSetSlow = 0;
  }
  if (SpeedSlowArmed){
    analogRead (SpeedSlowPin); // read and discard
    unsigned long SumReadings = 0;
    unsigned int Reading = 0;
//    for (byte i = 1 ; i <= 5 ; i++){
    for (byte i = 1 ; i <= 10 ; i++){
      SumReadings = SumReadings + analogRead (SpeedSlowPin);
    }
//    Reading = SumReadings/5;
    Reading = SumReadings/10;
    if (Reading <= 100) {
      SetSlow = 0;
    }
    else if (Reading <= 550) { // 600
      SetSlow = 1;
    }
    else if (Reading <= 690){ // 700
      SetSlow = 2;
    }
    else {
      SetSlow = 3;
    }
  }
  if (SetSlow != lastSetSlow){
    SpeedSlowArmed = false;
    byte DATA[2];
    DATA[0] = SetSlow;
    DATA[1] = ~DATA[0];
    SEND_MESSAGE (2, SetSlow_id, DATA);
    if (!NoPrinting){
    }
  }
} // end CK_SETSLOW

void CK_VOLTS (void){
  byte NtoAverage = 10;
  analogRead (InVoltPin); // read and discard
  unsigned int InVolts = 0;
  unsigned long SumInVolts = 0;
  for (byte i = 1 ; i <= NtoAverage ; i++){
    SumInVolts = SumInVolts + analogRead (InVoltPin);
  }
  InVolts = SumInVolts/NtoAverage;
  analogRead (OutVoltPin); // read and discard
  unsigned int OutVolts = 0;
  unsigned long SumOutVolts = 0;
  for (byte i = 1 ; i <= NtoAverage ; i++){
    SumOutVolts = SumOutVolts + analogRead (OutVoltPin);
  }
  OutVolts = SumOutVolts/NtoAverage;
  byte DATA[8];
  DATA[0] = InVolts >> 8;
  DATA[1] = InVolts & 0b11111111;
  DATA[2] = OutVolts >> 8;
  DATA[3] = OutVolts & 0b11111111;
  for (byte i = 0; i < 4; i++){
    DATA[i+4] = ~DATA[i];
  }
  SEND_MESSAGE (8, Volts_ret, DATA);
} // end CK_VOLTS

void CK_BRAKES (void) {
  byte BrakeState;
  byte Brake1State = 1;
  byte Brake2State = 1;
  byte FirstRead = 1;
  byte SecondRead = 1;
  FirstRead = digitalRead(Brake1DetectPin);
  delay (1);
  SecondRead = digitalRead(Brake1DetectPin);
  Brake1State = FirstRead + SecondRead;
  bitWrite (BrakeState,0,Brake1State);
  FirstRead = digitalRead(Brake2DetectPin);
  delay (1);
  SecondRead = digitalRead(Brake2DetectPin);
  Brake2State = FirstRead + SecondRead;
  bitWrite (BrakeState,1,Brake2State);
  byte DATA[2];
  DATA[0] = BrakeState;
  DATA[1] = ~DATA[0];
  SEND_MESSAGE (2, Brakes_ret, DATA);
}// end CK_BRAKES

void SETUP_CAN (void) {
  // initialize CAN bus class
  // this class initializes SPI communications with MCP2515
  CAN.begin();
  CAN.setMode(CONFIGURATION);  // set to "NORMAL" for standard communications
  CAN.baudConfig(BUS_SPEED);
/*
   The lower the ID value, the higher the priority.
   Highest priority messages go first to buffer 0, then to buffer 1
      if buffer 0 is full (depends on MCP2151 register setting)
      or if buffer 0 filter not matched.
   If 2 messages arrive at the same time, higher priority msg goes to
      buffer 0, lower priority msg goes to buffer 1.
  "Category" in 2 msb of ID determines priority of Roboteq message groups: 
      0b00=SetCommand, 0b01=GetValue, 0b10 and 0b11 = configuration.
   Within a catagory, "Name" in next 6 bits determines priority.
   Thus, SetCommand is highest priority group, and _G (motor output setting = 0)
      has absolute highest priority.
*/
/*
fromAux B000  toAux B001  fromRoboteq B010  toRoboteq B011
fromMaster B100  toMaster B101  fromDisplay B110  toDisplay B111
*/
  CAN.setMaskOrFilter(MASK_0,   0b00000000, 0b11100000, 0b00000000, 0b00000000);
  CAN.setMaskOrFilter(FILTER_0, 0b00000000, 0b00100000, 0b00000000, 0b00000000); // toAux messages

  CAN.setMaskOrFilter(MASK_1,   0b00000000, 0b11100000, 0b00000000, 0b00000000); // will want to set to all 0 to accept all messages
                                                                                 // if more modules are used
  CAN.setMaskOrFilter(FILTER_2, 0b00000000, 0b00100000, 0b00000000, 0b00000000); // toAux messages
  CAN.setMode(NORMAL);  // set to "NORMAL" for standard com
} // end SETUP_CAN

void printBuf(byte rx_status, byte length, uint32_t ReceiveFrame_id, byte filter, byte buffer, byte *frame_data, byte ext) {
   Serial.print(F("[Rx] Status:"));
   Serial.print(rx_status,HEX);
   Serial.print(F(" Len:"));
   Serial.print(length,HEX);
   Serial.print(F(" Frame:0x"));
   Serial.print(ReceiveFrame_id,HEX);
   Serial.print(F(" EXT?:"));
   Serial.print(ext==1,HEX);
   Serial.print(F(" Filter:"));
   Serial.print(filter,HEX);
   Serial.print(F(" Buffer:"));
   Serial.print(buffer,HEX);
   Serial.print(F(" Data:[0x"));
  for (int i=0;i<8;i++) {
    if (i>0){
      Serial.print(F(" 0x"));
    }
     Serial.print(frame_data[i],HEX);
  }
   Serial.println(F("]"));
} // end printBuf

/*

2013_05_25 added Roboteq_PWR and CAN message handling
   to turn off Roboteq control power -- see 2014_07_17 for change
2013_05_29 AuxNode program separated from RoboteqEmulator -- 
    will run on Nano CAN node while RoboteqEmulator will
    become Roboteq script
    functions that will be handled by AuxNode
      e.g. Roboteq_PWR controller-power relay
           speed inhibits
           shock/stability sensing
           possibly seat actuator outputs (currently Roboteq digout)
           possibly current sensing for individual brakes, actuators
2013_05_31 Added:
             do { // make sure CAN.setMode(SLEEP) isn't blocked by messages in buffer
               RECEIVE_MESSAGE ();
             } while (MsgReceived);
             in XCVR to dump messages left in buffers
2013_06_06 Added:
        CHECK_SETSLOW in Master to query AuxNode for SetSlow, case SetSlow to modify joystick values,
            and to send messages to display
        CK_SETSLOW in AuxNode with cycling 0, 1, 2, 3 every 9 seconds for testing
2013_06_21 Checking for slow or inhibit (i.e. SetSlow) changed from a response to a query
            from master to sending a fromAux _SLOW message any time SendSlow != lastSendSlow.
            When MASTER gets the fromAux _SLOW it sends a toAux _SLOW.  If the toAux _SLOW
            is NOT received, lastSetSlow remains unchanged and the SetSlow message will be 
            re-sent on the next loop cycle.  Once toAux _SLOW is receive, lastSetSlow = SetSlow
            and no _SLOW message will be sent fromAux until SetSlow again changes.
2013_10_06 Added Shutdown enumeration for non-Master XCVR messaging.  In the non.Master
             nodes, this pulls the PowerOnPin (currently A1) to HIGH.  Connected 
             base of transistor that pulls down pin Vr of the DC-DC converter
             for that node.  This gives a very positive power turn off.  
2014_02_16 Cycling of SetSlow 0-1-2-3 for demo purposes changed to a separate
             interval for 0 (stop).  See CK_SETSLOW.
2014_03_04 Added delay at two places in _XCVR shutdown.  If no delay,
             PowerOnPin does not stay high long enough to reliabl shut
             Recom DC-DC converter. Minimum delay is 6 msec, set at 10.
2014_04_14 Moved lights control from RoboteqEmulator to Aux and implemented
             actual control via STM VN540 high-side solid state switches
2014_04_28 Wired inhibit/slow switches to analog pin in Aux and corrected code
             to analog read once and send many times until confirmation
             comes from Master.  Added code to turn ADC back on after 
             waking from sleep.  Master responds very quickly, but display
             has significant lag.
2014_07_17 Roboteq MCU relay now powered directly from Aux  DC-DC converter
             5V rather than digital pin so that Roboteq turns on simultaneously
             with Aux Node rather than waiting until Aux Node is rfully awake.
2014_09_17 Added:
      lastSetSlow = SetSlow;
      SpeedSlowArmed = true;
             in if (XcvrSleep) section of loop () to avoid blocking by
             SetSlow confirmation that arrives before fully awake.

2014_11_19 Added in CK_SETSLOW ():
// force periodic update of setslow to undo false limit triggers
  if (loopCycles % 2000 == 0){
    SpeedSlowArmed = true;
    lastSetSlow = 0;
  }
2015_01_01   added:
             while ((!DisplayLive) || (!AuxLive) || (!RoboteqLive)){
               RECEIVE_MESSAGE ();
             }
             in Master and corresponding messaging in Aux, Display and Roboteq
               to hold Master in Setup until other nodes are live rather than
               using arbitrary delay.
2015_02_14 Constants.h, labels.h (Programmer node) and MsgIDs.h moved to WC_CAN library
           DO NOT UPLOAD WITH Arduino 1.6.0 
             -1- TURN_OFF OF Aux AND Display FROM Master DOES NOT WORK
             -2- Programmer RE-BOOTS AFTER VOLTAGE CALIBRATION STEP
             ERRORS IN ARRAY INDEXING FIXED AND NOW COMPILES WITH Arduino 1.6.x
2015_10_07 XCVR ( changed to use RESET_PIN, LOW to turn off transceiver so it
            can't receive any extraneous messages after getting a Shutoff msg.
2015_10_13 Changed order of receive buffer emptying and added extra delay in XCVR ()
             to make going into sleep more reliable.
2016_02_28 Moved VoltageDividerOffset from Master to Aux
           moved defines to Defines.h tab
2017_05_26 BrakeXDetectPins changed to INPUT_PULLUP to work
           with STM VN5160STR-E high side switch instead
           of voltage dividers.
2017_07_09 adding:
                   delay (25);
                   SETUP_CAN ();
                   delay (25);
           in MCU_WAKE () here and in Aux cures wake from sleep problem
2017_07)10 added #define BrakeLock 2 to lock brake during startup
2018_10_06 added:
     for (byte i = 1; i<=10; i++){
       SEND_MESSAGE (2, NodeLive_id, AuxStart);
       delay (10);
     }
   in three places in case Master has re-started
 */

