// pins 0 & 1 reserved for serial

// with fresh Nano, use:
#define BrakeLock 2 // pull low to lock brake during startup
#define XcvrRxDPin 3 // = interrupt 1
#define Brake1DetectPin 4
#define Brake2DetectPin 5
#define RightLtPin 6
#define LeftLtPin 7
#define HdTailLtPin 8
#define PowerOnPin 9 // OUTPUT HIGH to turn DC-DC converter OFF
// in Master PowerOnPin OUTPUT HIGH is used to hold DC-DC converter ON
// pins 10, 11, 12, 13 used for SPI
#define InVoltPin A0
#define OutVoltPin A1
// pin A2 used by CAN.h for MCP2151 reset
#define XcvrSleepPin A3
#define SpeedSlowPin A4
#define AccelXPin A5
#define AccelYPin A6 // analog only
#define AccelZPin A7 // analog only

// digital output pins
#define Open false
#define Closed true
// sleep controls
#define Sleep 0
#define Wake 1
#define Shutoff 2

#define MSG_IN_BUFFER0 (rx_status & 0x40)
#define MSG_IN_BUFFER1 (rx_status & 0x80)



