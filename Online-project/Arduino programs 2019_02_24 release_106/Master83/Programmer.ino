void Calibration (void){
  CalibrationRunning = true;
  Next_step (1, ThrottlePin);
  Next_step (4, SteeringPin);
  Next_step (2, ThrottlePin);
  Next_step (3, ThrottlePin);
  Next_step (5, SteeringPin);
  Next_step (6, SteeringPin);
  Next_step (7, SpeedPotPin);
  Next_step (8, SpeedPotPin);
  CalibrationRunning = false;
  GET_CALIBRATION (); // in JoystickProcedures tab
} // end Calibration

void Next_step (byte Step, byte PinToRead){
  byte INT_DATA[4];
  byte MSG_DATA[2];
  byte MsgNo = Step*3-2;
  byte Eaddress = Step*2-2;
  int Value;
  static boolean SkipFlag = false;
  if (Step != 4){
    do {
      ModePressed = digitalRead (ModePin);
      FwdPressed = digitalRead (FwdPin);
      delay (2);
    } while ((ModePressed == not_pressed) && (FwdPressed == not_pressed));
    if (FwdPressed == pressed){
      SkipFlag = true;
    }
    else {
      SkipFlag = false;
    }
  }
  else if ((Step == 4) && !SkipFlag){
    ModePressed = pressed;
  }
  if (ModePressed == pressed){
// stored Value
    MSG_DATA[0] = MsgNo;
    MSG_DATA[1] = ~MSG_DATA[0];
    SEND_MESSAGE (2, Calibration_ret, MSG_DATA);
    delay (10);
    INT_DATA[0] = EEPROM.read (CalibrationAddress+Eaddress);
    INT_DATA[1] = EEPROM.read (CalibrationAddress+Eaddress+1);
    INT_DATA[2] = ~INT_DATA[0];
    INT_DATA[3] = ~INT_DATA[1];
    SEND_MESSAGE (4, Value_ret, INT_DATA);
    delay (10);
// new Value
    MSG_DATA[0] = MsgNo+1;
    MSG_DATA[1] = ~MSG_DATA[0];
    SEND_MESSAGE (2, Calibration_ret, MSG_DATA);
    delay (10);
    Value = ReadValue (PinToRead);
    INT_DATA[0] = highByte (Value);
    INT_DATA[1] = lowByte (Value);
    INT_DATA[2] = ~INT_DATA[0];
    INT_DATA[3] = ~INT_DATA[1];
    EEPROM.update (CalibrationAddress+Eaddress, INT_DATA[0]);
    EEPROM.update (CalibrationAddress+Eaddress+1, INT_DATA[1]);
    delay (5);
    SEND_MESSAGE (4, Value_ret, INT_DATA);
    delay (10);
  }
  delay (100);
  do {
    ModePressed = digitalRead (ModePin);
    FwdPressed = digitalRead (FwdPin);
    delay (2);
  } while ((ModePressed == pressed) || (FwdPressed == pressed));
  MSG_DATA[0] = MsgNo+2;
  MSG_DATA[1] = ~MSG_DATA[0];
  SEND_MESSAGE (2, Calibration_ret, MSG_DATA);
  delay (10);
  lastCANMessageTime = millis ();
} // end Next_step

int ReadValue (byte pin){
  int Result = 0;
  long sumRaw = 0;
// average over Damping reads
  analogRead(pin);
  for (byte I = 0 ; I < Damping ; I++) {
    sumRaw = sumRaw + analogRead(pin);
  }
  Result = sumRaw/Damping;
  Result = map(Result, 0, 1023, 0, 5000); // scale to approiximate mV
  return Result;
}

void GET_UsrSettings (void){
  byte DATA [4];
  UsrFromEEPROM ();
  delay (10);
  for (byte i = 0; i<= sizeof(UserSettings)-1; i++){
    DATA[0] = i;
    DATA[1] = UserSettings[i];
    DATA[2] = ~DATA[0];
    DATA[3] = ~DATA[1];
    SEND_MESSAGE (4, Usr_ret, DATA);
    delay (5);
  } // end of sending User Settings, now send NewMCUflag
  DATA[0] = sizeof(UserSettings);
  DATA[1] = EEPROM.read (NewMCUFlag);
  DATA[2] = ~DATA[0];
  DATA[3] = ~DATA[1];
  SEND_MESSAGE (4, Usr_ret, DATA);
  delay (5);
  StartGetUsrSettings = false;
  lastCANMessageTime = millis ();
} // end GET_UsrSettings

void BLOCK_For_Download (void){
  do {
    RECEIVE_MESSAGE ();
    if ((millis()-AwakeTime)>=1000){
      SEND_MESSAGE (0,UsrSettings_ret,NULL);
      AwakeTime = millis();
    }
  } while ((ProgrammerRunning)&&(!UsrDownloadDone));
} // end BLOCK_For_Download

void BLOCK_For_Programmer (void){
  do {
    RECEIVE_MESSAGE ();
    if ((millis()-AwakeTime)>=1000){
      SEND_MESSAGE (0,UsrSettings_ret,NULL);
      AwakeTime = millis();
    }
  } while (ProgrammerRunning);
} // end BLOCK_For_Programmer

void GET_VoltCalibration (void) {
  byte DATA[8];
  int StoredVoltCalibration = EEPROM.read(VoltCalibrationAddress)<<8|EEPROM.read(VoltCalibrationAddress+1);
  int deciVolts = map (InVolts, 0, 1023, 0, VoltCalibration);
  DATA[0] = highByte (VoltCalibration);
  DATA[1] = lowByte (VoltCalibration);
  DATA[2] = highByte (deciVolts);
  DATA[3] = lowByte (deciVolts);
  for (byte i=0 ; i<=3 ; i++){
    DATA[i+4] = ~DATA[i];
  }
  delay (10);
  for (byte i=1 ; i<=5 ; i++){
    SEND_MESSAGE (8,VoltCalib_ret,DATA);
    delay (10);
  }
  lastCANMessageTime = millis ();
} // end GET_VoltCalibration

void SET_VoltCalibration (void) {
  int StoredVoltCalibration = (int)(EEPROM.read(VoltCalibrationAddress)<<8)
                               | EEPROM.read(VoltCalibrationAddress+1);
  int NewVoltCalibration = (int)(frame_data[1]<<8)|frame_data[2];
  if ((NewVoltCalibration != 0) && (NewVoltCalibration != StoredVoltCalibration)){
    VoltCalibration = NewVoltCalibration;
    EEPROM.update (VoltCalibrationAddress,highByte(VoltCalibration));
    EEPROM.update (VoltCalibrationAddress+1,lowByte(VoltCalibration));
  }
  lastCANMessageTime = millis ();
} // end SET_VoltCalibration

void SET_OffsetCalibration (void) {
  static boolean OffsetCalibrationDone = false;
  int oldOutVolts = OutVolts;
  boolean Stable = false;
  if (!OffsetCalibrationDone){
    OffsetCalibrationDone = true;
    VoltageDividerOffset = 0;
    EEPROM.update (VoltCalibrationAddress+2,highByte(VoltageDividerOffset));
    EEPROM.update (VoltCalibrationAddress+3,lowByte(VoltageDividerOffset));
    Steering = 1;
    do {
      JoystickConfirmed = false;
      SEND_JOYSTICK_MESSAGE ();
      Serial.println (F("****************************************"));
      Serial.println (F("Steering = 1 sent"));
      Serial.println (F("****************************************"));
      delay (5);
      RECEIVE_MESSAGE ();
    } while (!JoystickConfirmed);
    delay (50);
    do {
      oldOutVolts = OutVolts;
      LongReceived = false;
      SEND_MESSAGE (0, Volts_id, NULL);
      RECEIVE_MESSAGE ();
      if (LongReceived) {
        int diff = oldOutVolts-OutVolts;
        Stable = (abs(diff)<=2);
      }
    } while ((abs(InVolts-OutVolts)>5) || (!Stable));
    VoltageDividerOffset = InVolts-OutVolts;
    EEPROM.update (VoltCalibrationAddress+2,highByte(VoltageDividerOffset));
    EEPROM.update (VoltCalibrationAddress+3,lowByte(VoltageDividerOffset));
  // open contactor
    for (byte i = 1; i<=5; i++){
      Steering = 0;
      SEND_JOYSTICK_MESSAGE ();
      delay (10);
    }
  }
  byte DATA[8];
  DATA[0] = 0;
  DATA[1] = 0;
  DATA[2] = highByte (VoltageDividerOffset);
  DATA[3] = lowByte (VoltageDividerOffset);
  for (byte i=0 ; i<=3 ; i++){
    DATA[i+4] = ~DATA[i];
  }
  delay (10);
  for (byte i=1 ; i<=5 ; i++){
    SEND_MESSAGE (8,VoltCalib_ret,DATA);
    delay (10);
  }
  lastCANMessageTime = millis ();
} // end SET_OffsetCalibration




