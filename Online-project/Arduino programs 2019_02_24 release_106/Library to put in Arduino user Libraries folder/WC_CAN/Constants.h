#ifndef __Constants__
#define __Constants__
/* MODIFIED WITH ADDITIONS FROM Roboteq Constants.h */
/* last updated 2015_04_01 */

#define BUS_SPEED 500

/******************** frame ID components ********************/
// Category (top 2 bits)
#define SetCommand B00 // highest transmit priority
#define GetValue B01
#define Config B10 // SetConfig constants 0 to 63
#define Config2 B11 // SetConfig constants >= 64
// Config and Config2 also used for Display messages

// Target (bottom 3 bits)
#define toProgrammer B000
#define toAux B001
// #define fromRoboteq B010
#define toRoboteq B011
// #define fromMaster B100
#define toMaster B101
// #define fromDisplay B110
#define toDisplay B111

// Name (central six bits)

/******************** SetCommand Device ********************/
#define _G  0
#define _M  1
#define _P  2
#define _S  3
#define _C  4
#define _CB  5
#define _VAR  6
#define _AC  7
#define _DC  8
#define _DS  9

#define _D1  10
#define _D0  11
#define _H  13
#define _EX  14
#define _MG  15
#define _MS  16
#define _PR  17
#define _PX  18
#define _PRX  19

#define _AX  20
#define _DX  21
#define _B  22
#define _SX  23
#define _CS  24
#define _RC  26
#define _EES  27
#define _BND  28
#define _ST 29

#define _LTS 30
#define _XC 31
#define _PT 32 // not in Roboteq Constants.h, in script is PTHROTTLE
#define _POT 33 // not in Roboteq Constants.h, in script is POT

/******************** SetCommand Alias ********************/
#define _GO  0
#define _MOTCMD  1
#define _MOTPOS  2
#define _MOTVEL  3
#define _SENCNTR  4
#define _SBLCNTR  5
#define _VAR  6
#define _ACCEL  7
#define _DECEL  8
#define _DOUT  9

#define _DSET  10
#define _DRES  11
#define _HOME  13
#define _ESTOP  14
#define _MGO  15
#define _MSTOP  16
#define _MPOSREL  17
#define _NXTPOS  18
#define _NXTPOSR  19

#define _NXTACC  20
#define _NXTDEC  21
#define _BOOL  22
#define _NXTVEL  23
#define _CANSEND  24
#define _RCOUT  26
#define _EESAV  27
#define _BIND  28
#define _SEATCMD 29

#define _LIGHTSCMD 30
#define _XCVR 31
#define _PTHROTTLE 32 // not in Roboteq Constants.h, in script is PTHROTTLE
#define _POT 33 // not in Roboteq Constants.h, in script is POT

/******************** GetValue Device ********************/
#define _A  0
#define _M  1
#define _P  2
#define _S  3
#define _C  4
#define _CB  5
#define _VAR  6
#define _SR  7
#define _CR  8
#define _CBR  9

#define _BS  10
#define _BSR  11
#define _BA  12
#define _V  13
#define _D  14
#define _DI  15
#define _AI  16
#define _PI  17
#define _T  18
#define _F  19

#define _FS  20
#define _FF  21
#define _B  22
#define _DO  23
#define _E  24
#define _CIS  25
#define _CIA  26
#define _CIP  27
#define _TM  28
#define _LK  29

#define _TR  32
#define _K  33
#define _DR  34
#define _AIC  35
#define _PIC  36
#define _MA  37
#define _CL  38
#define _CAN  39

#define _CF  40
#define _MGD  41
#define _MGT  42
#define _MGM  43
#define _MGS  44
#define _MGY  45
#define _AWK 44 // OVERLOADED USE OF SAME NUMBER, BUT USED ONLY IN MY NODES
#define _SL 45 // OVERLOADED USE OF SAME NUMBER, BUT USED ONLY IN MY NODES
#define _FM  48
#define _HS  49

/******************** GetValue Alias ********************/
#define _MOTAMPS  0
#define _MOTCMD  1
#define _MOTPWR  2
#define _ABSPEED  3
#define _ABCNTR  4
#define _BLCNTR  5
#define _VAR  6
#define _RELSPEED  7
#define _RELCNTR  8
#define _BLRCNTR  9

#define _BLSPEED  10
#define _BLRSPEED  11
#define _BATAMPS  12
#define _VOLTS  13
#define _DIGIN  14
#define _DIN  15
#define _ANAIN  16
#define _PLSIN  17
#define _TEMP  18
#define _FEEDBK  19

#define _STFLAG  20
#define _FLTFLAG  21
#define _BOOL  22
#define _DIGOUT  23
#define _LPERR  24
#define _CMDSER  25
#define _CMDANA  26
#define _CMDPLS  27
#define _TIME  28
#define _LOCKED  29
#define _BRAKES 29 // OVERLOADED USE OF SAME NUMBER, BUT USED ONLY IN MY NODES

#define _TRACK  32
#define _SPEKTRUM  33
#define _DREACHED  34
#define _ANAINC  35
#define _PLSINC  36
#define _MEMS  37
#define _CALIVE  38
#define _CAN  39

#define _CF  40
#define _MGDET  41
#define _MGTRACK  42
#define _MGMRKR  43
#define _PITCH  43 // OVERLOADED USE OF SAME NUMBER, BUT USED ONLY IN MY NODES
#define _MGSTATUS  44
#define _MGYRO  45
#define _AWAKE 44 // OVERLOADED USE OF SAME NUMBER, BUT USED ONLY IN MY NODES
#define _SLOW 45 // OVERLOADED USE OF SAME NUMBER, BUT USED ONLY IN MY NODES
#define _MOTFLAG  48
#define _HSENSE  49

/******************** SetConfig/GetConfig Alias ********************/
// Roboteq configuration for Accel/Decel mixing, boost, pin assignments
// motor wiring and MotorCompensation
#define _SENSOR 40 // LimpSpeed, UseCurrentSensors, L&R sensor polarity, overloaded with _MXPF
#define _MCOMP 62 // MotorResistance, MotorCompBoost, overloaded with _BHL
#define _MCOMP2 63 // MotorCompDeadband, TurnCompBoost & TurnAccelBoost, overloaded with _BLLA
#define _BOOSTS 55 // AccelMix, SpeedBoost, TurnBoost & MidBoost, overloaded with _MXTRN
#define _LIMITS 56 // BackStickBraking, BoostLimit & MidBoostLimit, overloaded with _CLERD
#define _PINS1 57 // seat tilt and lift digout pins, overloaded with _BPOL
#define _PINS2 58 // current sensor analog inputs and brake digout pins, overloaded with _BLSTD
#define _MOTORS 59 // SwapMotors, M1 & M2 Polarity, Contactor digout pin, overloaded with _BLFB

// from Robogteq API Constants.h
#define _EE  0 // write user data to Flash
#define _BKD  1 // brake activation delay
#define _OVL  2 // overvoltage limit
#define _UVL  3 // undervoltage limit
#define _THLD  4 // short circuit detection limit
#define _MXMD  5 // mix mode
#define _PWMF  6 // PWM frequency
#define _CPRI  7 // command priorities
#define _RWD  8 // serial watchdog
#define _ECHOF  9 // enable/disable serial echo

#define _RSBR  10 // not in manual
#define _ACS  11 // analog center safety
#define _AMS  12 // analog within min/max safety
#define _CLIN  13 // command linearity
#define _DFC  14 // default command value
#define _DINA  15 // digital input action
#define _DINL  16 // digital input level
#define _DOA  17 // digital output action
#define _DOL  18 // digital output level
#define _AMOD  19 // analog input mode

#define _AMIN  20 // analog minimum
#define _AMAX  21 // analog maximum
#define _ACTR  22 // analog center
#define _ADB  23 // analog deadband
#define _ALIN  24 // analog linearity
#define _AINA  25 // analog input actions
#define _AMINA  26 // action on analog minimum
#define _AMAXA  27 // action on analog maximum
#define _APOL  28 // analog input polarity
#define _PMOD  29 // pulse input mode

#define _PMIN  30 // pulse minimum
#define _PMAX  31 // pulse maximum
#define _PCTR  32 // pulse center
#define _PDB  33 // pulse deadband
#define _PLIN  34 // pulse linearity
#define _PINA  35 // pulse input actions
#define _PMINA  36 // action on pulse min
#define _PMAXA  37 // action on pulse max
#define _PPOL  38 // pulse input polarity
#define _MMOD  39 // motor operating mode

#define _MXPF  40 // motor max power forward
#define _MXPR  41 // motor max power reverse
#define _ALIM  42 // amp limit
#define _ATRIG  43 // amps trigger value
#define _ATGA  44 // amps trigger action
#define _ATGD  45 // amps trigger delay
#define _KP  46 // PID proportional gain
#define _KI  47 // PID integral gain
#define _KD  48 // PID differential gain
#define _PIDM  49 // not in manual

#define _ICAP  50 // PID integral cap
#define _MAC  51 // motor acceleration
#define _MDEC  52 // motor deceleration
#define _MVEL  53 // motor default position velocity
#define _MXRPM  54 // motor RPM at 100%
#define _MXTRN  55 // number of motor turns between limits
#define _CLERD  56 // closed loop error detection
#define _BPOL  57 // brushless number of poles and speed polarity
#define _BLSTD  58 // brushless stall detection
#define _BLFB  59 // encoder or hall sensor feedback

#define _BHOME  60 // brushless counter load at home position
#define _BLL  61 // brushless counter low limit
#define _BHL  62 // brushless counter high limit
#define _BLLA  63 // brushless counter action at low limit
#define _BHLA  64 // brushless counter action at high limit
#define _SXC  65
#define _SXM  66

#define _EMOD  72
#define _EPPR  73
#define _ELL  74
#define _EHL  75
#define _ELLA  76
#define _EHLA  77
#define _EHOME  78
#define _SKUSE  79

#define _SKMIN  80
#define _SKMAX  81
#define _SKCTR  82
#define _SKDB  83
#define _SKLIN  84
#define _CEN  85
#define _CNOD  86
#define _CBR  87
#define _CHB  88
#define _CAS  89

#define _CLSN  90
#define _CSRT  91
#define _CTPS  92
#define _SCRO  93
#define _BMOD  94
#define _BADJ  95
#define _BADV  96
#define _BZPW  97

/******************** Display Node Messages ********************/
/* Information Messages */
#define _STARTUP 0 // setup finished and ready to enter loop
#define _AHR 1 // 0-100% remaining
#define _DVOLTS 2 // 0-100% (from 10% to 100% SOC)
#define _MODE 4 // 10*UsrToggle+Mode (UsrToggle=data/10, Mode=data%10)
#define _SPEEDPOT 5 // 0 to 100% of scale
#define Stop 0
#define VerySlow 1
#define Slow 2
#define FullSpeed 3
#define _SETSLOW 6
#define _OON 7 // 1=OutOfNeutral on Startup, 0=joystick neutral
#define _SWCLOSED 8 // !0=switch closed during Startup-sends SwitchFlags, 0=switches open
#define _RUNTIME 9 // 3 bytes: run time since start 2 for hours, 1 for minutes
#define _TOTTIME 10 // 3 bytes: total PM time 2 for hours, 1 for minutes
#define _JOYSTICK 11 // 1 byte: Throttle<< 4|Steering (re-scaled to positive 0-100 values)
#define _SEAT 12 // 1 byte containing seat flags
#define _LIGHTS 13 // 1 byte containing lights flags
#define _RAWVOLTS 14 // 2 int, 4 bytes

/* Error Messages */
#define _MOTOPEN 20 // 0=left motor open, 1=right, 2=both
                    // no bmp or text yet
#define BrakesOK 0
#define LeftBrake 1
#define RightBrake 2
#define BothBrakes 3
#define _BRKOPEN 21 // 0=erase, 1=left, 2=right open, 3=both open
#define _BRKSHRT 22 // 0=erase, 1=left, 2=right short, 3=both shorted
#define ContactorOK 0
#define StuckOpen 1
#define Fused 2
#define _CONTACT 23
#define _CHARGER 24 // 0=not connected, 1=connected
#define _BRAKESET 25  // 0=left, 1=right failed to set
#define _JSTICK 26 // 0=Throttle fault, 1=Steering fault
#define _POTFAULT 27 // 0=low fault, 1=high fault, >1=fixed
#define _FFLAG 28 // 8 values in 1 byte
#define _SFLAG 29 // 8 values in 1 byte
#define _WDGERR 30
#define _RPOWER 31
#define _EEPROM 32
#define _SENSORFLT 33 // current sensor fault msg from Roboteq
#define _SEATMSG 34
#define _CONMSG 35 // Failed to retrieve contactor state not changed
#define _BRKMSG 36
#define _VOLTERR 37
#define _AMPERR 38
#define _RSTART 39
#define _AUXSTART 40
#define _RTIMEERR 41
#define _ACTOVRLD 42 // no bmp yet
#define _LTSMSG 43
#define _CALIB 44 // no bmp and no text
// OTHER ERROR MESSAGES TO BE ADDED

/***************** Programmer Node Messages *****************/
#define _LOG 50 // signals Display to send LogFile, tells Programmer
  // that there's incoming log file information
#define _CAL 0 // send as config
#define _CalValue 1
#define _USR 2 // send as config
#define _UsrRet 3 // single byte of a user settings value
#define _StartStop 4 // single byte: 0 = programmer not running, 1 = running
#define _CkJS 5
#define _Msg 6
/*
#define _Steering0 2
#define _ThrottleFwd 3
#define _ThrottleRev 4
#define _SteeringLft 5
#define _SteeringRt 6
#define _PotMax 7
#define _PotMin 8
*/

#endif
















