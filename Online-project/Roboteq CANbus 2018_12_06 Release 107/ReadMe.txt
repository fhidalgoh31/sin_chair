Profile WC_CAN xxxx.xml includes settings for analog joystick 
and speed pot connections (AIN1, AIN2 and AIN3) for testing 
non-CANbus scripts.

Non CANbus versions of the script and profile, named "Analog" are included
for those who wish to use this script with direct analog wiring.  THESE
VERSIONS OF THE SCRIPT AND PROFILE HAVE NOT YET BEEN TESTED ON AN
ACTUAL WHEELCHAIR!  Please be cautious.

Roboteq has been known to change xml format
from time to time, this file is formatted for RoborunPlus V17 or V18.