*.sch and *.pcb files are in DesignSpark format.  Some of them will not
open in versions of DesignSpark older than V7.

CAN bd10 th xtal.pcb is drawn to use either a thru hole crystal or a 4 pad
SMD crystal.  IMPORTANT: if an SMD crystal is used it MUST NOT BE LOW-DRIVE-POWER!
(I had intended to use an SMD. RS promised them, but never got them in stock, so I
ended up using thru-hole.)

Hi-side switch is used to PWM control TFT screen backlight. (Original connection to
Vdd is interrupted to install this and components were glued to TFT
board and hand wired.)