void GET_CALIBRATION (void){
  Throttle0Cal = (EEPROM.read (CalibrationAddress))<<8 | EEPROM.read (CalibrationAddress+1);
  ThrottleFwdCal = (EEPROM.read (CalibrationAddress+2))<<8 | EEPROM.read (CalibrationAddress+3);
  ThrottleRevCal = (EEPROM.read (CalibrationAddress+4))<<8 | EEPROM.read (CalibrationAddress+5);
  Steering0Cal = (EEPROM.read (CalibrationAddress+6))<<8 | EEPROM.read (CalibrationAddress+7);
  SteeringLftCal = (EEPROM.read (CalibrationAddress+8))<<8 | EEPROM.read (CalibrationAddress+9);
  SteeringRtCal = (EEPROM.read (CalibrationAddress+10))<<8 | EEPROM.read (CalibrationAddress+11);
  PotMinCal = (EEPROM.read (CalibrationAddress+12))<<8 | EEPROM.read (CalibrationAddress+13);
  PotMaxCal = (EEPROM.read (CalibrationAddress+14))<<8 | EEPROM.read (CalibrationAddress+15);
  NotCalibrated = false;
  if ((Throttle0Cal<=0)||(ThrottleFwdCal<=0)||(Steering0Cal<=0)||(SteeringLftCal<=0)||(SteeringRtCal<=0)
         ||(PotMaxCal<=0)||(PotMinCal<=0)){
    NotCalibrated = true;
  }
  if ((Throttle0Cal>=5000)||(ThrottleFwdCal>=5000)||(Steering0Cal>=5000)||(SteeringLftCal>=5000)
         ||(SteeringRtCal>=5000)||(PotMaxCal>=5000)||(PotMinCal>=5000)){
    NotCalibrated = true;
  }
  if (NotCalibrated){
    Throttle0Cal = 2492;
    ThrottleFwdCal = 4800;
    ThrottleRevCal = 200;
    Steering0Cal = 2468;
    SteeringLftCal = 4800;
    SteeringRtCal = 200;
    PotMinCal = 200;
    PotMaxCal = 4800;
  }
  if (ThrottleRevCal > ThrottleFwdCal){
    ThrottleMaxCal = ThrottleRevCal;
    ThrottleMinCal = ThrottleFwdCal;
    ThrottleSense = -1;
  }
  else {
    ThrottleMaxCal = ThrottleFwdCal;
    ThrottleMinCal = ThrottleRevCal;
    ThrottleSense = 1;
  }
  if (SteeringLftCal > SteeringRtCal){
    SteeringMaxCal = SteeringLftCal;
    SteeringMinCal = SteeringRtCal;
    SteeringSense = -1;
  }
  else {
    SteeringMaxCal = SteeringRtCal;
    SteeringMinCal = SteeringLftCal;
    SteeringSense = 1;
  }
} // end GET_CALIBRATION

void READ_JOYSTICK (void){
  Throttle = 0; // to make sure old value can't be repeated
  Steering = 0; // to make sure old value can't be repeated
  int RawThrottle, RawSteering;
  READ_JOYSTICK_PINS (&RawThrottle, &RawSteering);
  Throttle = CURVE_JOYSTICK (RawThrottle, 0) * ThrottleSense;
  Steering = CURVE_JOYSTICK (RawSteering, 1) * SteeringSense;
  if (SpeedPotInstalled){
    if (LoopCycles % CyclesPerSpeedPotRead == 0) 
    {
      READ_SPEEDPOT ();
    } // end of LoopCycles check
    if (!NoPrinting){
      // Serial.print (F("    SpeedPot = "));
      // Serial.print (SpeedPot);
      // Serial.print (F("    TurnPot = "));
      // Serial.print (TurnPot);
    }
  } // end of if SpeedPotInstalled
  Throttle = (((long) SpeedPot*(long) Throttle)/100);
  Steering = (((long) TurnPot*(long) Steering)/100);
// reduce turn rate as speed increases; especially for FWD
// TurnAtFullSpeed 100=no reduction with speed,
//                 0=turn goes linearly to 0 at full speed
  byte RelSpeed; // fraction of maximum speed (1000 or -1000)
  byte TurnReduction = 0;
  if (Throttle < 0){
    RelSpeed = ((long)Throttle*100)/-1000;
  }
  else {
    RelSpeed = ((long)Throttle*100)/1000;
  }
  TurnReduction = ((long)RelSpeed*(long)(100-TurnAtFullSpeed))/100;
  Steering = (((long)(100-TurnReduction)*(long)Steering)/100);
} // end of READ_JOYSTICK

void READ_JOYSTICK_PINS (int * RawThrottle, int * RawSteering) {
  long sumRawThrottle = 0;
  long sumRawSteering = 0;
  static byte JSErr = 0;
  boolean ThrottleErr = false;
  boolean SteeringErr = false;
// average over 5 reads
// separately average Throttle and Steering and discard first read
// to avoid effect of delay in switching ADC channels
  analogRead(ThrottlePin);
  for (byte I = 0 ; I < 5 ; I++) {
    sumRawThrottle = sumRawThrottle + analogRead(ThrottlePin);
  }
  analogRead(SteeringPin);
  for (byte I = 0 ; I < 5 ; I++) {
    sumRawSteering = sumRawSteering + analogRead(SteeringPin);
  }
  *RawThrottle = sumRawThrottle/5;
  *RawThrottle = map(*RawThrottle, 0, 1023, 0, 5000); // scale to mV
  *RawSteering = sumRawSteering/5;
  *RawSteering = map(*RawSteering, 0, 1023, 0, 5000); // scale to mV
// check for out-of-range throttle and steering
  ThrottleErr = (*RawThrottle<(ThrottleMinCal/2)) || (*RawThrottle>(ThrottleMaxCal+(5000-ThrottleMaxCal)/2));
  if (ThrottleErr) {
    *RawThrottle = Throttle0Cal;
    *RawSteering = Steering0Cal;
    JSErr = JSErr + 1;
  }
  SteeringErr = (*RawSteering<(SteeringMinCal/2)) || (*RawSteering>(SteeringMaxCal+(5000-SteeringMaxCal)/2));
  if (SteeringErr) {
    *RawThrottle = Throttle0Cal;
    *RawSteering = Steering0Cal;
    JSErr = JSErr + 1;
  }
  if (*RawThrottle >= Throttle0Cal){
    *RawThrottle = map (*RawThrottle, Throttle0Cal, ThrottleMaxCal, 0, 1000);
    *RawSteering = map (*RawSteering, Steering0Cal, SteeringMaxCal, 0, 1000);
    JSErr = 0;
  }
  else {
    *RawThrottle = map (*RawThrottle, ThrottleMinCal, Throttle0Cal, -1000, 0);
    *RawSteering = map (*RawSteering, SteeringMinCal, Steering0Cal, -1000, 0);
    JSErr = 0;
  }
  if ((JSErr >= 3) && ThrottleErr){
     // Throttle disconnect - send Critical Error message;
      RoboteqNeutral ();
      Update_Total_Time ();
      SEND_DISPLAY_MESSAGE1 (_JSTICK, 0);
    }
  if ((JSErr >= 3) && SteeringErr){
     // Steering disconnect - send Critical Error message;
      RoboteqNeutral ();
      Update_Total_Time ();
      SEND_DISPLAY_MESSAGE1 (_JSTICK, 1);
    }

// ********************************************  
// add calculation of PercentThrottle here and SendByte it to Roboteq
// ********************************************  
  if ((UsrToggle==Attendant)&&((Mode==Drive1)||(Mode==Drive2)||(Mode==Drive3))){
    byte PercentThrottle = abs(*RawThrottle*100)/1000;
    newByte = PercentThrottle;
    SEND_BYTE (PThrottle_id, 0);
  }
} // end of READ_JOYSTICK_PINS

int CURVE_JOYSTICK (int raw, byte axis) {
  int scaled;
  int sign;
  int db = (int) Deadband*10;
// set to 0 if within deadband
  if (raw >= db){
    scaled = map (raw-db, 0, 1000-db, 0, 1000);
    scaled = constrain (scaled, 0, 1000);
    sign = 1;
  }
  else if (raw <= -db) {
    scaled = map (raw+db, 0, -1000+db, 0, -1000);
    scaled = constrain (scaled, -1000, 0);
    sign = -1;
  }
  else {
    scaled = 0;
  }
if (Mode < Seat){
// apply user-selected level of exponential curving
  int squared = sign*(((long)scaled* (long) scaled)/1000);
  byte Curving;
  if (axis == 0){
    Curving = ThrottleCurving;
  }
  else {
    Curving = SteeringCurving;
  }
  switch (Curving) {
    case 0:
       break;
    case 1: // low
      scaled = (2*scaled + squared)/3;
      break;
    case 2: // mild
      scaled = (scaled + squared)/2;
      break;
    case 3: // mid
      scaled = (scaled + 2*squared)/3;
      break;
    case 4: // strong
      scaled = squared;
     break;
  } // end switch (Curving)
} // if (Mode<Seat)
  return scaled;
} // end of CURVE_JOYSTICK

void READ_SPEEDPOT (void){
/* 5k or 10k speed pot with 500 ohm resistors to limit to 0.5 to 4.5 V
   connected to SpeedPotPin.  SpeedPot from DX-ACU is 10k with 470 ohm
   resistors.
   DO NOT CONFIGURE ANY OUT OF RANGE SAFETY STOP; we want chair to be
   driveable even if speed pot fails, but at reduced speeds for safety! */
  long SumPotInput = 0;
  int PotInput;
  int SpeedPotInput;
  int TurnPotInput;
  static boolean PotErrorSignalled = false;
  analogRead (SpeedPotPin);
  for (byte I = 0 ; I < SpeedPotFilter ; I++) {
    SumPotInput = SumPotInput + analogRead(SpeedPotPin);
  }
  PotInput = SumPotInput/SpeedPotFilter;
  PotInput = map(PotInput, 0, 1023, 0, 5000); // scale to mV
/* detect "overvoltage" if over PotMaxCal+50 and "undervoltage" if under
   PotMinCal-50.  Detection points set outside pot range to account for 
   resistor tolerance and change of value over time/temperature -- 
   this produces "fudge zones" that are set to normal minimum and maximum settings. 
   Outside these zones, SpeedPot is set to SpeedPotLowFault or SpeedPotHighFault
   and TurnPot is set to TurnPotFwdMin */
  if (PotInput <= (PotMinCal-50)){ // SpeedPot below normal minimum
    if (SpeedPot != SpeedPotLowFault){
      Update_Total_Time ();
      SEND_DISPLAY_MESSAGE1 (_POTFAULT, 0);
      lastCANMessageTime = millis ();
      PotErrorSignalled = true;
      SpeedPot = SpeedPotLowFault; // set SpeedPot to slow speed in case of fault
      TurnPot = TurnPotFwdMin;
    }
  }
  else if (PotInput >= (PotMaxCal+50)) { // SpeedPot above normal maximum
    if (SpeedPot != SpeedPotHighFault){
      Update_Total_Time ();
      SEND_DISPLAY_MESSAGE1 (_POTFAULT, 1);
      lastCANMessageTime = millis ();
      PotErrorSignalled = true;
      SpeedPot = SpeedPotHighFault;
      TurnPot = TurnPotFwdMin;
    }
  } // end if SpeedPot below else if above normal range
  else { // SpeedPot within normal range +/- 50 mV
    byte Min;
    byte Max;
    if (PotErrorSignalled){
      SEND_DISPLAY_MESSAGE1 (_POTFAULT, 3); // >1 implies "erase"
      PotErrorSignalled = false;
    }
    if (Throttle >=0){
      Min = SpeedPotFwdMin;
      Max = SpeedPotFwdMax;
    }
    else {
      Min = SpeedPotRevMin;
      Max = SpeedPotRevMax;
    }
    SpeedPotInput = PotInput;
    SpeedPotInput = map (SpeedPotInput, PotMinCal, PotMaxCal, Min*10, Max*10);
    SpeedPotInput = constrain (SpeedPotInput, Min*10, Max*10);
    SpeedPot = SpeedPotInput/10;
    DisplaySpeedPot = ((int)(SpeedPot - Min)*100)/(Max-Min);
    SEND_DISPLAY_MESSAGE1 (_SPEEDPOT, DisplaySpeedPot);
    if (Throttle > -250){ // use Fwd turn values when at or above "turn in place"
      Min = TurnPotFwdMin;
      Max = TurnPotFwdMax;
    }
    else { // use Rev turn values only when definitely going backwards; <=-250
      Min = TurnPotRevMin;
      Max = TurnPotRevMax;
    }
    TurnPotInput = PotInput;
    TurnPotInput = map (TurnPotInput, PotMinCal, PotMaxCal, Min*10, Max*10);
    TurnPotInput = constrain (TurnPotInput, Min*10, Max*10);
    TurnPot = TurnPotInput/10;
  } // end else SpeedPot within normal range
  SEND_POT_MESSAGE ();
} // end of READ_SPEEDPOT

void SEND_JOYSTICK_MESSAGE (void){
  if (BrakeError != BrakesOK) {
    Throttle = 0;
    Steering = 0;
  }
  if (Throttle || Steering){
    lastMoveTime = millis();
  }
// 4 bytes for 2 16-bit integers
// 4 bytes for complements of each of the actual data bytes
  byte DATA[8];
  byte ThrottleDATA[2];
  byte SteeringDATA[2];
  CREATE_DATA (Throttle, ThrottleDATA);
  CREATE_DATA (Steering, SteeringDATA);
  DATA[0] = ThrottleDATA[0];
  DATA[1] = ThrottleDATA[1];
  DATA[2] = SteeringDATA[0];
  DATA[3] = SteeringDATA[1];
  for (byte i = 0; i < 4; i++){
    DATA[i+4] = ~DATA[i];
  }
// no confirmation needed here as message will be re-sent in each loop
// if Throttle or Steering != LastThrottle or LastSteering or after 100 msec
// (was Heartbeat) if unchanged.  LastThrottle = Throttle and 
// LastSteering = Steering if joystick_ret confirmation message received from Roboteq.
// Confirmation *is* checked in void RoboteqNeutral ().
  SEND_MESSAGE (8, Joystick_id, DATA);
  if ((Throttle)||(Steering)){
    lastCANMessageTime = millis ();
  }
  lastJoystickMessageTime = millis ();
} // end SEND_JOYSTICK_MESSAGE ()

void SEND_POT_MESSAGE (void){
  static byte LastSpeedPot = 67;
  static byte LastTurnPot = 21;
// 2 bytes for pot values
// 2 bytes for complements
  byte DATA[4];
  if ((SpeedPot != LastSpeedPot) || (TurnPot != LastTurnPot)){
    DATA[0] = SpeedPot;
    DATA[1] = TurnPot;
    DATA[2] = ~DATA[0];
    DATA[3] = ~DATA[1];
    SEND_MESSAGE (4, Pot_id, DATA);
    lastCANMessageTime = millis ();
  }
  LastSpeedPot = SpeedPot;
  LastTurnPot = TurnPot;
} // end SEND_POT_MESSAGE ()

void CREATE_DATA (int value, byte * bytes) {
  bytes[0] = (uint16_t) value>>8;
  bytes[1] = value;
} // end of CREATE_DATA

void DO_JS_SEAT (void){
  int RawThrottle, RawSteering;
  int aThrottle, aSteering;
  int JoystickTrigger = 10*JswitchTrigger;
  READ_JOYSTICK_PINS (&RawThrottle, &RawSteering);
  Throttle = CURVE_JOYSTICK (RawThrottle, 0)*ThrottleSense;
  Steering = CURVE_JOYSTICK (RawSteering, 1)*SteeringSense;
  aThrottle = abs (Throttle);
  aSteering = abs (Steering);
// emulate switches if joystick moved by at least JoystickTrigger
  boolean Lifting = ((aThrottle > aSteering) && (aThrottle > JoystickTrigger));
  boolean Tilting = ((aSteering > aThrottle) && (aSteering > JoystickTrigger));
  boolean Movement = (Lifting || Tilting);
  if (!Movement){
    if (lastSeatMove != none){
      SEAT_MOVE (none);
    }
  }
// give precedence to axis that is furthest from neutral
  else { // Movement
    if (Lifting){
      if (Throttle > 0){
        if (lastSeatMove != up){
          SEAT_MOVE (up);
        }
      } // end of Throttle > 0
      else if (Throttle < 0){
        if (lastSeatMove != down){
          SEAT_MOVE (down);
        }
      } // end of Throttle < 0
    } // end of Lifting
    else if (Tilting){
      if (Steering > 0){
        if (lastSeatMove != forward){
          SEAT_MOVE (forward);
        }
      } // end of Steering > 0
      else if (Steering < 0){
        if (lastSeatMove != back){
          SEAT_MOVE (back);
        }
      } // end of Steering < 0
    } // end of Tilting
  } // end else Movement
  RECEIVE_MESSAGE ();
} // end of DO_JS_SEAT

void DO_JS_LIGHTS (void){
// PROGRAMMING of Roboteq digOut pins to be done in Roboteq script.
  int RawThrottle, RawSteering;
  int aThrottle, aSteering;
  int JoystickTrigger = 10*JswitchTrigger;
  static boolean SetLightsCalled = false;
  READ_JOYSTICK_PINS (&RawThrottle, &RawSteering);
  Throttle = CURVE_JOYSTICK (RawThrottle, 0)*ThrottleSense;
  Steering = CURVE_JOYSTICK (RawSteering, 1)*SteeringSense;
  aThrottle = abs (Throttle);
  aSteering = abs (Steering);
// same logic as for seat except that works as
// push on/push off toggles.
  lastAmpsTime = millis(); // prevent CHECK_CURRENT from triggering an error
  if (SetLightsCalled && !SendByteDone){
    SET_LIGHTS ();
  }
  else if (SetLightsCalled && SendByteDone){
    SetLightsCalled = false;
    NSEWArmed = false;
  }
  else if (!SetLightsCalled) {
    if (!NSEWArmed) { // NSEWArmed
      if ((aThrottle <= JoystickTrigger) && (aSteering <= JoystickTrigger)){ // JS near neutral
        if (SendByteDone){
          NSEWArmed = true;
          SetLightsCalled = false;
        }
      }
    } // arm NSEW if SendByteDone and JS near neutral
    else if ((aThrottle > JoystickTrigger) || (aSteering > JoystickTrigger)){ // NSEWArmed and JS moved
      if ((aThrottle > aSteering) && (aThrottle > JoystickTrigger)){ // Head and FourWay section
        if (Throttle > 0){
          if (bitRead (LightsFlags, HeadlightBit) == OFF){
            bitWrite (LightsFlags, HeadlightBit,ON);
          }
          else if (bitRead (LightsFlags, HeadlightBit) == ON){
            bitWrite (LightsFlags, HeadlightBit,OFF);
          }
        } // end of Throttle > 0
        else if (Throttle < 0){
          if (bitRead (LightsFlags, FourWayBit) == OFF){
            bitWrite (LightsFlags, FourWayBit,ON);
          }
          else if (bitRead (LightsFlags, FourWayBit) == ON){
            bitWrite (LightsFlags, FourWayBit,OFF);
          }
        } // end of Throttle < 0
      } // end Head and FourWay section
      else if ((aSteering > aThrottle) && (aSteering > JoystickTrigger)){ // Turn signals section
        if (Steering > 0){
          if (bitRead (LightsFlags, RightTurnBit) == OFF){
            bitWrite (LightsFlags, LeftTurnBit,OFF);
            bitWrite (LightsFlags, RightTurnBit,ON);
          }
          else if (bitRead (LightsFlags, RightTurnBit) == ON){
            bitWrite (LightsFlags, RightTurnBit,OFF);
          }
        } // end of Steering > 0
        else if (Steering < 0){
          if (bitRead (LightsFlags, LeftTurnBit) == OFF){
            bitWrite (LightsFlags, LeftTurnBit,ON);
            bitWrite (LightsFlags, RightTurnBit,OFF);
          }
          else if (bitRead (LightsFlags, LeftTurnBit) == ON){
            bitWrite (LightsFlags, LeftTurnBit,OFF);
          }
        } // end of Steering < 0
      } // end Turn signals section
      SET_LIGHTS ();
      SetLightsCalled = true;
      if (SendByteDone){
        NSEWArmed = false;
        SetLightsCalled = false;
      }
    } // end else NSEWArmed and JS moved
  } // end if ((SetLightsCalled) && (!SendByteDone)) else if (!SetLightsCalled) else if (!SetLightsCalled)
  RECEIVE_MESSAGE ();
} // end of DO_JS_LIGHTS

























