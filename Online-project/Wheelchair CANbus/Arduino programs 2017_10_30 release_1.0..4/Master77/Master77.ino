/*
===== MAKE SURE TO SWAP CAN LIBRARY SO THAT IT MATCHES CURRENT MODULES =====
***** TESTED WITH Arduino 1.8.0.  NOT tested with later versions *****
*/

// *****************************************************************
// ROBOTEQ CAN Heartbeat must be set to 0 or it may keep MCP2551
// from sleeping
// *****************************************************************

/*
 SEARCH FOR "TESTING" to find sections to be removed before real use.
*/

/*
TO DO:
 port Accelerometer code to AuxNode (3 analog inputs or I2C?).

 Reburned extended fuse from:
    nano328.bootloader.extended_fuses=0x05
      to
    nano328.bootloader.extended_fuses=0x04
 to change brownout detection from 2.7V to 4.3V

 USER SETTINGS in UserSettings.h
 Roboteq constants (with additions) and Display node enumerations in Constants.h
    in WheelchairCAN library
 General use #defines in Defines.h
 General use GLOBAL VARIABLES in Globals.h
 CAN message IDs in MsgIDs.h in WheelchairCAN library
 All procedures except Setup() and Loop() in
 GeneralProcedures
 JoystickProcedures
 SwitchProcedures
 */

/* ************************************************************************************************/
/* ************************************************************************************************/
/* ************************************************************************************************/
#include <CAN.h>
#include <SPI.h>
#include <avr/sleep.h>
#include <EEPROM.h>
#include <Constants.h>
#include "UserSettings.h"
#include "Defines.h"
#include <MsgIDs.h>
#include <Labels.h>
#include "Globals.h"

void setup (void) {
  // uncomment following line to reset from EEPROM error
//   EEPROM.write (NewMCUFlag,0b1010101);
  // setup starts when momentary switch starts DC-DC converter
  // RC delay of ON/OFF board keeps converter working long enough to:
  // set PowerOnPin to INPUT_PULLUP and this then holds DC-DC converter
  // Vr pin high even when momentary switch is no longer pressed
  pinMode (PowerOnPin, INPUT_PULLUP); // adequate to keep DC-DC converter on
  // *************************************************************************
  //  For testing, uncomment following line(s) to "fool" Programmer as desired
  //  FakeNewMCU ();
  //  ResetTotalTime ();
  //  ResetCalibration ();
  // For testing, uncomment above line(s) to "fool" Programmer as desired
  // *************************************************************************
  // if volt reading has been calibrated by Programmer, use stored value
  // else default to value set in Globals.h
  int StoredVoltCalibration = (int)(EEPROM.read(VoltCalibrationAddress) << 8)
                              | EEPROM.read(VoltCalibrationAddress + 1);
  if (StoredVoltCalibration > 100) {
    VoltCalibration = StoredVoltCalibration;
  }
  unsigned long SetupStartTime = millis ();
   Serial.begin (115200);
   Serial.println(__FILE__ " - " __DATE__ " " __TIME__);
  // For new MCU or to completely re-write UserSettings, Programmer
  // will send values to put in UserSettings[] array.  For loop will
  // then write values from array to EEPROM.
  // Until then, default values will be used.
  SetDefaultUsr ();
  CK_UserEEPROM ();
  if (EEPROM.read (NewMCUFlag) != 0b1010101) { // use default settings until first programming
    // or replacement of corrupted EEPROM
    //    // Serial.println (F("NewMCU or EEPROMfault: loading default settings"));
    DefaultUsrToEEPROM ();
  }
  UsrFromEEPROM (); // retrieve user settings from EEPROM
  TurnPot = TurnPotFwdMin;
  XCVR (Wake),
  SETUP_CAN ();
  nonMASTER (Wake);
  // switches go LOW when closed:
  pinMode (OnOffPin, INPUT_PULLUP);
  pinMode (FwdPin, INPUT_PULLUP);
  pinMode (RevPin, INPUT_PULLUP);
  pinMode (LftPin, INPUT_PULLUP);
  pinMode (RtPin, INPUT_PULLUP);
  pinMode (ModePin, INPUT_PULLUP);
  pinMode (UsrTogglePin, INPUT_PULLUP);
  pinMode (StopPin, INPUT_PULLUP);
  pinMode (XcvrSleepPin, INPUT); // change to OUTPUT HIGH to put 2551 transceiver to sleep
                                 // OUTPUT LOW then INPUT to resume CAN communication
  // XcvrRxDPin used to wake a sleeping non-master node (not used for MASTER_WAKE)
  pinMode (XcvrRxDPin, INPUT);
  EEPROM_RING (); // set up, maintain and read ring memory to store Mode and AmpHrs
  GET_CALIBRATION (); // in JoysticProcedures, retrieve values from EEPROM
  lastUsrToggle = digitalRead (UsrTogglePin);
  UsrToggle = lastUsrToggle;
  // make sure not to start in a high speed mode if toggle set to switch control
  if ((UsrToggle == Driver) && ((Mode == Drive1) || (Mode == Drive2) || (Mode == Drive3))) {
    Mode = Drive1;
  }
  if (SpeedPotInstalled) {
    READ_SPEEDPOT ();
  }
  lastJoystickMessageTime = millis ();
  lastAmpsTime = millis ();
  ModeMsg = true;
  old_ADCSRA = ADCSRA; // store ADC state to restore after sleep
  for (byte i = 0; i <= 19 ; i++) {
    ThrottleStack[i] = 0;
    SteeringStack[i] = 0;
  }
//  while (!RoboteqLive) { // use this line if lots of prints during setup
  while ((!DisplayLive) || (!AuxLive) || (!RoboteqLive)) {
    SEND_MESSAGE (0, Aux_Dummy, NULL);
    delay (5);
    RECEIVE_MESSAGE ();
    SEND_MESSAGE (0, Display_Dummy, NULL);
    delay (5);
    RECEIVE_MESSAGE ();
    Serial.print (F("DisplayLive = "));
    Serial.print (DisplayLive);
    Serial.print (F("  AuxLive = "));
    Serial.print (AuxLive);
    Serial.print (F("  RoboteqLive = "));
    Serial.println (RoboteqLive);
    if (((millis () - SetupStartTime) > 30000)) {
      Serial.println (F("*** WARNING *** WARNING *** STARTUP ERROR - will shut down NOW!"));
      Serial.println (F("Setup timed out"));
      delay (3000);
      TURN_OFF ();
    }
  }// end of ck for non Masters live
  delay (100);
  lastMoveTime = millis ();
  lastCANMessageTime = millis ();
  SEND_MESSAGE (0, UsrSettings_ret, NULL);
  Charging = true;
  while (digitalRead (OnOffPin) == LOW) { // make sure OnOffPin has gone
    // HIGH before leaving setup
    delay (100);
  }
  VoltageDividerOffset = EEPROM.read(VoltCalibrationAddress+2)<<8 | EEPROM.read(VoltCalibrationAddress+3);
  StepTime = millis();
// ***********************************************************************
// uncomment next line to send a sequence of motions defined in procedure AutoStep()
//   DoAutoStep = true;
// ***********************************************************************
  Serial.println (F("*****************************************"));
  Serial.println (F("              END SETUP"));
  Serial.println (F("*****************************************"));
  RunningTime = millis ();
  Update_Total_Time ();
} // end setup ()

void loop (void) {
  // the first if segment runs just once at startup
  if (SetupJustRun) {
    // make sure OnOffPin has gone HIGH so that RC delay on DC-DC board
    // doesn't trigger an immediate shutdown in Loop
    while (digitalRead (OnOffPin) == LOW) {
      delay (10);
    }
    CheckOON ();
    SetupJustRun = false;
  }
  // the following three segments run at startup, and are called each
  // time through loop until message handshake has been received
  if (!StartingVoltsDone) {
    // send StartingVolts to Display node
    FIND_STARTING_VOLTS ();
  }
  // following runs just once after RoboteqTime has been read
  static boolean StartupMsgSent = false;
  if (!StartupMsgSent) {
    // Serial.println (F("!StartupMsgSent"));
    // with delay (10) there's no longer any need for repeating the messaging!
    SEND_TIME (_TOTTIME, TotalHours, TotalMinutes);
    delay (10);
    SEND_DISPLAY_MESSAGE0 (_STARTUP);
    delay (10);
    byte DisplayAHr = 100 - ((halfAHr * 5 * 100) / (AHrFull - AHrEmpty));
    SEND_DISPLAY_MESSAGE1 (_AHR, DisplayAHr);
    // DisplayVolts was found by FIND_STARTING_VOLTS call above
    SEND_DISPLAY_MESSAGE1 (_DVOLTS, DisplayVolts);
    SEND_DISPLAY_MESSAGE1 (_MODE, UsrToggle << 3 | Mode);
    delay (10);
    if (EEPROM.read (NewMCUFlag) != 0b1010101) {
      SEND_DISPLAY_MESSAGE0 (_EEPROM);
      delay (10);
    }
    StartupMsgSent = true;
  } // end if (!StartupMessageSend)
  // above segments run at startup, but are skipped once message
  // handshake has been received
  // update Roboteq user settings at startup and if programmer has
  // changed any user settings
  if (!RoboteqSettingsSent) {
    SendRoboteqSettings ();
    if (SensorsSet && AccelSet && BoostsSet && MotorCompSet && Pins1Set && Pins2Set and MotorsSet) { // && LimitsSet
      RoboteqSettingsSent = true;
      // Serial.println (F("Roboteq User Settings UPDATED"));
      if (EEPROM.read (NewMCUFlag) != 0b1010101) {
        SEND_DISPLAY_MESSAGE0 (_EEPROM);
        delay (10);
      }
    }
  }
  if (!XcvrSleep) {
    boolean elapsed = false;
    boolean SleepTimeElapsed = false;
    // to let Programmer know that Master is awake
    if ((millis() - AwakeTime) >= 1000) {
      SEND_MESSAGE (0, UsrSettings_ret, NULL);
      AwakeTime = millis();
    }
    if (!JustAwakened) {
      if (digitalRead (OnOffPin) == LOW) {
        pinMode (OnOffPin, OUTPUT);
        digitalWrite (OnOffPin, LOW);
        TURN_OFF ();
      }
      // force display update if toggle changed while asleep
      if (ToggleChangeInSleep) {
        SEND_DISPLAY_MESSAGE1 (_MODE, digitalRead (UsrTogglePin) << 3 | 0);
        ToggleChangeInSleep = false;
      }
      if (CkContactorArmed && (millis() - lastCkContactorTime) >= 100) {
        CK_CONTACTOR ();
      }
      RUN_CHAIR ();
      // read Amps every 3 seconds and accumulate as milliAHrs
      // also checks volts if current flow is low and averages five such
      // reads to send to Display.  (A low value triggers a SetSlow change
      // to conserve low battery.)
elapsed = ((uint32_t)(millis () - lastAmpsTime) > 3600);
      if (elapsed) {
        CHECK_CURRENT ();
      } // if elapsed
      if (TimeToSleep > 0) {
        SleepTimeElapsed = ((uint32_t)(millis () - lastCANMessageTime) > (uint32_t)((uint32_t)TimeToSleep * 10000));
        if ((SleepTimeElapsed) && (ContactorError != Fused)) {
          GO_TO_SLEEP ();
        }
      }
    } // !JustAwakened
    else {
      JustAwakened = false; // if JustAwakened change to !JustAwakened
      delay (10); // to make sure nodes fully awake after sleep
    }
    RECEIVE_MESSAGE (); // receive messages to detect returned values and confirmations
  } // end if (!XcvrSleep)
  else {
    WAKE_UP ();
  } // if (!XcvrSleep) else
  if (LoopCycles % 2000 == 0) {
    if (EEPROM.read (NewMCUFlag) != 0b1010101) {
      SEND_DISPLAY_MESSAGE0 (_EEPROM);
    }
  }
  if (!StartupLogged) {
    if (LoopCycles % 5000 == 0) {
      // Serial.println (F(" repeat _STARTUP message"));
      SEND_TIME (_TOTTIME, TotalHours, TotalMinutes);
      delay (10);
      SEND_DISPLAY_MESSAGE0 (_STARTUP);
      delay (10);
      byte DisplayAHr = 100 - ((halfAHr * 5 * 100) / (AHrFull - AHrEmpty));
      SEND_DISPLAY_MESSAGE1 (_AHR, DisplayAHr);
      CHECK_VOLTS ();
      byte DisplayVolts = ((deciVolts - VoltsEmpty) * 100) / (VoltsFull - VoltsEmpty);
      SEND_DISPLAY_MESSAGE1 (_DVOLTS, DisplayVolts);
    }
  }
  LoopCycles = LoopCycles + 1; // for actions not done on every pass through loop
} // end of loop



