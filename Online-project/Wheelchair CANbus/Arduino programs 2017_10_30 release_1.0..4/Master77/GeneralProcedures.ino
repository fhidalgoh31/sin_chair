void AutoStep (void){
  if (millis()-StepTime >= 5000){
    StepTime = millis();
    switch (Step) {
      case (1):
        Throttle = 1000;
        Steering = 200;
        break;
      case (2):
        Throttle = -1000;
        Steering = -200;
        break;
      case (3):
        Throttle = -1000;
        Steering = -200;
        break;
      case (4):
        Throttle = 0;
        Steering = 0;
        break;
      case (5):
        Throttle = 0;
        Steering = 0;
        break;
      case (6):
        Throttle = 0;
        Steering = 0;
        break;
      case (7):
        Throttle = 0;
        Steering = 0;
        break;
      case (8):
        Throttle = 0;
        Steering = 0;
        break;
      case (9):
        Throttle = 0    ;
        Steering = 0;
        break;
      case (10):
        Throttle = 0;
        Steering = 0;
        break;
    } // end switch
    Step = Step + 1;
    if (Step > 5){
      Step = 1;
    }
  }
} // end AutoStep

void ResetTotalTime (void) {
  //**************************************************************************
  // ZERO TOTAL TIME (for TESTING only) - comment out calling statement before use
  for (byte i = 0; i <= 3; i++) {
    EEPROM.update (TotalTimeAddress + i, 0);
    EEPROM.update (TotalTimeAddress + i + 4, 0);
  }
  // above serves to ZERO TOTAL TIME (for testing only)
  // comment out calling statement before use
  //**************************************************************************
} // end ResetTotalTime

void ResetCalibration (void) {
  // TESTING only: to force use of default values as though calibration never run.
  // MUST run calibration after doing this!
  for (byte addy = 0 ; addy <= 15 ; addy++) {
    EEPROM.write (CalibrationAddress + addy, 0);
  }
} // end ResetCalibration (void)

void FakeNewMCU (void) {
  // pretend that Programmer has never sent UserSettings
  EEPROM.write (NewMCUFlag, 0);
} // end FakeNewMCU

void XCVR (boolean XcvrState) {
  if (XcvrState == Wake) {
    digitalWrite (XcvrSleepPin, LOW);
    pinMode (XcvrSleepPin, INPUT);
    XcvrSleep = false;
    // Serial.println (F("AWAKE"));
  }
  if (XcvrState == Sleep) {
    pinMode (XcvrSleepPin, OUTPUT);
    digitalWrite (XcvrSleepPin, HIGH);
    XcvrSleep = true;
    // Serial.println (F("SLEEPING"));
    do { // make sure CAN.setMode(SLEEP) isn't blocked by messages in buffer
      RECEIVE_MESSAGE ();
    }
    while (MsgReceived);
    CAN.setMode(SLEEP);
    delay (1); // some delay needed so Serial.print not blocked
  }
} // end XCVR

void SETUP_CAN (void) {
  CAN.begin();
  CAN.setMode(CONFIGURATION);  // set to "NORMAL" for standard communications
  CAN.baudConfig(BUS_SPEED);
  /* MASKS and FILTERS for Master (joystick/switch) module.
   The lower the ID value, the higher the priority.
   Highest priority messages go first to buffer 0, then to buffer 1
   if buffer 0 is full (depends on MCP2151 register setting)
   or if buffer 0 filter not matched.
   If 2 messages arrive at the same time, higher priority msg goes to
   buffer 0, lower priority msg goes to buffer 1.
   "Category" in 2 msb of ID determines priority of Roboteq message groups:
   0b00=SetCommand, 0b01=GetValue, 0b10 and 0b11 = configuration.
   Within a catagory, "Name" in next 6 bits determines priority.
   Thus, SetCommand is highest priority group, and _G (motor output setting = 0)
   has absolute highest priority.
   */
  /*
  fromAux B000  toAux B001  fromRoboteq B010  toRoboteq B011
   fromMaster B100  toMaster B101  fromDisplay B110  toDisplay B111
   */
  CAN.setMaskOrFilter(MASK_0,   0b00000000, 0b11100000, 0b00000000, 0b00000000);
  CAN.setMaskOrFilter(FILTER_0, 0b00000000, 0b01000000, 0b00000000, 0b00000000); // fromRoboteq
  CAN.setMaskOrFilter(FILTER_1, 0b00000000, 0b10100000, 0b00000000, 0b00000000); // toMaster

  CAN.setMaskOrFilter(MASK_1,   0b00000000, 0b11100000, 0b00000000, 0b00000000); // may want to set to all 0 to
  // accept all messages if more modules are used
  //  CAN.setMaskOrFilter(FILTER_2, 0b00000000, 0b00000000, 0b00000000, 0b00000000); // fromAux
  //  CAN.setMaskOrFilter(FILTER_3, 0b00000000, 0b11000000, 0b00000000, 0b00000000); // fromdisplay
  //  CAN.setMaskOrFilter(FILTER_4, 0b00000000, 0b01000000, 0b00000000, 0b00000000); // fromRoboteq
  CAN.setMaskOrFilter(FILTER_5, 0b00000000, 0b10100000, 0b00000000, 0b00000000); // toMaster
  CAN.setMode(NORMAL);  // set to "NORMAL" for standard communications
} // end SETUP_CAN

void nonMASTER (byte State) {
  Aux_SLEEPWAKE (State);
  Display_SLEEPWAKE (State);
} // end nonMaster

void Aux_SLEEPWAKE (byte State) {
  byte DATA [2];
  if (State == Sleep) {
    SEND_MESSAGE (0, SetSlow_confirm, NULL); // confirm any pending
                                             // setslow before sleeping
    XcvrSleep = true;
    // Serial.println (F("Nighty-night Aux"));
  }
  else {
    XcvrSleep = false;
    if (State == Wake) {
//      Serial.println (F("Aux WAKEUP!!!"));
    }
    else {
      // Serial.println (F("Aux SHUTOFF!!!"));
    }
  }
  DATA [0] = State;
  DATA [1] = ~DATA[0];
  if (!XcvrSleep) { // send OneShot Wake message
    CAN.OneShotTx(1); // there will be no ACK if transceiver in SLEEP
    // message sent OneShot will pull low RxD of transceiver in receiving node
    // receiving node monitors RxD to awaken controller, transceiver and MCU
    SEND_MESSAGE (2, AuxSleepWake_id, DATA);
    delay (10); // some delay IS needed here to let other nodes come on line
    SETUP_CAN (); // force back into NORMAL mode
  }
  // normal mode Sleep, Wake or Shutoff message
  SEND_MESSAGE (2, AuxSleepWake_id, DATA);
  if (State == Shutoff) {
    for (byte i = 1; i <= 5; i++) {
      SEND_MESSAGE (2, AuxSleepWake_id, DATA);
    }
  }
  lastCANMessageTime = millis ();
  delay (100);
} // end Aux_SLEEPWAKE

void Display_SLEEPWAKE (byte State) {
  byte DATA [2];
  if (State == Sleep) {
    XcvrSleep = true;
    // Serial.println (F("Nighty-night Display"));
  }
  else {
    XcvrSleep = false;
    if (State == Wake) {
//      Serial.println (F("Display WAKEUP!!!"));
    }
    else {
      // Serial.println (F("Display SHUTOFF!!!"));
    }
  }
  DATA [0] = State;
  DATA [1] = ~DATA[0];
  if (!XcvrSleep) { // send OneShot Wake message
    CAN.OneShotTx(1); // there will be no ACK if transceiver in SLEEP
    // message sent OneShot will pull low RxD of transceiver in receiving node
    // receiving node monitors RxD to awaken controller, transceiver and MCU
    SEND_MESSAGE (2, DisplaySleepWake_id, DATA);
    delay (10); // some delay IS needed here to let other nodes come on line
    SETUP_CAN (); // force back into NORMAL mode
  }
  // normal mode Sleep, Wake or Shutoff message
  SEND_MESSAGE (2, DisplaySleepWake_id, DATA);
  if (State == Shutoff) {
    for (byte i = 1; i <= 5; i++) {
      SEND_MESSAGE (2, DisplaySleepWake_id, DATA);
    }
  }
  lastCANMessageTime = millis ();
  delay (100);
} // end Display_SLEEPWAKE

void FIND_STARTING_VOLTS (void) {
  CHECK_VOLTS ();
  if (CheckVoltsDone) {
    StartingVoltsDone = true;
    CheckVoltsDone = false;
    int StartingVolts = deciVolts;
    if (StartingVolts < 200) { // running from PC brick
      StartingVolts = StartingVolts + 70;
    }
    DisplayVolts = ((StartingVolts - VoltsEmpty) * 100) / (VoltsFull - VoltsEmpty);
    SEND_DISPLAY_MESSAGE1 (_DVOLTS, DisplayVolts);
    lastCANMessageTime = millis ();
    //TESTING: uncomment next line to force reset of AHr
    //     StartingVolts = RechargeVolts + 10; // (for TESTING only)
    if (StartingVolts >= RechargeVolts) {
      halfAHr = 0;
      EEPROM.update (AHrAddress, halfAHr); // Battery has been recharged; AHr reset to 0
    }
  } // end if (CheckVoltsDone)
} // end FIND_STARTING_VOLTS

void RoboteqNeutral (void) {
  byte repeats = MaxTries;
  byte time = 1;
  Throttle = 0;
  Steering = 0;
  // motors to 0
  JoystickConfirmed = false;
  for (byte i = 1 ; i <= repeats; i++) {
    SEND_JOYSTICK_MESSAGE ();
    delay (time);
    RECEIVE_MESSAGE ();
    if (JoystickConfirmed) {
      break;
    }
  }
  // lights flags are global, not critical, therefore no loop to detect return
  LightsFlags = 0b1111;
  for (byte i = 1 ; i <= repeats; i++) {
    SET_LIGHTS();
    delay (time);
    RECEIVE_MESSAGE ();
    if (SendByteDone) {
      break;
    }
  }
  // stopping actuators not critical, therefore no loop to detect return
  for (byte i = 1 ; i <= repeats; i++) {
    SEAT_MOVE (none);
    delay (time);
    RECEIVE_MESSAGE ();
    if (SendByteDone) {
      break;
    }
  }
} // end RoboteqNeutral

void SendRoboteqSettings (void) {
  if (!SensorsSet) {
    SET_RoboteqSensors ();
  }
  if (!AccelSet) {
    SET_RoboteqAccel ();
  }
  if (!MotorCompSet) {
    SET_RoboteqMotorComp ();
  }
  if (!MotorComp2Set) {
    SET_RoboteqMotorComp2 ();
  }
  if (!BoostsSet) {
    SET_RoboteqBoosts ();
  }
  if (!Pins1Set) {
    SET_RoboteqPins1 ();
  }
  if (!Pins2Set) {
    SET_RoboteqPins2 ();
  }
  if (!MotorsSet) {
    SET_RoboteqMotors ();
  }
} // end SendRoboteqSettings

void EEPROM_RING (void) {
  // set up ring memory for Mode
  ModeAddress = EEPROM.read (0); // address where Mode value was last stored
  //  Mode EEPROM memory ring runs from address 20 through 39
  if ((ModeAddress < 20) || (ModeAddress > 39)) {
    ModeAddress = 20;
  }
  else {
    Mode = EEPROM.read (ModeAddress);
    ModeAddress = ModeAddress + 1;
    //  Mode EEPROM memory ring runs from address 20 through 39
    if (ModeAddress > 39) {
      ModeAddress = 20;
    }
  }
  EEPROM.update (0, ModeAddress); // address where Mode will be stored
  EEPROM.update (ModeAddress, Mode); // re-stored in new address
  AHrAddress = EEPROM.read (1); // address where AHr value was last stored
  //  AHr EEPROM memory ring runs from address 40 through 59
  if ((AHrAddress < 40) || (AHrAddress > 59)) {
    AHrAddress = 40;
  }
  else {
    halfAHr = EEPROM.read (AHrAddress);
    AHrAddress = AHrAddress + 1;
    //  AHr EEPROM memory ring runs from address 40 through 59
    if (AHrAddress > 59) {
      AHrAddress = 40;
    }
  }
  EEPROM.update (1, AHrAddress); // address where AHr will be stored
  EEPROM.update (AHrAddress, halfAHr); // re-stored in new address
} // end of EEPROM_RING

void UsrToEEPROM (void) {
  // JOYSTICK AND SPEED POT SETTINGS:
  UserSettings[0] = SpeedPotRevMin;
  UserSettings[1] = SpeedPotRevMax;
  UserSettings[2] = TurnPotFwdMin;
  UserSettings[3] = TurnPotRevMin;
  UserSettings[4] = TurnAtFullSpeed;
  UserSettings[5] = ThrottleCurving;
  UserSettings[6] = SteeringCurving;
  UserSettings[7] = Deadband;
  UserSettings[8] = Damping;
  UserSettings[9] = JswitchTrigger;
  UserSettings[10] = SpeedPotInstalled;
  UserSettings[11] = SpeedPot;
  UserSettings[12] = TurnPotFwdMax;
  UserSettings[13] = TurnPotRevMax;
  UserSettings[14] = SpeedPotFwdMin;
  UserSettings[15] = SpeedPotFwdMax;
  UserSettings[16] = SpeedPotLowFault;
  UserSettings[17] = SpeedPotHighFault;
  UserSettings[18] = 0; //SpeedPotFilter now in Defines.h
  // SWITCH DRIVING SETTINGS:
  UserSettings[19] = Drive1FwdSpeed;
  UserSettings[20] = Drive1RevSpeed;
  UserSettings[21] = Drive1FwdTurnRate;
  UserSettings[22] = Drive1RevTurnRate;
  UserSettings[23] = Drive2FwdSpeed;
  UserSettings[24] = Drive2RevSpeed;
  UserSettings[25] = Drive2FwdTurnRate;
  UserSettings[26] = Drive2RevTurnRate;
  UserSettings[27] = Drive3FwdSpeed;
  UserSettings[28] = Drive3RevSpeed;
  UserSettings[29] = Drive3FwdTurnRate;
  UserSettings[30] = Drive3RevTurnRate;
  UserSettings[31] = LoadBoostCurving; // for MotorLoadBoost
  // CHAIR SETUP:
  UserSettings[32] = TimeToSleep;
  // SPEED REDUCTIONS
  UserSettings[33] = Slow1;
  UserSettings[34] = Slow2;
  UserSettings[35] = LimpSpeed;
  UserSettings[36] = 0; // obsolete AccelMix
  UserSettings[37] = 0; // obsolete LoadBoost_I
  UserSettings[38] = 0;  // obsolete SpeedBoost
  UserSettings[39] = TurnBoost;
  UserSettings[40] = 0; // obsolete MinPWM
  UserSettings[41] = BackStickBraking; // multiplied *10 in Roboteq script
  UserSettings[42] = LowIBoost;
  UserSettings[43] = 0; // obsolete TransitionPWM
  // Roboteq connections
  UserSettings[44] = UseCurrentSensors;
  UserSettings[45] = LeftSensorPin;
  UserSettings[46] = RightSensorPin;
  UserSettings[47] = LeftSensorReversePolarity;
  UserSettings[48] = RightSensorReversePolarity;
  UserSettings[49] = TiltForwardPin;
  UserSettings[50] = TiltBackPin;
  UserSettings[51] = LiftUpPin;
  UserSettings[52] = LiftDownPin;
  UserSettings[53] = Brake1Pin;
  UserSettings[54] = Brake2Pin;
  UserSettings[55] = ContactorPin;
  UserSettings[56] = SwapMotors;
  UserSettings[57] = Motor1ReversePolarity;
  UserSettings[58] = Motor2ReversePolarity;
  UserSettings[59] = LoadBoost_D;
  UserSettings[60] = CompDeadband;
  UserSettings[61] = CompTurnBoost;
  UserSettings[62] = LoadBoost_P;
  for (byte i=63;i<=75;i++) {
    UserSettings[i] = 0; // unused
  }
// DISPLAY MESSAGING
  UserSettings[76] = highByte(VoltsFull);
  UserSettings[77] = lowByte(VoltsFull);
  UserSettings[78] = highByte(VoltsEmpty);
  UserSettings[79] = lowByte(VoltsEmpty);
  UserSettings[80] = highByte(AHrFull);
  UserSettings[81] = lowByte(AHrFull);
  UserSettings[82] = highByte(AHrEmpty);
  UserSettings[83] = lowByte(AHrEmpty);
// Roboteq SETTINGS
  UserSettings[84] = highByte(Accel);
  UserSettings[85] = lowByte(Accel);
  UserSettings[86] = highByte(Decel);
  UserSettings[87] = lowByte(Decel);
  UserSettings[72] = 0; // unused
  UserSettings[73] = 0; // unused
  UserSettings[74] = 0; // unused
  UserSettings[75] = 0; // unused
  UserSettings[92] = highByte(RechargeVolts);
  UserSettings[93] = lowByte(RechargeVolts);
  UserSettings[94] = highByte(LowVoltLimit);
  UserSettings[95] = lowByte(LowVoltLimit);
  UserSettings[96] = highByte(MotorResistance);
  UserSettings[97] = lowByte(MotorResistance);
  for (byte i = 0; i <= sizeof(UserSettings) - 1; i++) {
    EEPROM.update (UsrAddress + i, UserSettings[i]);
  }
}  // end UsrToEEPROM

void CK_UserEEPROM (void) {
  boolean EEPROMfault = false;
  byte Differences = 0;
  byte EEPROMUsrValue = 0;
  for (byte i = 0; i <= sizeof(UserSettings) - 1; i++) {
    EEPROMUsrValue = EEPROM.read (UsrAddress + i);
// PRINT STATEMENTS TO LIST ENTIRE ARRAY DURING SETUP
/*
Serial.print (i);
Serial.print (F(" "));
strcpy_P(LabelBuffer, (char*)pgm_read_word(&(UsrSettingLabels[i])));
Serial.print (LabelBuffer);
Serial.print (F(" Default = "));
Serial.print (UserSettings[i]);
Serial.print (F("  EEaddress = "));
Serial.print (UsrAddress + i);
Serial.print (F(" stored = "));
Serial.println (EEPROMUsrValue);
*/
    if (UserSettings[i] != EEPROMUsrValue) {
// PRINT STATEMENTS FOR DIAGNOSIS
/*
      Serial.print (F("mismatch at i = "));
      Serial.print (i);
      Serial.print (F("  "));
      strcpy_P(LabelBuffer, (char*)pgm_read_word(&(UsrSettingLabels[i])));
      Serial.print (LabelBuffer);
      Serial.print (F("  Default = "));
      Serial.print (UserSettings[i]);
      Serial.print (F("  EEaddress = "));
      Serial.print (UsrAddress + i);
      Serial.print (F("  stored = "));
      Serial.println (EEPROMUsrValue);
*/
      Differences = Differences + 1;
      if (Differences >= 30) {
        // Serial.println (F(">= 30 differences in EEPROM"));
        EEPROMfault = true;
      }
      else if ((i==0) || (i==2) || (i==3) || (i==14)) {
        // SpeedPotRevMin, TurnPotFwdMin, TurnPotRevMin, SpeedPotFwdMin
        if ((EEPROMUsrValue >= 80) || (EEPROMUsrValue == 0)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==1) { // SpeedPotRevMax
        if ((EEPROMUsrValue > 100) || (EEPROMUsrValue < UserSettings[0])) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==4) { // TurnAtFullSpeed
        if ((EEPROMUsrValue <= 20) || (EEPROMUsrValue > 100)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if ((i==5) || (i==6) || (i==31)) {
  // ThrottleCurving, SteeringCurving, LoadBoostCurving
        if ((EEPROMUsrValue > 4) || (EEPROMUsrValue < 0)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==7) { // Deadband
        if ((EEPROMUsrValue >= 40)  || (EEPROMUsrValue < 0)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==8) { // Damping
        if ((EEPROMUsrValue <= 0) || (EEPROMUsrValue > 20)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==9) { // JswitchTrigger
        if ((EEPROMUsrValue < 10) || (EEPROMUsrValue > 80)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if ((i==10) || (i==44) || (i==47) || (i==48) || ((i>=56 ) && (i<=58))){ 
// SpeedPotInstalled, UseCurrentSensors, LeftSensorReversePolarity, RightSensorReversePolarity
// SwapMotors, Motor1ReversePolarity, Motor2ReversePolarity
        if ((EEPROMUsrValue != 0) && (EEPROMUsrValue != 1)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==11) { // SpeedPot
        if ((EEPROMUsrValue < 40) || (EEPROMUsrValue > 100)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==12) { // TurnPotFwdMax
        if ((EEPROMUsrValue > 100) || (EEPROMUsrValue < UserSettings[2])) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==13) { // TurnPotRevMax
        if ((EEPROMUsrValue > 100) || (EEPROMUsrValue < UserSettings[3])) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==15) { // SpeedPotFwdMax
        if ((EEPROMUsrValue > 100) || (EEPROMUsrValue < UserSettings[14])) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if ((i==16) || (i==17)) { // SpeedPotLowFault, SpeedPotHighFault
        if ((EEPROMUsrValue > 100) || (EEPROMUsrValue < 0)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if ((i>=19) && (i<=30)) { // switch driving settings
        if ((EEPROMUsrValue <= 0) || (EEPROMUsrValue > 100)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==32) { // TimeToSleep
        if ((EEPROMUsrValue <= 0) || (EEPROMUsrValue >= 150)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if ((i>=33) && (i<=35)) { // Slow1,Slow2,LimpSpeed
        if ((EEPROMUsrValue > 100) || (EEPROMUsrValue < 0)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
// RANGE MODIFIED
      else if (i==62){ // LoadBoost_P
        if ((EEPROMUsrValue > 100) || (EEPROMUsrValue < 0)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==41){ // BackStickBraking
        if ((EEPROMUsrValue > 15) || (EEPROMUsrValue < 0)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==39){ // TurnBoost
        if ((EEPROMUsrValue > 20) || (EEPROMUsrValue < 1)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==42) { // LowIBoost
        if ((EEPROMUsrValue < 0) || (EEPROMUsrValue > 100)){
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==61){ // CompTurnBoost
        if ((EEPROMUsrValue > 200) || (EEPROMUsrValue < 0)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==59){ // LoadBoost_D
        if ((EEPROMUsrValue > 255) || (EEPROMUsrValue < 0)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==60) { // CompDeadband
        if ((EEPROMUsrValue > 40) || (EEPROMUsrValue < 0)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if ((i>=49) && (i<=55)){ // Roboteq pin assignments
        if ((EEPROMUsrValue <= 0) || (EEPROMUsrValue >= 15)) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if ((i==76) || (i==78) || (i==92) || (i==94) || (i==82) || (i==96)) {
// highByte VoltsFull, VoltsEmpty, RechargeVolts, LowVoltLimit, AHrEmpty, MotorResistance
        if (EEPROMUsrValue > 0b10) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if (i==80) { // highByte(AHrFull)
        if (EEPROMUsrValue > 0b100) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
      else if ((i==84) || (i==86)) { // highByte(Accel),highByte(Decel)
        if (EEPROMUsrValue > 0b100111) {
          EEPROMfaultPrint (i);
          EEPROMfault = true;
        }
      }
    } // end if (UserSettings[i] != EEPROMUsrValue)
  } // end for (byte i = 0; i <= sizeof(UserSettings)-1; i++)
  if (EEPROMfault){ // use Default user settings if 30 values changed
                    // or absurd value in an altered parameter
    EEPROM.update (NewMCUFlag, 0b11111111);
    // Serial.println (F("************************************************"));
    // Serial.println (F("EEPROMfault, will reset to default user settings"));
    // Serial.println (F("************************************************"));
  }
  else {
    EEPROM.update (NewMCUFlag, 0b1010101);
  }
} // end CK_UserEEPROM

void EEPROMfaultPrint (byte i) {
/*
  Serial.print (F("EEPROMfault at i = "));
  Serial.print (i);
  Serial.print (F("  "));
  strcpy_P(LabelBuffer, (char*)pgm_read_word(&(UsrSettingLabels[i])));
  Serial.print (LabelBuffer);
  Serial.print (F("  Default = "));
  Serial.print (UserSettings[i]);
  Serial.print (F("  EEaddress = "));
  Serial.print (UsrAddress + i);
  Serial.print (F("  stored = "));
  Serial.println (EEPROM.read (UsrAddress + i));
*/
} // end EEPROMfaultPrint

void SetDefaultUsr (void) {
  // JOYSTICK AND SPEED POT SETTINGS:
  UserSettings[0] = 19; // SpeedPotRevMin;
  UserSettings[1] = 46; // SpeedPotRevMax;
  UserSettings[2] = 11; // TurnPotFwdMin;
  UserSettings[3] = 11; // TurnPotRevMin;
  UserSettings[4] = 80; // TurnAtFullSpeed;
  UserSettings[5] = 0; // ThrottleCurving;
  UserSettings[6] = 0; // SteeringCurving;
  UserSettings[7] = 5; // Deadband;
  UserSettings[8] = 5; // Damping;
  UserSettings[9] = 40; // JswitchTrigger;
  UserSettings[10] = YES; // SpeedPotInstalled;
  UserSettings[11] = 90; // SpeedPot;
  UserSettings[12] = 16; // TurnPotFwdMax;
  UserSettings[13] = 14; // TurnPotRevMax;
  UserSettings[14] = 27; // SpeedPotFwdMin;
  UserSettings[15] = 100; // SpeedPotFwdMax;
  UserSettings[16] = 20; // SpeedPotLowFault;
  UserSettings[17] = 70; // SpeedPotHighFault;
  UserSettings[18] = 0; // SpeedPotFilter now in Defines.h
  // SWITCH DRIVING SETTINGS:
  UserSettings[19] = 10; // Drive1FwdSpeed;
  UserSettings[20] = 7; // Drive1RevSpeed;
  UserSettings[21] = 12; // Drive1FwdTurnRate;
  UserSettings[22] = 10; // Drive1RevTurnRate;
  UserSettings[23] = 20; // Drive2FwdSpeed;
  UserSettings[24] = 15; // Drive2RevSpeed;
  UserSettings[25] = 16; // Drive2FwdTurnRate;
  UserSettings[26] = 14; // Drive2RevTurnRate;
  UserSettings[27] = 30; // Drive3FwdSpeed;
  UserSettings[28] = 20; // Drive3RevSpeed;
  UserSettings[29] = 18; // Drive3FwdTurnRate;
  UserSettings[30] = 15; // Drive3RevTurnRate;
  UserSettings[31] = 4;  // LoadBoostCurving; 
  // CHAIR SETUP:
  UserSettings[32] = 3; // TimeToSleep;
  // SPEED REDUCTIONS
  UserSettings[33] = 40; // Slow1;
  UserSettings[34] = 80; // Slow2;
  UserSettings[35] = 80; // LimpSpeed; - Roboteq motor open speed
  UserSettings[36] = 0; // obsolete AccelMix
  UserSettings[37] = 0; // obsolete LoadBoost_I
  UserSettings[38] = 0; // obsolete SpeedBoost
  UserSettings[39] = 2; // TurnBoost
  UserSettings[40] = 0; // obsolete MinPWM
  UserSettings[41] = 5; // BackStickBraking // multiplied *10 in Roboteq script
  UserSettings[42] = 40; // LowIBoost
  UserSettings[43] = 0; // obsolete TransitionPWM
  // Roboteq connections
  UserSettings[44] = 1; // UseCurrentSensors
  UserSettings[45] = 5; // LeftSensorPin
  UserSettings[46] = 6; // RightSensorPin
  UserSettings[47] = 1; // LeftSensorReversePolarity
  UserSettings[48] = 1; // RightSensorReversePolarity
  UserSettings[49] = 5; // TiltForwardPin;
  UserSettings[50] = 4; // TiltBackPin;
  UserSettings[51] = 1; // LiftUpPin;
  UserSettings[52] = 2; // LiftDownPin;
  UserSettings[53] = 7; // Brake1Pin;
  UserSettings[54] = 8; // Brake2Pin;
  UserSettings[55] = 3; // ContactorPin;
  UserSettings[56] = NO; // SwapMotors;
  UserSettings[57] = NO; // Motor1ReversePolarity;
  UserSettings[58] = NO; // Motor2ReversePolarity;
  UserSettings[59] = 50; // LoadBoost_D;
  UserSettings[60] = 20; // CompDeadband
  UserSettings[61] = 30; // CompTurnBoost
  UserSettings[62] = 0; // LoadBoost_P
  for (byte i = 63; i<= 75; i++){
    UserSettings[i] = 0; // unused
  }
// DISPLAY MESSAGING
  VoltsFull = 250;
  UserSettings[76] = highByte(VoltsFull);
  UserSettings[77] = lowByte(VoltsFull);
  VoltsEmpty = 234;
  UserSettings[78] = highByte(VoltsEmpty);
  UserSettings[79] = lowByte(VoltsEmpty);
  AHrFull = 570;
  UserSettings[80] = highByte(AHrFull);
  UserSettings[81] = lowByte(AHrFull);
  AHrEmpty = 57;
  UserSettings[82] = highByte(AHrEmpty);
  UserSettings[83] = lowByte(AHrEmpty);
// Roboteq SETTINGS
  Accel = 2100;
  UserSettings[84] = highByte(Accel);
  UserSettings[85] = lowByte(Accel);
  Decel = 3100;
  UserSettings[86] = highByte(Decel);
  UserSettings[87] = lowByte(Decel);
  UserSettings[88] = 0; // unused
  UserSettings[89] = 0; // unused
  UserSettings[90] = 0; // unused
  UserSettings[91] = 0; // unused
// battery settings
  RechargeVolts = 258;
  UserSettings[92] = highByte(RechargeVolts);
  UserSettings[93] = lowByte(RechargeVolts);
  LowVoltLimit = 236;
  UserSettings[94] = highByte(LowVoltLimit);
  UserSettings[95] = lowByte(LowVoltLimit);
  MotorResistance = 105;
  UserSettings[96] = highByte(MotorResistance);
  UserSettings[97] = lowByte(MotorResistance);
} // end SetDefaultUsr

void DefaultUsrToEEPROM (void) {
  for (byte i = 0; i <= sizeof(UserSettings) - 1; i++) {
    EEPROM.update (UsrAddress + i, UserSettings[i]);
  }
} // end DefaultUsrToEEPROM

void UsrFromEEPROM (void) {
  for (byte i = 0; i <= sizeof(UserSettings) - 1; i++) {
    UserSettings[i] = EEPROM.read (UsrAddress + i);
  }
  // JOYSTICK AND SPEED POT SETTINGS:
  SpeedPotRevMin = UserSettings[0];
  SpeedPotRevMax = UserSettings[1];
  TurnPotFwdMin = UserSettings[2];
  TurnPotRevMin = UserSettings[3];
  TurnAtFullSpeed = UserSettings[4];
  ThrottleCurving = UserSettings[5];
  SteeringCurving = UserSettings[6];
  Deadband = UserSettings[7];
  Damping = UserSettings[8];
  // make sure damping >=1 and <= number of cells in ThrottleStack and SteeringStack
  if (Damping < 1) {
    Damping = 1;
  }
  else if (Damping > 20) {
    Damping = 20;
  }
  JswitchTrigger = UserSettings[9];
  SpeedPotInstalled = UserSettings[10];
  SpeedPot = UserSettings[11];
  TurnPotFwdMax = UserSettings[12];
  TurnPotRevMax = UserSettings[13];
  SpeedPotFwdMin = UserSettings[14];
  SpeedPotFwdMax = UserSettings[15];
  SpeedPotLowFault = UserSettings[16];
  SpeedPotHighFault = UserSettings[17];
  // SWITCH DRIVING SETTINGS:
  Drive1FwdSpeed = UserSettings[19];
  Drive1RevSpeed = UserSettings[20];
  Drive1FwdTurnRate = UserSettings[21];
  Drive1RevTurnRate = UserSettings[22];
  Drive2FwdSpeed = UserSettings[23];
  Drive2RevSpeed = UserSettings[24];
  Drive2FwdTurnRate = UserSettings[25];
  Drive2RevTurnRate = UserSettings[26];
  Drive3FwdSpeed = UserSettings[27];
  Drive3RevSpeed = UserSettings[28];
  Drive3FwdTurnRate = UserSettings[29];
  Drive3RevTurnRate = UserSettings[30];
  LoadBoostCurving = UserSettings[31];
  // CHAIR SETUP:
  TimeToSleep = UserSettings[32];
  Slow1 = UserSettings[33];
  Slow2 = UserSettings[34];
  LimpSpeed = UserSettings[35];
  TurnBoost = UserSettings[39];
  BackStickBraking = UserSettings[41];
  LowIBoost = UserSettings[42];
  UseCurrentSensors = UserSettings[44];
  // Roboteq connections
  LeftSensorPin = UserSettings[45];
  RightSensorPin =UserSettings[46];
  LeftSensorReversePolarity = UserSettings[47];
  RightSensorReversePolarity = UserSettings[48];
  TiltForwardPin = UserSettings[49];
  TiltBackPin = UserSettings[50];
  LiftUpPin = UserSettings[51];
  LiftDownPin = UserSettings[52];
  Brake1Pin = UserSettings[53];
  Brake2Pin = UserSettings[54];
  ContactorPin = UserSettings[55];
  SwapMotors = UserSettings[56];
  Motor1ReversePolarity = UserSettings[57];
  Motor2ReversePolarity = UserSettings[58];
  LoadBoost_D = UserSettings[59];
  CompDeadband = UserSettings[60];
  CompTurnBoost = UserSettings[61];
  LoadBoost_P = UserSettings[62];
// DISPLAY MESSAGING
  VoltsFull = (unsigned int)UserSettings[76] << 8 | UserSettings[77];
  VoltsEmpty = (unsigned int)UserSettings[78] << 8 | UserSettings[79];
  AHrFull = (unsigned int)UserSettings[80] << 8 | UserSettings[81];
  AHrEmpty = (unsigned int)UserSettings[82] << 8 | UserSettings[83];
// Roboteq SETTINGS
  Accel = (unsigned int)UserSettings[84] << 8 | UserSettings[85];
  Decel = (unsigned int)UserSettings[86] << 8 | UserSettings[87];
// battery settings
  RechargeVolts = (unsigned int)UserSettings[92] << 8 | UserSettings[93];
  LowVoltLimit = (unsigned int)UserSettings[94] << 8 | UserSettings[95];
  MotorResistance = (unsigned int)UserSettings[96] << 8 | UserSettings[97];
// set pin assignment variable used in Master to values sent to Roboteq
  RoboteqForwardPin = TiltForwardPin;
  RoboteqBackPin = TiltBackPin;
  RoboteqUpPin = LiftUpPin;
  RoboteqDownPin = LiftDownPin;
  RoboteqContactorPin = ContactorPin;
  RoboteqBrake1Pin = Brake1Pin;
  RoboteqBrake2Pin = Brake2Pin;
  RoboteqForwardBit = RoboteqForwardPin-1;
  RoboteqBackBit = RoboteqBackPin-1;
  RoboteqUpBit = RoboteqUpPin-1;
  RoboteqDownBit = RoboteqDownPin-1;
  RoboteqContactorBit = RoboteqContactorPin-1;
  RoboteqBrake1Bit = RoboteqBrake1Pin-1;
  RoboteqBrake2Bit = RoboteqBrake2Pin-1;
}  // end UsrFromEEPROM

void SET_RoboteqSensors (void) {
  byte DATA [8];
  DATA [0] = LimpSpeed;
  DATA [1] = UseCurrentSensors;
  DATA [2] = LeftSensorReversePolarity;
  DATA [3] = RightSensorReversePolarity;
  for (byte i = 0; i <= 3; i++) {
    DATA [i + 4] = ~DATA [i];
  }
  SEND_MESSAGE (8, Sensors_id, DATA);
} // end SET_RoboteqSensors

void SET_RoboteqAccel (void) {
  byte DATA [8];
  DATA [0] = highByte(Accel);
  DATA [1] = lowByte(Accel);
  DATA [2] = highByte (Decel);
  DATA [3] = lowByte (Decel);
  for (byte i = 0; i <= 3; i++) {
    DATA [i + 4] = ~DATA [i];
  }
  SEND_MESSAGE (8, Accel_id, DATA);
} // end SET_RoboteqAccel

void SET_RoboteqMotorComp (void) {
  byte DATA [8];
  DATA [0] = highByte(MotorResistance);
  DATA [1] = lowByte(MotorResistance);
  DATA [2] = CompDeadband;
  DATA [3] = LoadBoostCurving;
  for (byte i = 0; i <= 3; i++) {
    DATA [i + 4] = ~DATA [i];
  }
  SEND_MESSAGE (8, MotorComp_id, DATA);
} // end SET_RoboteqMotorComp

void SET_RoboteqMotorComp2 (void) {
  byte DATA [8];
  DATA [0] = LowIBoost;
  DATA [1] = LoadBoost_P;
  DATA [2] = LoadBoost_D;
  DATA [3] = 0;
  for (byte i = 0; i <= 3; i++) {
    DATA [i + 4] = ~DATA [i];
  }
  SEND_MESSAGE (8, MotorComp2_id, DATA);
} // end SET_RoboteqMotorComp2

void SET_RoboteqBoosts (void) {
  byte DATA [8];
  DATA [0] = 0;
  DATA [1] = TurnBoost;
  DATA [2] = CompTurnBoost;
  DATA [3] = BackStickBraking;
  for (byte i = 0; i <= 3; i++) {
    DATA [i + 4] = ~DATA [i];
  }
  SEND_MESSAGE (8, Boosts_id, DATA);
} // end SET_RoboteqBoosts

void SET_RoboteqPins1 (void) {
  byte DATA [8];
  DATA[0] = TiltForwardPin;
  DATA [1] = TiltBackPin;
  DATA [2] = LiftUpPin;
  DATA [3] = LiftDownPin;
  for (byte i = 0; i <= 3; i++) {
    DATA [i + 4] = ~DATA [i];
  }
  SEND_MESSAGE (8, Pins1_id, DATA);
} // end SET_RoboteqPins1

void SET_RoboteqPins2 (void) {
  byte DATA [8];
  DATA[0] = LeftSensorPin;
  DATA [1] = RightSensorPin;
  DATA [2] = Brake1Pin;
  DATA [3] = Brake2Pin;
  for (byte i = 0; i <= 3; i++) {
    DATA [i + 4] = ~DATA [i];
  }
  SEND_MESSAGE (8, Pins2_id, DATA);
} // end SET_RoboteqPins2

void SET_RoboteqMotors (void) {
  byte DATA [8];
  DATA[0] = SwapMotors;
  DATA [1] = Motor1ReversePolarity;
  DATA [2] = Motor2ReversePolarity;
  DATA [3] = ContactorPin;
  for (byte i = 0; i <= 3; i++) {
    DATA [i + 4] = ~DATA [i];
  }
  SEND_MESSAGE (8, Motors_id, DATA);
} // end SET_RoboteqMotors

void SlowAccel (void){
  unsigned int ModAccel;
  unsigned int ModDecel;
  if (SetSlow == VerySlow) {
    ModAccel = (int)Slow1*Accel;
    ModDecel = (int)Slow1*Decel;
  }
  else if (SetSlow == Slow){
    ModAccel = (int)Slow2*Accel;
    ModDecel = (int)Slow2*Decel;
  }
  byte DATA [8];
  DATA [0] = highByte(ModAccel);
  DATA [1] = lowByte(ModAccel);
  DATA [2] = highByte (ModDecel);
  DATA [3] = lowByte (ModDecel);
  for (byte i = 0; i <= 3; i++) {
    DATA [i + 4] = ~DATA [i];
  }
  SEND_MESSAGE (8, Accel_id, DATA);
} // SlowAccel

void CK_RoboteqFLAGS (void) {
  if ((RoboteqFaultFlag) && (RoboteqFaultFlag != 0b00010000)) {
    /*
    Roboteq Fault Flags (_FF or _FLTFLAG)
     f1 = overheat (bit 0)
     f2 = overvoltage (bit 1)
     f3 = undervoltage (bit 2)
     f4 = short circuit (bit 3)
     f5 = emergency stop (bit 4)
     f6 = Sepex excitation fault (bit 5) // irrelevant
     f7 = MOSFET failure (bit 6)
     f8 = startup configuration fault (bit 7)
     */
    RoboteqNeutral ();
    Update_Total_Time ();
    SEND_DISPLAY_MESSAGE1 (_FFLAG, RoboteqFaultFlag);
    RoboteqFaultFlag = 0;
  }
  if ((RoboteqStatusFlag) && (RoboteqStatusFlag != 0b10000001)) {
    /*
    Roboteq Status Flags (_FS or _STFLAG)
     f1 = Serial mode (bit 0)
     f2 = Pulse mode (bit 1)
     f3 = Analog mode (bit 2)
     f4 = Spektrum mode (bit 3)
     f5 = Power stage off (bit 4)
     f6 = Stall detected (bit 5)
     f7 = At limit (bit 6)
     f8 = MicroBasic script running (bit 7)
     BITS 1, 2 & 3 are non-critical FAULT conditions
     BITS 4, 5 & 6 are INFORMATION conditions
     BITS 0 and 7 are NORMAL condition and CAN message should not be sent
     */
    RoboteqStatusFlag = 0b10000001;
    Update_Total_Time ();
    SEND_DISPLAY_MESSAGE1 (_SFLAG, RoboteqStatusFlag);
  }
} // end CK_RoboteqFLAGS

void CheckOON (void) {
  boolean OONcondition = false;
  byte SwitchTest = 5;
  byte SwitchFlags = 0;
  READ_JOYSTICK ();
  SwitchTest = digitalRead(FwdPin) + digitalRead(RevPin) + digitalRead(LftPin) + digitalRead(RtPin) + digitalRead(ModePin);
  if ((Throttle) || (Steering) || (SwitchTest != 5)) {
    for (byte i = 1 ; i <= MaxTries; i++) {
      SEAT_MOVE (none);
      delay (1);
      RECEIVE_MESSAGE ();
      if (SendByteDone) {
        break;
      }
    }
    if ((UsrToggle == Attendant) && (Throttle || Steering)) {
      OONcondition = true;
      Update_Total_Time ();
      SEND_DISPLAY_MESSAGE1 (_MODE, UsrToggle << 3 | Mode);
      SEND_DISPLAY_MESSAGE1 (_OON, 1);
    } // if ((UsrToggle == Attendant)&&(Throttle||Steering))
    else if ((UsrToggle == Driver) && (SwitchTest != 5)) {
      byte nClosed = 0;
      // closed switches: 1=multiple, 2=Fwd, 4=Rev, 8=Lft, 16=Rt, 32=Mode
      for (byte i = 0; i <= 4; i++) {
        byte Sw = digitalRead (FwdPin + i);
        if (Sw == ON) {
          bitWrite (SwitchFlags, i + 1, 1);
          nClosed = nClosed + 1;
        }
      }
      if (nClosed > 1) {
        SwitchFlags = 1;
      }
      OONcondition = true;
      Update_Total_Time ();
      SEND_DISPLAY_MESSAGE1 (_MODE, UsrToggle << 3 | Mode);
      SEND_DISPLAY_MESSAGE1 (_SWCLOSED, SwitchFlags);
    } // else if (UsrToggle = Driver)&&(SwitchTest != 5)
  } // end if ((Throttle) || (Steering) || (SwitchTest != 5))
  while (OONcondition) {
    UsrToggle = digitalRead (UsrTogglePin);
    if (UsrToggle != lastUsrToggle) { // UsrToggle changed
      OONcondition = false;
      SEND_DISPLAY_MESSAGE1 (_SWCLOSED, 0);
      SEND_DISPLAY_MESSAGE1 (_OON, 0);
      lastCANMessageTime = millis ();
    }
    else { // UsrToggle NOT changed
      if (UsrToggle == Attendant) {
        READ_JOYSTICK ();
        if ((!Throttle) && (!Steering) || (CalibrationRunning)) {
          OONcondition = false;
          SEND_DISPLAY_MESSAGE1 (_OON, 0);
          lastCANMessageTime = millis ();
        }
      }
      else if (UsrToggle == Driver) {
        SwitchTest = digitalRead(FwdPin) + digitalRead(RevPin) + digitalRead(LftPin) + digitalRead(RtPin) + digitalRead(ModePin);
        if ((SwitchTest == 5) || (CalibrationRunning)) {
          OONcondition = false;
          SEND_DISPLAY_MESSAGE1 (_SWCLOSED, 0);
          lastCANMessageTime = millis ();
        }
      }
    }
  } // end of holding for: while (OONcondition)
} // end CheckOON

void CK_CONTACTOR (void) {
/*
Roboteq contactor pin = LOW gives low-side switch HIGH means Contactor pulled in
Roboteq contactor pin = HIGH give low-side switch pulled LOW meand Contactor released

NOTE: decision of whether contacts are open or closed based on InVolts and OutVolts
is done separately for ContactorState = 1 (closed) and ContactorState = 0 (open).
*/
  boolean VoltsFound = false;
  boolean Contacts;
  static byte FusedHits = 0;
  static byte StuckHits = 0;
  byte NonFusedSetSlow = SetSlow;
  static boolean ContactorErrorMsgSent = false;
  if (!CheckVoltsDone || ((InVolts == 0) && (OutVolts == 0))) {
    CHECK_VOLTS ();
  }
  else {
    CheckVoltsDone = false;
    VoltsFound = true;
  }
  if (VoltsFound) {
// Serial.print (F("VoltsFound: Contactor state = "));
// Serial.println (ContactorState);
    lastCkContactorTime = millis();
    if (ContactorState > 0) {
// Serial.print (F("  Contactor CLOSED -- "));
     if ((InVolts - OutVolts) <= OpenClosedDifferential + 2) {
        Contacts = CLOSED;
// Serial.println (F("Contacts CLOSED"));
      }
      else {
        Contacts = OPEN;
// Serial.println (F("Contacts OPEN"));
      }
      if (Contacts == OPEN) {
        StuckHits = StuckHits + 1;
        if (StuckHits >= 5) {
          ContactorError = StuckOpen;
          if (!ContactorErrorMsgSent) {
            SEND_DISPLAY_MESSAGE4 (_RAWVOLTS, InVolts, OutVolts);
            delay (10);
            SEND_DISPLAY_MESSAGE1 (_CONTACT, ContactorError);
            ContactorErrorMsgSent = true;
            delay (50);
          }
        }
      }
      else if (Contacts == CLOSED) {
        StuckHits = 0;
        CkContactorArmed = false;
// Serial.println (F("OK - CkContatorArmed = false"));
// Serial.println ();
        if (ContactorError == StuckOpen) {
          SEND_DISPLAY_MESSAGE1 (_CONTACT, ContactorOK);
          ContactorErrorMsgSent = false;
        }
      ContactorError = ContactorOK;
      }
    } // ContactorState > 0
    else { // ContactorState == 0
// Serial.print (F("  Contactor OPEN -- "));
// modified to avoid false fused caused by regeneration increasing OutVolts
      if (((InVolts-OutVolts)>=0)&&((InVolts-OutVolts)<=OpenClosedDifferential-1)) {
        Contacts = CLOSED;
// Serial.println (F("Contacts CLOSED"));
      }
      else {
        Contacts = OPEN;
// Serial.println (F("Contacts OPEN"));
      }
      if (Contacts == CLOSED) {
        FusedHits = FusedHits + 1;
        if (FusedHits >= 15) {
          ContactorError = Fused;
          if (!ContactorErrorMsgSent) {
            SEND_DISPLAY_MESSAGE4 (_RAWVOLTS, InVolts, OutVolts);
            delay (10);
            SEND_DISPLAY_MESSAGE1 (_CONTACT, ContactorError);
            delay (10);
            SEND_DISPLAY_MESSAGE1 (_SETSLOW, VerySlow);
            ContactorErrorMsgSent = true;
            delay (50);
          }
        }
      }
      else { // Contacts == OPEN
        FusedHits = 0;
        CkContactorArmed = false;
// Serial.println (F("OK - CkContatorArmed = false"));
// Serial.println ();
        if (ContactorError == Fused) {
          SEND_DISPLAY_MESSAGE1 (_SETSLOW, NonFusedSetSlow);
          SEND_DISPLAY_MESSAGE1 (_CONTACT, ContactorOK);
          ContactorErrorMsgSent = false;
        }
        ContactorError = ContactorOK;
      } // end if (ContactorState > 0) else
    } // end checking for contactor failures
  } // end if (VoltsFound)
} // end CK_CONTACTOR ()

void CK_BRAKES (void) {
  static byte Errors = 0;
  boolean Brake1Coil;
  boolean Brake2Coil;
  static boolean BrakeErrorMsgSent = false;
  Brake1Coil = bitRead (BrakeStatus, 0); // status pin high=coil OK & brake released,
  Brake2Coil = bitRead (BrakeStatus, 1); // status pin low = coil open
  if (!Brake1Coil || !Brake2Coil) {
    Errors = Errors + 1;
    if (Errors >=3){
      if (Brake1Coil && !Brake2Coil) {
        BrakeError = RightBrake;
      }
      else if (!Brake1Coil && Brake2Coil) {
        BrakeError = LeftBrake;
      }
      else { // both brakes open
        BrakeError = BothBrakes;
      }
      if (!BrakeErrorMsgSent) {
        Update_Total_Time ();
        delay (5);
        SEND_DISPLAY_MESSAGE1 (_BRKOPEN, BrakeError);
        BrakeErrorMsgSent = true;
      }
    } // end if (Errors >=3)
  } // end if (!Brake1Coil || !Brake2Coil)
  else { // both released so circuits OK
    if (BrakeError != BrakesOK) { // remove Brake Open display if problem fixed
      SEND_DISPLAY_MESSAGE1 (_BRKOPEN, BrakesOK);
      BrakeErrorMsgSent = false;
      CheckOON ();
    }
    BrakeError = BrakesOK;
    Errors = 0;
  } // end if (Brake1Coil || Brake2Coil) else
  if (BrakeError == BrakesOK) {
    lastCkBrakesTime = millis ();
  }
} // end CK_BRAKES

void CHECK_CHARGING (void) {
  CHECK_VOLTS ();
  static int HighVolts;
  if (deciVolts >= (RechargeVolts + 12)) { // RechargeVolts currently 260 and charger float = 272
    HighVolts = deciVolts;
    Throttle = 0;
    Steering = 0;
    SEND_DISPLAY_MESSAGE1 (_CHARGER, Charging);
    delay (100);
  }
  else if ((deciVolts < RechargeVolts + 12) || (deciVolts < HighVolts)) {
    if (Charging) {
      Charging = false;
      SEND_DISPLAY_MESSAGE1 (_CHARGER, Charging);
    }
  }
} // end CHECK_CHARGING

void RUN_CHAIR (void) {
  RECEIVE_MESSAGE (); // make sure things like SetSlow messages get
  // read before being stuck in confirmation while loops.
  CK_RoboteqFLAGS ();
  READ_ModeSwitch ();
  READ_UsrToggle (); // includes ckOON and resets ModeMsg = true if toggle position changed
  DO_MODE ();
  ModeMsg = false;
} // end RUN_CHAIR

void CHECK_CURRENT (void) {
  static int milliAHr;
  if (LoopCycles == 0) {
    milliAHr = 0;
  }
  if (!CheckAmpsDone) {
    CHECK_AMPS ();
  }
  else { // CheckAmpsDone
    long TimeSinceLastAmps = millis () - lastAmpsTime;
    int parasiticLoad = 17; // deciAmps without contactor
    if (ContactorState != 0) {
      parasiticLoad = parasiticLoad + 17; // added load when contactor closed
    }
    int addMilliAHr = (long)((long)(deciAmps + parasiticLoad) * TimeSinceLastAmps)/36000;
    milliAHr = milliAHr + addMilliAHr ;
    if (milliAHr >= 500) {
      halfAHr = halfAHr + 1;
      EEPROM.update (AHrAddress, halfAHr);
      //each halfAHr unit is 1/2 amp hour, AHrFull and AHrEmpty are deci amp hours
      byte DisplayAHr = 100 - ((halfAHr * 5 * 100) / (AHrFull - AHrEmpty));
      SEND_DISPLAY_MESSAGE1 (_AHR, DisplayAHr);
      milliAHr = 0;
    }
    if (ContactorState != 0) {
      lastAmpsTime = millis ();
      CheckAmpsDone = false;
    }
    else { // ContactorState = 0
      if (!CheckVoltsDone) {
        CHECK_VOLTS ();
      }
      else {
        lastAmpsTime = millis ();
        CheckAmpsDone = false;
        CheckVoltsDone = false;
      } // end if (!CheckVoltsDone) else
    } // end (ContactorState != 0) else
  } // end if (!CheckAmpsDone) else
} // end CHECK_CURRENT

void CHECK_AMPS (void) {
  GET_INT (BatAmps_id, _AMPERR);
  if (!GetIntDone) {
    CheckAmpsDone = false;
  }
  else {
    CheckAmpsDone = true;
  }
} // end CHECK_AMPS

void CHECK_VOLTS (void) {
  GET_LONG (Volts_id, _VOLTERR);
  if (!GetLongDone) {
    CheckVoltsDone = false;
  }
  else {
    CheckVoltsDone = true;
    GetLongDone = false;
    if (ContactorState == 0) {
      CALC_QUIESCENT_VOLTS ();
    }
  }
} // end of CHECK_VOLTS

void CALC_QUIESCENT_VOLTS (void) {
  unsigned int QuiescentVolts = 237;
  static unsigned int SumVolts = 0;
  static byte VoltReads = 0;
  byte LowVolts = 0;
  static byte LastLowVolts = LowVolts;
  // SetSlow has been changed within this procedure
  VoltReads = VoltReads + 1;
  SumVolts = SumVolts + deciVolts;
  if (VoltReads == 5) {
    QuiescentVolts = SumVolts / 5;
    VoltReads = 0;
    SumVolts = 0;
    // SEND QuiescentVolts TO DISPLAY NODE AND USE TO CHANGE SetSlow
    if (QuiescentVolts < 200) { // running from PC brick
      QuiescentVolts = QuiescentVolts + 70;
    }
    DisplayVolts = ((QuiescentVolts - VoltsEmpty) * 100) / (VoltsFull - VoltsEmpty);
    SEND_DISPLAY_MESSAGE1 (_DVOLTS, DisplayVolts);
Serial.print (F("DisplayVolts = "));
Serial.println (DisplayVolts);
    // do not change setslow if operating from power supply of less than 19 Volts
    if ((QuiescentVolts <= LowVoltLimit) && (QuiescentVolts > 190)) {
      LowVolts = 1;
    }
    else {
      LowVolts = 0;
    }
    if (LastLowVolts != LowVolts) {
      LastLowVolts = LowVolts;
      if (LastLowVolts == 0) {
        SetSlow = FullSpeed;
      }
      else {
        SetSlow = SetSlow - LastLowVolts;
        if (SetSlow < 0){
          SetSlow = 0;
        }
      }
      if (RUNstate == true) {
        SEND_DISPLAY_MESSAGE1 (_SETSLOW, SetSlow);
      }
    }
    if (RUNstate == true) {
      SEND_DISPLAY_MESSAGE1 (_SPEEDPOT, DisplaySpeedPot);
    }
  } // end if VoltReads == 5
} // end CALC_QUIESCENT_VOLTS ()

void READ_ModeSwitch (void) {
  if (digitalRead (ModePin) == pressed) {
    UPDATE_DISPLAY ();
    if (UsrToggle == Attendant) {
      if (!Throttle && !Steering) { // joystick neutral
        if (MODEarmed == true) {
          MODEarmed = false;
          ModeMsg = true;
          NextMode ();
        }
      }
      else if (!OONmsgSent) { // joystick out of neutral
        SEND_DISPLAY_MESSAGE1 (_OON, 1);
        OONmsgSent = true;
      }
    } // end if (UsrToggle == Attendant)
    else { // (UsrToggle == Driver)
      byte SwitchesOpenTest = digitalRead(FwdPin) + digitalRead(RevPin) + digitalRead(LftPin) + digitalRead(RtPin);
      if (SwitchesOpenTest == 4) { // no direction switches pressed
        if (MODEarmed) {
          MODEarmed = false;
          ModeMsg = true;
          NextMode ();
        }
      }
      else if (!SwMsgSent) { // one or more direction switches pressed
        byte SwitchFlags = 0;
        byte nClosed = 0;
        // closed switches: 1=multiple, 2=Fwd, 4=Rev, 8=Lft, 16=Rt, 32=Mode
        for (byte i = 0; i <= 3; i++) {
          byte Sw = digitalRead (FwdPin + i);
          if (Sw == ON) {
            bitWrite (SwitchFlags, i + 1, 1);
            nClosed = nClosed + 1;
          }
        }
        if (nClosed > 1) {
          SwitchFlags = 1;
        }
        SEND_DISPLAY_MESSAGE1 (_SWCLOSED, SwitchFlags);
        SwMsgSent = true;
      } // end one or more direction switches pressed
      delay (20); // extra debounce needed when UsrToggle == Driver because loop() is very fast
    } // end else (UsrToggle == Driver)
    delay (20); // debounce delay
  } // end mode switch pressed
  else { // mode switch is not pressed
    if (OONmsgSent) {
      SEND_DISPLAY_MESSAGE1 (_OON, 0);
      OONmsgSent = false;
    }
    else if (SwMsgSent) {
      SEND_DISPLAY_MESSAGE1 (_SWCLOSED, 0);
      SwMsgSent = false;
    }
    ModeMsg = false;
    MODEarmed = true;
  }
} // end of READ_ModeSwitch

void NextMode (void) {
  if ((UsrToggle == Attendant) && (Mode < Seat)) {
    Mode = Seat;
  }
  else {
    Mode = Mode + 1;
  }
  if (Mode > NumberOfModes) {
    Mode = 1;
  }
  EEPROM.update (ModeAddress, Mode);
} // end of NextMode

void READ_UsrToggle (void) {
  UsrToggle = digitalRead (UsrTogglePin);
  if (UsrToggle != lastUsrToggle) {
    lastUsrToggle = UsrToggle;
    if (SleepUsrToggle) {
      SleepUsrToggle = false;
      delay (10); // give extra time for other nodes to come on line
    }
    CheckOON ();
    // make sure to be in Drive1 if switching toggle to Driver
    if ((UsrToggle == Driver) && ((Mode == Drive1) || (Mode == Drive2) || (Mode == Drive3))) {
      Mode = Drive1;
    }
    ModeMsg = true;
    UPDATE_DISPLAY ();
  } // end (UsrToggle != lastUsrToggle)
} // end of READ_UsrToggle

void GO_TO_SLEEP (void) {
  // must turn off lights before nonMaster (Sleep) or Aux node will not go to sleep
  LightsFlags = 0b1111;
  SET_LIGHTS ();
  oldLightsFlags = 0b1111;
  delay (100);
  nonMASTER (Sleep);
  delay (1); // delay needed so that nonMASTER (Sleep) completes
  // before XCVR and/or Master go to bed
  do { // make sure XCVR (sleep) isn't blocked by messages in buffer
    RECEIVE_MESSAGE ();
  }
  while (MsgReceived);
  Charging = true;
  XCVR (Sleep);
  lastXcvrTime = millis ();
  MASTER_SLEEP ();
  // re-enters here after MASTER_WAKE interrupt service routine
  ADCSRA = old_ADCSRA; // restore ADC to state it was in before sleep
  // check that not attempting to drive while charger plugged in during sleep
} // end GO_TO_SLEEP

void WAKE_UP (void) {
  // to wake from sleep with OnOffPin LOW
  if (digitalRead (OnOffPin) == LOW) {
    XCVR (Wake);
    nonMASTER (Wake);
    JustAwakened = true;
    lastAmpsTime = millis ();
    lastMoveTime = lastAmpsTime;
    lastXcvrTime = lastAmpsTime;
    // to prevent check for OON in READ_UsrToggle if toggled while in sleep
    SleepUsrToggle = true;
    for (byte i = 1 ; i < 10; i++) {
      SEND_MESSAGE (0, Aux_Dummy, NULL);
      SEND_MESSAGE (0, Display_Dummy, NULL);
      delay (1);
    }
    // make sure pin has gone HIGH before detachInterrupt
    // or MASTER_SLEEP may re-trigger
    while (digitalRead (OnOffPin) == LOW) {
      digitalRead (OnOffPin);
      delay (1);
    }
    detachInterrupt (0);
  } // wake from sleep with OnOffPin LOW
  // force display update if toggle changed while asleep
  if (digitalRead (UsrTogglePin) != lastUsrToggle) {
    ToggleChangeInSleep = true;
  }
  CheckOON ();
} // end of WAKE_UP

void DO_MODE (void) {
  if (!ModeMsg) { // update screen - needed particularly if
    // UsrToggle is switched back and forth very quickly
    int UpdateCycles;
    if (UsrToggle == Attendant) {
      UpdateCycles = CyclesPerDisplayUpdate;
    }
    else {
      UpdateCycles = CyclesPerDisplayUpdate*4;
    }
    if (LoopCycles % UpdateCycles == 0) {
      SEND_DISPLAY_MESSAGE1 (_MODE, UsrToggle << 3 | Mode);
    }
  }
  else if (ModeMsg) {
    SEND_DISPLAY_MESSAGE1 (_MODE, UsrToggle << 3 | Mode);
    UPDATE_DISPLAY ();
    lastCANMessageTime = millis ();
  }
  switch (Mode) {
    case Seat:
      DO_SEAT ();
      break;
    case Lights:
      DO_LIGHTS ();
      break;
    case Drive1:
      DO_DRIVE ();
      break;
    case Drive2:
      DO_DRIVE ();
      break;
    case Drive3:
      DO_DRIVE ();
      break;
  } // end switch (Mode)
} // end of DO_MODE

void UPDATE_DISPLAY (void){ // updates display whenever base screen is re-painted
  if (SetSlow < FullSpeed) {
    SEND_DISPLAY_MESSAGE1 (_SETSLOW, SetSlow);
  }
   if (BrakeError != BrakesOK){
    SEND_DISPLAY_MESSAGE1 (_BRKOPEN, BrakeError);
  }
  if (ContactorError != ContactorOK){
    SEND_DISPLAY_MESSAGE1 (_CONTACT, ContactorError);
  }
} // end of UPDATE_DISPLAY

void DO_DRIVE (void) {
  STOPswitch = digitalRead (StopPin); // no debounce, seems OK with tact switch
  // but needed with microswitch when UsrToggle==Driver, thus 2 msec delay added
  // (1 msec OK, but put in 2 to be sure)
  // STOParmed is true if STOP switch up after previous press
  if (STOPswitch == pressed) {
    if (STOParmed) {
      if (RUNstate) {
        RUNstate = false;
        SEND_DISPLAY_MESSAGE1 (_SETSLOW, Stop); // display STOP symbol
      } // end if RUNstate == true
      else { // RUNSstate != true
        RUNstate = true;
        SEND_DISPLAY_MESSAGE1 (_SETSLOW, SetSlow);
      } // end checking RUNstate
      STOParmed = false;
    } // end STOParmed == true
    else {
    } // end checking STOParmed
  } // end STOPswitch == pressed
  else { // STOPswitch not pressed
    STOParmed = true;
  } // end checking STOPswitch
  // delay added to debounce when in switch driving UsrToggle
  if (UsrToggle == Driver) {
    delay (2);
  }
  if (RUNstate) {
    if (!DoAutoStep){
      if (UsrToggle == Attendant) {
        READ_JOYSTICK ();
        // if Damping > 1 use stacks to find running average
        if (Damping > 1) {
          // put new values in arrays using circular index
          static byte i = 0;
          ThrottleStack[i] = Throttle;
          SteeringStack[i] = Steering;
          i = i + 1;
          if (i >= Damping) {
            i = 0;
          }
          // average cells
          int SumThrottle = 0;
          int SumSteering = 0;
          for (byte i = 0; i <= (Damping - 1); i++) {
            SumThrottle = SumThrottle + ThrottleStack[i];
            SumSteering = SumSteering + SteeringStack[i];
          }
          Throttle = SumThrottle / Damping;
          Steering = SumSteering / Damping;
        }
      }
      else { // UsrToggle == Driver
        SWITCH_DRIVING ();
      }
      static byte lastSetSlow = SetSlow;
      if (RUNstate && (SetSlow != lastSetSlow)) {
        lastSetSlow = SetSlow;
        SEND_DISPLAY_MESSAGE1 (_SETSLOW, SetSlow);
      }
      switch (SetSlow) {
        case 0:
          Throttle = 0 * Throttle;
          Steering = 0 * Steering;
          break;
        case 1:
  // modified so that inhibits reduce only Throttle for SetSlow = VerySlow or Slow
          Throttle = ((long)Slow1 * (long)Throttle) / 100;
          // Steering = ((long)Slow1 * (long)Steering) / 100;
          // following reduces Accel/Decel if Slow = 1 or 2
            if (!AccelSet) {
              SlowAccel (); // will keep setting msg to Roboteq at each loop cycle until confirmed
            }
          break;
        case 2:
          Throttle = ((long)Slow2 * (long)Throttle) / 100;
          // Steering = ((long)Slow2 * (long)Steering) / 100;
          // following reduces Accel/Decel if Slow = 1 or 2
            if (!AccelSet) {
              SlowAccel (); // will keep setting msg to Roboteq at each loop cycle until confirmed
            }
      } // end switch (SetSlow)
      if (ContactorError == Fused) {
        Throttle = ((long)Slow2 * (long)Throttle) / 100;
        Steering = ((long)Slow2 * (long)Steering) / 100;
      }

      if ((millis() - lastCkBrakesTime) >= 100){
        SEND_MESSAGE (0, Brakes_id, NULL); // brakes_ret will call CK_BRAKES in this program
      }

    } // end if (!DoAutoStep)
    // to reduce bus traffic, send JOYSTICK_MESSAGE only if values changed or
    // 50 msec have elapsed (Roboteq script has 100 msec joystick message watchdog)
    boolean elapsed;
    elapsed = ((uint32_t)(millis () - lastJoystickMessageTime) > 50);
    if ((LoopCycles % 200 == 0) && (Throttle || Steering)) {
       Serial.print (F("Throttle: "));
       Serial.print (Throttle);
       Serial.print (F("   Steering: "));
       Serial.println (Steering);
    }
    // following runs at startup until charger is disconnected
    while (Charging) {
      CHECK_CHARGING (); // check for attempt to drive with charger plugged in
    }
    if (DoAutoStep) {
      AutoStep ();
    }
    if ((Throttle != lastThrottle) || (Steering != lastSteering) || (elapsed)) {
      SEND_JOYSTICK_MESSAGE ();
      // ****************** (for TESTING only) *******************
      // FOLLOWING SECTION FOR DEMO OR TESTING PURPOSES ONLY - COMMENT OUT BEFORE USE
      //      byte DisplayThrottle = map (Throttle, -1000, 1000, 0, 100);
      //      byte DisplaySteering = map (Steering, (-1000*TurnPotFwdMax)/100, (1000*TurnPotFwdMax)/100, 0, 100);
      //      SEND_DISPLAY_MESSAGE2 (_JOYSTICK, DisplayThrottle, DisplaySteering);
      // PRECEEDING SECTION FOR DEMO OR TESTING PURPOSES ONLY - COMMENT OUT BEFORE USE
      // ****************** (for TESTING only) *******************
    } // end check Throttle or Steering change or elapsed (currently 50 msec)
  } // end if RUNstate == true
  else { // RUNstate = false
    // Throttle, Steering to 0 and driving and mode switches to HIGH
    Throttle = 0;
    Steering = 0;
    lastThrottle = 0;
    lastSteering = 0;
    digitalWrite (FwdPin, HIGH);
    digitalWrite (RevPin, HIGH);
    digitalWrite (LftPin, HIGH);
    digitalWrite (RtPin, HIGH);
    digitalWrite (ModePin, HIGH);
  }
} // end of DO_DRIVE

void DO_SEAT (void) {
  switch (UsrToggle) {
    // chose apprpriate procedure for jostick or switch control
    case (Attendant):
      DO_JS_SEAT ();
      break;
    default:
      DO_SW_SEAT ();
      break;
  } // end switch
} // end of DO_SEAT

void DO_LIGHTS (void) {
  // chose apprpriate procedure for jostick or switch control
  switch (UsrToggle) {
    case (Attendant):
      DO_JS_LIGHTS ();
      break;
    default:
      DO_SW_LIGHTS ();
      break;
  } // end switch
} // end of DO_LIGHTS

void Update_Total_Time (void) {
// Serial.println (F("in Update_Total_Time"));
  unsigned long CurrentTime;
  CurrentTime = millis();
  Interval = (CurrentTime-RunningTime) / 1000;
  static unsigned long ElapsedTime = 0;
  ElapsedTime = ElapsedTime + Interval;
  unsigned long TotalTime = (uint32_t)EEPROM.read (TotalTimeAddress) << 24;
  TotalTime = TotalTime | (uint32_t)EEPROM.read (TotalTimeAddress + 1) << 16;
  TotalTime = TotalTime | (uint16_t)EEPROM.read (TotalTimeAddress + 2) << 8;
  TotalTime = TotalTime | EEPROM.read (TotalTimeAddress + 3);
// Serial.print (F("old TotalTime = "));
// Serial.print (TotalTime);
// Serial.print (F("  Interval = "));
// Serial.print (Interval);
  TotalTime = TotalTime + Interval;
// Serial.print (F("  new TotalTime = "));
// Serial.println (TotalTime);
  byte FirstByte = TotalTime >> 24;
  byte SecondByte = TotalTime >> 16;
  byte ThirdByte = TotalTime >> 8;
  byte FourthByte = TotalTime;
  EEPROM.update (TotalTimeAddress, FirstByte);
  EEPROM.update (TotalTimeAddress + 1, SecondByte);
  EEPROM.update (TotalTimeAddress + 2, ThirdByte);
  EEPROM.update (TotalTimeAddress + 3, FourthByte);
  int ElapsedHours = ElapsedTime / 3600;
  int ElapsedMinutes = (ElapsedTime - ElapsedHours * 3600);
  ElapsedMinutes = ElapsedMinutes / 60;
  // TotalHours and TotalMinutes are global so that they can be
  // sent repeatedly at startup before sending _STARTUP msg to Display
  TotalHours = TotalTime / 3600;
  TotalMinutes = (TotalTime - TotalHours * 3600);
  TotalMinutes = TotalMinutes / 60;
// Serial.print (F("  Display TOTAL time = "));
// Serial.print (TotalHours);
// Serial.print (F(":"));
// Serial.println (TotalMinutes);
// Serial.println ();
// have to SEND_TIME several times to ensure that it's received
// by Display
  for (byte i = 1; i <= 10; i++) {
    SEND_TIME (_RUNTIME, ElapsedHours, ElapsedMinutes);
    delay (5);
  }
  for (byte i = 1; i <= 10; i++) {
    SEND_TIME (_TOTTIME, TotalHours, TotalMinutes);
    delay (5);
  }
  RunningTime = CurrentTime;
} // end Update_Total_Time

void SEAT_MOVE (byte Move) {
  boolean skip = false;
  newDigFlags = DigFlags;
  newDigFlags = bitWrite (newDigFlags, RoboteqForwardBit, 0);
  newDigFlags = bitWrite (newDigFlags, RoboteqBackBit, 0);
  newDigFlags = bitWrite (newDigFlags, RoboteqUpBit, 0);
  newDigFlags = bitWrite (newDigFlags, RoboteqDownBit, 0);
  newDigFlags = bitWrite (newDigFlags, RoboteqContactorBit, 0);
  if (Move != none) {
    newDigFlags = bitWrite (newDigFlags, RoboteqContactorBit, 1);
  }
  switch (Move) {
    // none, forward, back, up, down
    case none:
      break;
    case forward:
      newDigFlags = bitWrite (newDigFlags, RoboteqForwardBit, 1);
      break;
    case back:
      newDigFlags = bitWrite (newDigFlags, RoboteqBackBit, 1);
      break;
    case up:
      newDigFlags = bitWrite (newDigFlags, RoboteqUpBit, 1);
      break;
    case down:
      newDigFlags = bitWrite (newDigFlags, RoboteqDownBit, 1);
      break;
  }
  if (newDigFlags == DigFlags) {
    SendByteDone = true;
//    SendByteReceivedID = Seat_ret;
    skip = true;
  }
  else {
    Byte = DigFlags;
    newByte = newDigFlags;
    SEND_BYTE (DigSet_id, _SEATMSG);
    if (SendByteDone && !skip) {
      DigFlags = Byte;
      newDigFlags = newByte;
      lastSeatMove = Move;
      //     SEND_DISPLAY_MESSAGE1 (_SEAT, lastSeatMove); // used only for testing or demo
      lastCANMessageTime = millis ();
    }
  }
  RECEIVE_MESSAGE ();
} // end SEAT_MOVE

void SET_LIGHTS (void) {
  boolean skip = false;
  if (LightsFlags == oldLightsFlags) {
    SendByteDone = true;
    skip = true;
  }
  else {
    newByte = LightsFlags;
    SEND_BYTE (Lights_id, _LTSMSG);
    if (SendByteDone && !skip) {
//      LightsFlags = Byte; // moved to Lights_ret case
//      oldLightsFlags = Byte; // moved to Lights_ret case
      lastCANMessageTime = millis ();
    }
  }
  RECEIVE_MESSAGE ();
} // end SET_LIGHTS

void SEND_DISPLAY_MESSAGE0 (byte Name) {
  byte Category = SetCommand;
  byte Target = toDisplay;
  unsigned int ID = (uint16_t) (((Category << 6 | Name ) << 3) | Target); // << 5 done in CAN.cpp;
  SEND_MESSAGE (0, ID, NULL);
} // end SEND_DISPLAY_MESSAGE0 ()

void SEND_DISPLAY_MESSAGE1 (byte Name, byte FirstByte) {
  byte Category = SetCommand;
  byte Target = toDisplay;
  byte DATA[2];
  DATA[0] = FirstByte;
  DATA[1] = ~DATA[0];
  unsigned int ID = (uint16_t) (((Category << 6 | Name ) << 3) | Target); // << 5 done in CAN.cpp;
  SEND_MESSAGE (2, ID, DATA);
} // end SEND_DISPLAY_MESSAGE1 ()

void SEND_TIME (byte Name, int Hours, byte Minutes) {
  byte Category = SetCommand;
  byte Target = toDisplay;
  byte DATA[6];
  DATA[0] = highByte (Hours);
  DATA[1] = lowByte (Hours);
  DATA[2] = Minutes;
  for (byte i = 3; i <= 5; i++) {
    DATA[i] = ~DATA[i - 3];
  }
  unsigned int ID = (uint16_t) (((Category << 6 | Name ) << 3) | Target); // << 5 done in CAN.cpp;
  SEND_MESSAGE (6, ID, DATA);
} // end SEND_TIME ()

void SEND_DISPLAY_MESSAGE2 (byte Name, byte FirstByte, byte SecondByte) {
  byte Category = SetCommand;
  byte Target = toDisplay;
  byte DATA[4];
  DATA[0] = FirstByte;
  DATA[1] = SecondByte;
  DATA[2] = ~DATA[0];
  DATA[3] = ~DATA[1];
  unsigned int ID = (uint16_t) (((Category << 6 | Name ) << 3) | Target); // << 5 done in CAN.cpp;
  SEND_MESSAGE (4, ID, DATA);
} // end SEND_DISPLAY_MESSAGE2

void SEND_DISPLAY_MESSAGE4 (byte Name, int FirstVal, int SecondVal) {
  byte Category = SetCommand;
  byte Target = toDisplay;
  byte DATA[8];
  DATA[0] = highByte(FirstVal);
  DATA[1] = lowByte(FirstVal);
  DATA[2] = highByte(SecondVal);
  DATA[3] = lowByte(SecondVal);
  for (byte i = 0; i <= 3; i++) {
    DATA[i + 4] = ~DATA[i];
  }
  unsigned int ID = (uint16_t) (((Category << 6 | Name ) << 3) | Target); // << 5 done in CAN.cpp;
  SEND_MESSAGE (8, ID, DATA);
} // end SEND_DISPLAY_MESSAGE4

void SEND_BYTE (unsigned int ID, byte ErrDef) {
  byte DATA[2];
  static boolean SendByteError = false;
  static byte tries = 0;
  SendByteDone = false;
  if (ReturnedByte != newByte) {
    tries = tries + 1;
    if (tries >= MaxTries) {
      Update_Total_Time ();
      if (!SendByteError) {
        SEND_DISPLAY_MESSAGE0 (ErrDef);
        lastCANMessageTime = millis ();
        SendByteError = true;
      }
      tries = 0;
    } // end if (tries >= MaxTries)
    else { // tries < MaxTries
      DATA[0] = newByte;
      DATA [1] = ~DATA[0];
      SEND_MESSAGE (2, ID, DATA);
      if ((Throttle) || (Steering)) {
        lastCANMessageTime = millis ();
      }
    } // end tries < MaxTries
  } // end if (ReturnedByte != newByte)
  else { // ReturnedByte == newByte
    if (tries > 0) {
      Byte = newByte;
      SendByteDone = true;
    }
    // make sure that ReturnedByte != newByte before re-entering procedure
    // in case they were equal on 0th transmit
    ReturnedByte = newByte + 1;
    tries = 0;
    SendByteError = false;
  } // end if (ReturnedByte != newByte) else
} // end SEND_BYTE

void SEND_INT (unsigned int ID, byte ErrDef) {
  byte DATA[4];
  static boolean SendIntError = false;
  static byte tries = 0;
  SendIntDone = false;
  if (returnedInt != newInt) {
    tries = tries + 1;
    if (tries >= MaxTries) {
      Update_Total_Time ();
      if (!SendIntError) {
        SEND_DISPLAY_MESSAGE0 (ErrDef);
        lastCANMessageTime = millis ();
        SendIntError = true;
      }
      tries = 0;
    }
    else { // tries < MaxTries
      DATA[0] = newInt >> 8;
      DATA[1] = newInt & 0b11111111;
      DATA[2] = ~DATA[0];
      DATA[3] = ~DATA[1];
      SEND_MESSAGE (4, ID, DATA);
    }
  } // end if (returnedInt != newInt)
  else { // returnedInt == newInt
    if (tries > 0) {
      SendIntDone = true;
      Int = newInt;
    }
    // make sure that returnedInt != newInt before re-entering procedure
    // if they were equal on 0th transmit
    returnedInt = newInt + 1;
    tries = 0;
    SendIntError = false;
  } // end if (returnedInt != newInt) else
} // end SEND_INT

/*
void GET_BYTE (uint16_t ID, byte ErrDef) {
  static byte tries = 0;
  static boolean ErrorSentToDisplay = false;
  GetByteDone = false;
  if (!ByteReceived) {
    tries = tries + 1;
    if (tries >= MaxTries) {
      Update_Total_Time ();
      if (!ErrorSentToDisplay) {
        SEND_DISPLAY_MESSAGE0 (ErrDef);
        ErrorSentToDisplay = true;
      }
      tries = 0;
    }
    else {
      SEND_MESSAGE (0, ID, NULL);
    }
  } // end if (!ByteReceived)
  else { // ByteReceived = true because return message received
    if (tries > 0) {
      GetByteDone = true;
    }
    ByteReceived = false;
    tries = 0;
    ErrorSentToDisplay = false;
  }// end if (!ByteReceived) else
  RECEIVE_MESSAGE ();
} // end GET_BYTE
*/

void GET_INT (uint16_t ID, byte ErrDef) {
  static byte tries = 0;
  static boolean ErrorSentToDisplay = false;
  GetIntDone = false;
  if (!IntReceived) {
    tries = tries + 1;
    if (tries >= MaxTries) {
      Update_Total_Time ();
      if (!ErrorSentToDisplay) {
        SEND_DISPLAY_MESSAGE0 (ErrDef);
        ErrorSentToDisplay = true;
      }
      tries = 0;
    }
    else {
      SEND_MESSAGE (0, ID, NULL);
    }
  } // end if (!IntReceived)
  else { // IntReceived = true because return message received
    if (tries > 0) {
      GetIntDone = true;
    }
    IntReceived = false;
    tries = 0;
    ErrorSentToDisplay = false;
  }// end if (!IntReceived) else
  RECEIVE_MESSAGE ();
} // end GET_INT

void GET_LONG (uint16_t ID, byte ErrDef) {
  static byte tries = 0;
  static boolean ErrorSentToDisplay = false;
  GetLongDone = false;
  if (!LongReceived) {
    tries = tries + 1;
    if (tries >= MaxTries) {
      Update_Total_Time ();
      if (!ErrorSentToDisplay) {
        SEND_DISPLAY_MESSAGE0 (ErrDef);
        ErrorSentToDisplay = true;
      }
      tries = 0;
    }
    else {
      SEND_MESSAGE (0, ID, NULL);
    }
  } // end if (!LongReceived)
  else { // LongReceived = true because return message received
    if (tries > 0) {
      GetLongDone = true;
    }
    LongReceived = false;
    tries = 0;
    ErrorSentToDisplay = false;
  }// end if (!LongReceived) else
  RECEIVE_MESSAGE ();
} // end GET_LONG

void MASTER_SLEEP (void) {
  // Serial.println (F("MASTER_SLEEP called"));
  delay (10); // some time needed for serial print
  // disable ADC
  ADCSRA = 0;
  set_sleep_mode (SLEEP_MODE_PWR_DOWN);
  sleep_enable();
  // make sure we can't interrupt before we go to sleep,
  noInterrupts ();
  attachInterrupt (0, MASTER_WAKE, LOW);
  /*
   Arduino.h enumerates:
   #define CHANGE 1
   #define FALLING 2
   #define RISING 3
   #define LOW  0x0
   #define HIGH 0x1
   sets 2 bits in EICRA register
   */
  // turn off brown-out enable in software
  MCUCR = _BV (BODS) | _BV (BODSE);
  MCUCR = _BV (BODS);
  // it is guaranteed that the sleep_cpu call will be done
  // as the processor executes the next instruction after
  // interrupts are turned on.
  interrupts ();
  sleep_cpu ();
} // end of MASTER_SLEEP

void MASTER_WAKE (void) {
  sleep_disable();
}  // end of MASTER_WAKE

void TURN_OFF (void) {
  if (JustAwakened) {
    delay (1000);
  }
  RoboteqNeutral ();
/*
  while (ContactorState != 0){
    GET_BYTE (DigOut_id, _CONMSG);
    delay (100);
    RECEIVE_MESSAGE ();
  }
*/
  Update_Total_Time ();
  // Save time updated during normal turn off as backup and use it at next startup
  // to replace time value that might have been corrupted by power interruption
  // while running.
  for (byte i = 0; i <= 3; i++) {
    EEPROM.update (TotalTimeAddress + i + 4, EEPROM.read (TotalTimeAddress + i));
  }
  nonMASTER (Shutoff);
delay (100);
  digitalWrite (RESET_PIN, LOW); // CAN controller reset = A2, #define in CAN.h
  delay (10);
  // PowerOnPin was set to INPUT in startup,
  // have to set to OUTPUT here before it can be pulled low
  pinMode (PowerOnPin, OUTPUT);
delay (10);
  digitalWrite (PowerOnPin, LOW);
  delay (1000);
} // end TURN_OFF

void SEND_MESSAGE (byte Number, unsigned int ID, byte *DATA) {
  if ((!XcvrSleep) || (ID == DisplaySleepWake_id) || (ID == AuxSleepWake_id)) {
    SendFrame_id = ID;
    if (!NoPrinting) {
      // Serial.println ();
      // Serial.print (F("SendFrame_id = "));
      // Serial.print (SendFrame_id);
      // Serial.print (F(" = 0x"));
      // Serial.print (SendFrame_id, HEX);
      // Serial.print (F(" = 0b"));
      // Serial.println (SendFrame_id, BIN);
      // Serial.println (F("data bytes:"));
      // Serial.print (F("0b"));
      // for (byte i = 0; i < (Number - 1); i++) {
        // Serial.print (DATA[i], BIN);
        // Serial.print (F("   0b"));
      // }
      // Serial.println ();
    }
    /* void CANClass::load_ff_g(byte length, uint32_t *id, byte *data, bool ext) procedure
     added to CAN.cpp and CAN.h so that a CAN.load_ff_g call will send msg
     to Tx buffer 2 if free, then 1 then 0.  Buffer transmit priority is buffer 2, 1, 0
     unless register write is used to change priorities.
     Lower priority messages can be sent by calling e.g. load_ff_0

     2012_1_9: transmission on buffers 0 and 1 work fine, but on buffer
     2 only the ID seems to be sent, but data received are all 0x0 and
     complements = 0xFF.  For now, CAN.load__ff_g changed to send only to
     buffers 0 and 1.
     */
    CAN.load_ff_g(Number, &SendFrame_id, DATA, false);
    //   CAN.load_ff_1(Number,&SendFrame_id,DATA, false);
  }
} // end SEND_MESSAGE

void RECEIVE_MESSAGE (void) {
  MsgReceived = false;
  byte pairs;
  boolean check = true;
  // clear data received buffers, just in case.
  for (byte i = 0; i <= 7; i++) {
    frame_data[i] = 0x00;
  }
  ReceiveFrame_id = 0x00;
  rx_status = CAN.readStatus();
  if (MSG_IN_BUFFER0) {
    CAN.readDATA_ff_0(&length, frame_data, &ReceiveFrame_id, &ext, &filter);
    // printBuf(rx_status, length, ReceiveFrame_id, filter, 0, frame_data, ext);
    pairs = length / 2;
    for (byte j = 0; j <= (pairs - 1); j++) {
      if ((frame_data[j] ^ frame_data[j + pairs]) != 0xFF) {
        // following works, BUT ~frame_data[j] returns unsigned int unless (byte) used
        //      if ((byte)~frame_data[j] == frame_data[j+pairs]){
        check = false;
        break;
      }
    }
  }
  /* in original code from which I lifted this, the following was
   "if" rather than "else if".  Doing that means that a lower-priority
   (buffer 1) message could replace a higher priority (buffer 0) message
   if both buffers were full.  Using "else if" will delay buffer 1 read
   until next pass if another buffer 0 message has not arrived in the
   meantime.
   */
  else if (MSG_IN_BUFFER1) {
    CAN.readDATA_ff_1(&length, frame_data, &ReceiveFrame_id, &ext, &filter);
    // printBuf(rx_status, length, ReceiveFrame_id, filter, 1, frame_data, ext);
    pairs = length / 2;
    for (byte j = 0; j <= (pairs - 1); j++) {
      if ((frame_data[j] ^ frame_data[j + pairs]) != 0xFF) {
        check = false;
        break;
      }
    }
  }
  if ((MSG_IN_BUFFER0) || (MSG_IN_BUFFER1)) {
    if (check) {
      MsgReceived = true;
      CK_INCOMING ();
    } // end check for corrupted message
    else {
      MsgReceived = false;
    }
  } // filtered message in receive buffer
  else {
    MsgReceived = false;
  } // no filtered message in receive buffer
} // end RECEIVE_MESSAGE ()

void CK_INCOMING (void) {
  // uncomment following line to adjust VoltCalibration if Programmer node not available
  //  unsigned int deciOutVolts = 0;
  boolean Brake1Bit = true;
  boolean Brake2Bit = true;
  boolean CkJSDone = false;
  switch (ReceiveFrame_id) {
    case NodeLive_id:
      if (frame_data[0] == 1) {
        DisplayLive = true;
      }
      else if (frame_data[0] == 2) {
        AuxLive = true;
      }
      else if (frame_data[0] == 3) {
        RoboteqLive = true;
      }
      else if (frame_data[0] == 4) {
        StartupLogged = true;
      }
      break;
    case Sensors_ret:
      SensorsSet = true;
      break;
    case Accel_ret:
      AccelSet = true;
      break;
    case Boosts_ret:
      BoostsSet = true;
      break;
    case MotorComp_ret:
      MotorCompSet = true;
      break;
    case MotorComp2_ret:
      MotorComp2Set = true;
      break;
    case Motors_ret:
      MotorsSet = true;
      break;
    case Pins1_ret:
      Pins1Set = true;
      break;
    case Pins2_ret:
      Pins2Set = true;
      break;
    case SetSlow_id:
      SEND_MESSAGE (0, SetSlow_confirm, NULL); // if AuxNode doesn't receive this,
      // it will re-send SetSlow message
      SetSlow = frame_data[0];
      if (RUNstate == true) {
        SEND_DISPLAY_MESSAGE1 (_SETSLOW, SetSlow);
      }
      break;
    case RoboteqFault_id:
      RoboteqFaultFlag = frame_data[0];
      SEND_MESSAGE (0, RoboteqFault_confirm, NULL); // Roboteq will stop re-sending unchanged
      // fault flags once it has received this.
      break;
    case RoboteqStatus_id:
      RoboteqStatusFlag = frame_data[0];
      SEND_MESSAGE (0, RoboteqStatus_confirm, NULL); // if Roboteq doesn't
      // receive this, it will re-send Status message until StatusFlags
      // resets to 0b10000001 = Serial mode with script running
      break;
    case BatAmps_ret:
      deciAmps = (int) frame_data[0] << 8 | frame_data[1];
      // comment out next line to test for error detection
      IntReceived = true;
      break;
    case Volts_ret:
      InVolts = (int) frame_data[0] << 8 | frame_data[1];
      OutVolts = (int) frame_data[2] << 8 | frame_data[3];
// Serial.print (F("InVolts = "));
// Serial.print (InVolts);
// Serial.print (F("   OutVolts = "));
// Serial.print (OutVolts);
// Serial.print (F("   Offset = "));
// Serial.print (VoltageDividerOffset);
      OutVolts = OutVolts + VoltageDividerOffset;
// Serial.print (F("   adj. OutVolts = "));
// Serial.println (OutVolts);
      deciVolts = map (InVolts, 0, 1023, 0, VoltCalibration);
      LongReceived = true;
      break;
    case Brakes_ret:
      ByteReceived = true;
      GetByteDone = true;
      BrakeStatus = frame_data[0];
      CK_BRAKES ();
      break;
    case DigSet_ret:
      ReturnedByte = frame_data[0];
      break;
    case Lights_ret:
      ReturnedByte = frame_data[0];
      LightsFlags = ReturnedByte;
      oldLightsFlags = ReturnedByte;
      break;
    case DigOut_ret:
      ReturnedByte = frame_data[0];
      DigOutFlags = frame_data[0];
      ByteReceived = true;
      ContactorState =  bitRead (DigOutFlags, RoboteqContactorBit);
      RoboteqBrake1State = bitRead (DigOutFlags, RoboteqBrake1Bit);
      RoboteqBrake2State = bitRead (DigOutFlags, RoboteqBrake2Bit);
      RoboteqSeatForward = bitRead (DigOutFlags, RoboteqForwardBit);
      RoboteqSeatBack = bitRead (DigOutFlags, RoboteqBackBit);
      RoboteqSeatUp = bitRead (DigOutFlags, RoboteqUpBit);
      RoboteqSeatDown = bitRead (DigOutFlags, RoboteqDownBit);
// Serial.println ();
// Serial.print (F("DigOut_ret: ContactorState = "));
// Serial.println (ContactorState);
      if (!CkContactorArmed){
        CkContactorArmed = true;
      }
      break;
    case PThrottle_ret:
      ReturnedByte = frame_data[0];
      break;
    case Joystick_ret:
      JoystickConfirmed = true;
      lastThrottle = (int) frame_data[0] << 8 | frame_data[1];
      lastSteering = (int) frame_data[2] << 8 | frame_data[3];
      break;

    case StartStop_id:
      if ((frame_data[0] == 1) && (!ProgrammerRunning)) {
        ProgrammerRunning = true;
        BLOCK_For_Programmer ();
      }
      else if ((frame_data[0] == 0) && (ProgrammerRunning)) {
        ProgrammerRunning = false;
        RoboteqSettingsSent = false; // force update of Roboteq settings
        SensorsSet = false;
        AccelSet = false;
        BoostsSet = false;
        MotorCompSet = false;
        Pins1Set = false;
        Pins2Set = false;
        MotorsSet = false;
      }
      break;
    case CkJS_id:
      if (!CkJSDone) {
        byte DATA [2];
        DATA[0] = NotCalibrated;
        DATA[1] = ~DATA[0];
        for (byte i = 1; i <= 10; i++) {
          SEND_MESSAGE (2, CkJS_ret, DATA);
          delay (10);
        }
        CkJSDone;
      }
      break;
    case Calibration_id:
      Calibration ();
      break;
    case UsrSettings_id:
      byte DATA [2];
      DATA[0] = 1;
      DATA[1] = ~DATA[0];
      if (frame_data[0] == 1) {
        // StartGetUsrSettings is a state variable so that GET_UsrSettings
        // gets called only once although UsrSettings_id is sent repeatedly to
        // make sure the message isn't dropped
        if (!StartGetUsrSettings) {
          StartGetUsrSettings = true;
          SEND_MESSAGE (2, ProgrammerMsg_ret, DATA);
          delay (100);
          GET_UsrSettings ();
        }
      }
      break;
    case Usr_id:
      RoboteqSettingsSent = false;
      if (frame_data[0] == sizeof (UserSettings)) {
        EEPROM.update (NewMCUFlag, frame_data[1]);
        delay (10);
        UsrDownloadDone = true;
      }
      else if (frame_data[0] <= sizeof (UserSettings) - 1) { // added if clause to protect from bad frame_data[0]
        UserSettings[frame_data[0]] = frame_data[1];
        EEPROM.update(UsrAddress + frame_data[0], frame_data[1]);
      }
      // first User Setting byte triggers start of update and is sent
      // multiple times from Programmer. FirstUsrReceived is state
      // variable used to ensure that only first instance triggers rest
      // of upload which blocks loop () by repeatedly calling RECEIVE_MESSAGE
      // until UsrDownloadDone when last byte is received.
      if (!FirstUsrReceived && (frame_data[0] == 0)) {
        FirstUsrReceived = true;
        BLOCK_For_Download ();
      }
      break;
    case VoltCalib_id:
      if (frame_data[0] == 1) {
        GET_VoltCalibration ();
        VoltCalibrationSet = false;
      }
      else if (frame_data[0] == 2) {
        if (!VoltCalibrationSet) {
          SET_VoltCalibration ();
          VoltCalibrationSet = true;
        }
      }
      else if (frame_data[0] == 3) {
//        if (!OffsetCalibrationSet) {
          SET_OffsetCalibration ();
//          OffsetCalibrationSet = true;
//        }
      }
      break;
  } // end switch (ReceiveFrame_id)
} // end CK_INCOMING

void printBuf(byte rx_status, byte length, uint32_t ReceiveFrame_id, byte filter, byte buffer, byte *frame_data, byte ext) {
  Serial.print(F("[Rx] Status:"));
  Serial.print(rx_status, HEX);
  Serial.print(F(" Len:"));
  Serial.print(length, HEX);
  Serial.print(F(" Frame:0b"));
  Serial.print(ReceiveFrame_id, BIN);
  Serial.print(F(" EXT?:"));
  Serial.print(ext == 1, HEX);
  Serial.print(F(" Filter:"));
  Serial.print(filter, HEX);
  Serial.print(F(" Buffer:"));
  Serial.print(buffer, HEX);
  Serial.print(F(" Data:[0x"));
  for (byte i = 0; i < 8; i++) {
    if (i > 0) {
      Serial.print(F(" 0x"));
    }
    Serial.print(frame_data[i], HEX);
  }
  Serial.println(F("]"));
} // end printBuf






