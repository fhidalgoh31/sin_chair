#ifndef __Defines__
#define __Defines__

// *****************************************************************
// ROBOTEQ CAN Heartbeat must be set to 0 or it may keep MCP2551
// from sleeping
// *****************************************************************

#define OpenClosedDifferential 3
#define MaxTries 200 //number of tries until handshake failure
#define OnOffPin 2
// pulled down by push button on On/Off board, high by internal pullup
#define XcvrRxDPin 3 // INT of 2151 CAN controller, or mechanical switch triggers to re-awaken sleeping Arduino
// NOT USED IN MASTER NODE, but used in non-master nodes
#define FwdPin 4
#define RevPin 5
#define LftPin 6
#define RtPin 7
#define ModePin 8
#define UsrTogglePin 9
// pins 10, 11, 12, 13 used for SPI: 10 is CAN board CS
// pin A0 is unused (so far)
#define PowerOnPin A1 // INPUT_PULLUP to hold OnOff board DC-DC converter ON
                      // OUTPUT LOW to turn DC-DC converter Off
// pin A2 used for MCP2151 reset by CAN.h
#define XcvrSleepPin A3 // OUTPUT HIGH to put 2551 transceiver in sleep, INPUT LOW to awaken
                        // INPUT for CAN reception active
#define StopPin A4 // used as digital input
#define SpeedPotPin A5
#define SteeringPin A6
#define ThrottlePin A7
#define pressed LOW
#define not_pressed HIGH
#define OFF 1
#define ON 0
#define CLOSED 1
#define OPEN 0
#define YES 1
#define Yes 1
#define yes 1
#define NO 0
#define No 0
#define no 0
#define CyclesPerSpeedPotRead 20
#define CyclesPerDisplayUpdate 250
#define SpeedPotFilter 20
#define NumberOfModes 5
// modes as cycled by ModeSwitch
#define Drive1 1
#define Drive2 2
#define Drive3 3
#define Seat 4
#define Lights 5
#define Driver HIGH
#define Attendant LOW
// seat movement
#define none 0
#define forward 1
#define back 2
#define down 3
#define up 4
// lights
#define HeadlightBit 0
#define FourWayBit 1
#define LeftTurnBit 2
#define RightTurnBit 3
// brakes
#define Set true
#define Release false
// sleep controls
#define Sleep 0
#define Wake 1
#define Shutoff 2
#define TotalTimeAddress 60 // starting EEPROM address for storing total elapsed time, 
  // Roboteq returns int seconds, so 4 bytes
#define CalibrationAddress 70 // starting EEPROM address for storing 
    // Throttle0, ThrottleFwd, ThrottleRev, Steering0, SteeringLft, SteeringRt
    // SpeedPotMin, SpeedPotMax - 8 values in 16 bytes
#define UsrAddress 100 // starting EEPROM address for storing user settings
    // without settings still in Roboteq script, this comes to 46 bytes of a 
    // 70 byte array.  
#define NewMCUFlag 200 // EEPROM address that will be written to 0b1010101 after initial UserSettings
    // have been sent by Programmer node
#define VoltCalibrationAddress 300 // EEPROM address in which Programmer
               // stores 2 byte long VoltCalibration weighting factor
               // and 2 byte long VoltageDividerOffset between contactor in and out
// steps for remote joystick and speedpot calibration over CAN
#define throttleN 1
#define foreward 2
#define reverse 3
#define steeringN 4
#define left 5
#define right 6
#define speedpotmin 7
#define speedpotmax 8

// MCP2151 read buffer status macros
#define MSG_IN_BUFFER0 (rx_status & 0x40)
#define MSG_IN_BUFFER1 (rx_status & 0x80)

#endif




















