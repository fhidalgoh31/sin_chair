/*
NOTE:
 All confirmation message handling changed to event driven on 2014_05_25.   Many procedures have
 been changed or eliminated and some added, so change notes earlier than 2014_05_25 may be very
 confusing, though they do show the logic of how the program developed.

VERSION WITH OnOffPin (= 2 = int0) GOES LOW FOR SHUT OFF AND
 TO WAKE MASTER FROM SLEEP
 
2013_05_19 fixed contactor OFF/ON so that it turns back on as needed
           rearranged TurnOffProcedure to call separate RoboteqNeutral
           added code in setup to check that the contactor works and 
             doesn't have fused contacts, and to check that the brake 
             coils are neither open nor shorted.  
 2013_05_25 Roboteq_PWR added to turn off Roboteq control power
 2013_05_28 "guts" moved from Setup and Loop into seperate procedures 
              to improve readability
 2013_05_29 added RoboteqPresent boolean to allow running program when 
              Roboteq not present e.g. for setting deciAmp values for 
              testing 
            Add CHECK_BRAKE_COILS when releasing brakes error gives 
              setup error or (Throttle and Steering = 0) requires
              Master re-start to clear
            Added Slow1, Slow2 user settings (percentage reductions of
              Throttle and Steering)
            Added SetSlow (byte) = 0, 1, 2, 3 (inhibit, Slow1, Slow2, 
              full speed)
            Modified SEND_JOYSTICK_MESSAGE to adjust Throttle and Steering
              in accord with SetSlow
            Modified nonMASTER to sleep/wake both Roboteq and Aux nodes
            Changed Roboteq_PWR to communicate with AuxNode
            Modified CHECK_CONFIRMATION to recognize confirmation messages
              from any SEND_MESSAGE target
 2013_05_30 Separated RoboteqEmulator into two programs:
              RoboteqEmulator - for functions that will be handled
                directly by Roboteq
              AuxNode - for functions that will be handled by Nano 
                node e.g. Roboteq_PWR controller-power relay, speed inhibits,
                shock/stability sensing, probably lighting, possibly seat 
                actuator outputs, possibly current sensing for individual
                brakes, actuators
 2013_05_31 Added:
     do { // make sure CAN.setMode(SLEEP) isn't blocked by messages in buffer
       RECEIVE_MESSAGE ();
     } while (MsgReceived);
     in sleep, shutdown and XCVR routines to dump messages left in buffers
 2013_06_06 Added: CHECK_SETSLOW to query AuxNode for SetSlow, case SetSlow
              to modify joystick values and to send messages to display
            CK_SETSLOW in AuxNode with cycling 0, 1, 2, 3 every 9 seconds
              for testing
 2013_06_17 Maximum interval between sending repeated unchanged joystick 
              messages changed to Heartbeat = 90% of RoboteqWatchdog rather
              than fixed 900 msec.
            Added SetRoboteqWatchdog () to set Roboteq Watchdog to 
              RoboteqWatchdog in UserSettings.h via _RWD message
 2013_06_18 CHECK_CONFIRMATION changed to repeatedly RECEIVE_MESSAGE 
              up to 50 times to remove irrelevant messages still in
              buffer and to allow time for sending node to send the 
              confirmation message.  50 RECEIVE_MESSAGE calls take 1.31 msec,
              and, in general 7-11 repeats suffice to clear buffers and wait
              for message arrival.
            delay () in repeat calls to CHECK_CONFIRMATION therefore removed
              as no longer needed and calls to CHECK_CONFIRMATION succeed
              the first time through.  Hence, repeat of "SEND_xxx_MESSAGE 
              and CHECK_CONFIRMATION" set to a maximum of two.  Real errors
              will therefore be trapped within 2.6 msec.
            PROCESS_SEAT_MESSAGE () added with included CHECK_CONFIRMATION
              and seat routines changed to call this rather than directly
              calling SEND_SEAT_MESSAGE. 
             (Later removed see 2013_06_20)
 2013_06_20 All GetValue messages changed to use same repeated RECEIVE_MESSAGE
              scheme as used for CHECK_CONFIRMATION, and the code for this
              removed from individual procedures to a GET_VALUE (Name, Target)
              procedure that returns a boolean.
            GET_VALUE (Name, Target) includes both the repeated RECEIVE_MESSAGE
              loop and an outer loop to repeat the SEND_MESSAGE and up to 50
              RECEIVE_MESSAGE calls. 
            Did the same for procedures that send SetCommand messages.  This
              not only removes CHECK_CONFIRMATION from the main body of the
              program, but speeds execution as the data to be sent is not
              recalculated even if the message has to be re-sent.  This also
              allowed me to eliminate PROCESS_SEAT_MESSAGE 
            The above changes on 2013_06_18 and 2013_06_20 have freed ca. 2k
              of flash memory, surely removed a fair amount of stack swapping,
              and probably speeds loop time as lots of delay() have been removed
              and confirmation takes only the time needed for the sending node
              to send its message (though that remains to be checked).
            Operation of 3-node network checked at 10Kbps -- no problems noted,
              and seemed just as responsive as at 1Mbps.  CANbus time seems not
             to be a limiting factor.
 2013_06_21 Checking for slow or inhibit (i.e. SetSlow) changed from a query in
              each loop cycle to monitoring at each RECEIVE_MESSAGE for a
              fromAux _SLOW message, applying it, and sending a _SLOW message back
              toAux.  If AuxNode doesn't receive the toAux _SLOW, it again sends
              the same _SLOW on its next loop.  When the Aux node does receive the
              toAux _SLOW, it sets lastSetSlow to the current SetSlow and in that
              way knows to not send it again.
 2013_06_26 Added handling of errors as follows:
              non-critical - just display a message, 
              mildly critical - Roboteq needn't shut off, but OON set until jostick
                & switches neutral; i.e. sets OONflag = true;
              strongly critical - Roboteq stops politely and no restart needed;
                i.e. calls RoboteqNeutral ()
              absolutely critical - Roboteq shut off and restart is obligatory;
                i.e calls RoboteqNeutral () and Roboteq_PWR (0); 
                // see 2014_07_17 for change
 2013_06_29  Sketch divided into several TABS:
               Roboteq constants (with additions) in Constants.h
               USER SETTINGS in UserSettings.h
               General use #defines in Defines.h
               General use GLOBAL VARIABLES in Globals.h
               All procedures except Setup() and Loop() in:
                 GeneralProcedures
                 JoystickProcedures
                 SwitchProcedures
 2013_07_13 Fixed cast bug in finding time elapsed to enter sleep
            Roboteq _FLTFLAG and _STFLAG messages added to Roboteq Emulator and
              handled in Master in the same way as SetSlow messages from Aux node.
              (All three now in CK_INCOMING () procedure called from SEND_MESSAGE.)
           _FLTFLAG messages are critical faults and invoke RoboteqNeutral () 
              and Roboteq_PWR (0) // see 2014_07_17 for removal of RoboteqPWR
           _STFLAG messages are treated as non critical
 2013_07_18 Added monitoring of Amps during seat movements so that overload stops
              actuator.  This is a secondary protection for jammed machinery
              beyond physical limit switches used to stop movement at tilt and
              lift limits.
 2013_07_20 Added reading _TIME to calculate total elapsed time and store as
              3 bytes in EEPROM
 2013_07_22 Added checks for startup of Aux node and Roboteq
            Added DisplayNode enumerations to Constants.h
 2013_08_07 void EEPROM_RING () in GeneralProcedures tab changed to not assume
              that unused ring has 0 stored, but checks for whether the value
              is outside the address ranges in which either Mode or AHr are stored.
            CHECK_BRAKES now checks whether coil actually works by monitoring
              current: see CHECK_BRAKE_COILS.  Roboteq, however, could monitor
              voltage at high side of coil to detect open microswitch or coil.
              Master would GetValue _DIGIN to read whether high after brake release
              (open coil) or low (digital output MOSFET pulls coil to ground).
 2013_08_19 CHECK_CURRENT () changed to accumulate milliAHrs even when contactor
              is open
            Code added in CHECK_CURRENT to:
              IF deciAmps < 20 i.e. no motor loads, call FIND_QUIESCENTVOLTS and
                average 5 reads (>= 15 seconds), send average Volts to Display
                node, and if below LowVoltLimit reduce SetSlow to conserve low
                battery (i.e. a limp home state).
 2013_09_29 First complete implementation of DisplayNODE.
              SpeedSlow display messages not working
            check for OON conditions should be moved into
            separate procedures
 2013_09_30 SpeedSlow display messages had been commented out for testing;
              fixed
            Volts and AHr meters were not updating except when base bmp was
              repainted.  Added line to fill a Background rectangle before
              filling updated bars.
            Random offset added to QuiescentVolts so that volt meter goes up 
              and down.  Be sure to remove before real use.  Same for 
              simulation of AHr draw.
 2013_10_04 Display messaging removed from setup and periodic display update
              added to DO_MODE.  Presently updating every CyclesPerDisplayUpdate
              cycles through Loop, but may want to change to millis-based timing.
            Check for StartupState removed from DislpayNode loop so that display
              overpaints Startup wait message as soon as first update arrives.
            DisplayVolts changed from local to global variable so that Display
              update can use the value found during setup.
           At startup, seems that CAN could be saturated with non-ack before
             non-Master nodes come on line.  Added: delay (1000);
          // substantial delay needed here to let other nodes come on line
            in Setup.  Have not tested for shorter than 1000 msec delay. 1000 OK
            but see 2013_10_06 for change to 2000 msec.
         Display was sometimes re-painted after it was supposedly set to sleep.
           Added if ((!XcvrSleep)|| (Name = _XCVR)) SEND_MESSAGE to avoid
           cueing any messages except XCVR when transceivers are in sleep.  
         This solved the problem of screen re-paint while asleep, but Roboteq
           emulator still doesn't always go to sleep.
         With Dislplay messaging cleaned up, there is no longer need for delays
           after sending display messages and they have been removed.
         if (_S < 3) {
           SEND_DISPLAY_MESSAGE1 (_SETSLOW, SetSlow);
         added in DO_MODE so that stop, very slow and slow symbols get repainted
           if UsrToggle or Mode change.
 2013_10_06 Added Shutdown enumeration for non-Master XCVR messaging.  In the non-Master
              nodes, this pulls the PowerOnPin (currently A1) to HIGH.  Connected 
              base of transistor that pulls down pin Vr of the DC-DC converter
              for that node.  This gives a very positive power turn off.  
            With added PinMode calls in non-Master setups, the delay (1000) in Master
              setup becomes marginal - sometimes enough, sometimes not.  Increased to
              delay (2000).
 2013_10_08 Added:
              #define JSThrottleSwap 1 // 1 or -1 to swap forward and back
              #define JSSteeringSwap -1 // 1 or -1 to swap left and right
              #define JSMin 890 // millivolts
              #define JSMax 3950 // millivolts
           to UserSettings.h to allow easy change for different joysticks
           NOTE: The above values were empirically found for an old DX-ACU
             jostick in very bad shape: X mirror != X, Y mirror != Y, 
             mechanical center is not 0,0 and max and min are different for
             X and Y.  If this is true also for good joysticks, will have
             to add calibration settings.
           Found a casting error in READ_JOYSTICK ().  Corrected in this
             version and in 2013_10_06, but not in earlier versions.
 2013_10_09 to 2013_10_11 Modified to use joystick and speedpot calibration values
             stored in EEPROM using Joystick_Calibration sketch. Throttle
             and Steering sense inversion and throttle and steering Min and Max 
             are now globals set based on those calibration values.
 2013_10_12 Scaling and curving of joystick readings re-written to correctly
              offset by Throttle0 and Steering0, to set to 0 if within deadband,
              re-scale from deadband to +/-1000, then apply exponential curve.
            Added SEND_DISPLAY_MESSAGE1 (_OON, 0) to erase Out-of-neutral or 
              SwitchClosed messages.
 2013_10_15 Out of neutral checking when starting up and waking up moved to
              procedure CheckOON ().  The similar check in READ_UsrToggle, 
              however, has a very different structure and was left in READ_UsrToggle.
 2013_10_30 DisplaySpeedPot changed from local to global variable so display update in
              DO_MODE won't require a READ_SPEEDPOT call.
            Added SpeedPotMax to UserSettings, changed PotMin user setting to SpeedPotMin
              and speedpot calibration values to PotMin and PotMax.
            Added if (SpeedPotInstalled) around each call to READ_SPEEDPOT
 2013_11_02 Joystick, Seat and Lights display markers added for demo or 
              testing purposes.  COMMENT OUT BEFORE USE
            Stop switch (push on/push off) sends _SETSLOW 0 (STOP symbol)
              message to display node.
 2013_11_07 Serial.print messages added to test for CAN dropouts (e.g. tries >50 etc.)
              and discovered that READ_AMPS message was not being seen by RoboteqEmulator
              - that case had been left out of emulator's loop.  FIXED,
            Seat routines changed to read NoMoveAmps only one when seat not
              moving.  MoveAmps still read continuously while moving to test
              for overload.
 2013_11_09 Switch OutOfNeutral messages added to display which switch,
              or >1 switch, is out of neutral.
            NOTE: demo markers for driving, seat and lights do not re-paint if
              UsrToggle is flipped.  As these markers are for demo only, and will
              be absent in actual use, will probably not add re-paint code.
            Some timing tests: GET_VALUE with confirmation within procedure takes 
              about 200 microseconds.  GET_CONFIRMATION takes about 400 microseconds.
 2013_11_18 Removed unneccessary SpeedPot messaging at start of DO_MODE
 2014_03_04 Added delay at two places in NonMaster nodes _XCVR shutdown. 
              If no delay in NonMaster nodes, PowerOnPin does not stay high long
              enough to reliabl shut Recom DC-DC converter.
           For Aux and Display, minimum delay is 6 msec, set at 10. 
           For Roboteq Emulator, minimum is 11, set at 15.
 2014_03_06 Change single user setting for curving Throttle and Steering to 
              separate settings for the two axes.  See CURVE_JOYSTICK.
 2014_03_24 Completed Display node coding of real time clock and PWM control of screen
              backlight (P-channel high side MOSFET added to TFT board).  
            Fixed some bugs (in Master) that blocked showing OON messages.
 2014_03_31 OON and SwitchClosed display messages added to READ_ModeSwitch.
 2014_04_07 Added:
              #define PotTurnWeight 50 //0=no effect of SpeedPot on turn rates,
                100=full effect of SpeedPot on turn rates,
                // 50=half as much effect of SpeedPot on turn rates
               to allow tuning of how steering responds to SpeedPot value.
             Added 1 msec delay to checking StopSwitch in DO_DRIVE when in UsrToggle==Driver
               because loop time was so short in Driver that some switches need debouncing.
             Added check in GET_CALIBRATION for MCU that has not yet had calibration run and
               assigns drivable values that will give joystick or speed pot errors as one
               approaches end of travel (to indicate that calibration is needed).
 2014_04_14 Moved lights control from RoboteqEmulator to Aux and implemented
              actual control via STM VN540 high-side solid state switches
 2014_04_28 Wired inhibit/slow switches to analog pin in Aux and corrected code
              to analog read once and send many times until confirmation comes from Master.
            Added code to turn ADC back on after waking from sleep.  Master responds very
              quickly if Throttle or Sterring != 0, but there's significant lag if !Throttle 
              && !Steering.
 2014_05_03 Changed category of SetSlow messages from GetValue to SetCommand to give it 
              higher priority.  Had very little effect if any on SetSlow lag.
            Added:
              RECEIVE_MESSAGE (); // make sure things like SetSlow messages get read 
              before being stuck in confirmation while loops.
            at top of RUN_CHAIR ().
            Eliminates receive buffer overload and all delays.
 2014_05_05 Started working on changing all message handshaking to event driven.
              Instead of calculating message IDs from Category, Name and Target for each
              transmission, most are now calculated in the compiler in #define
              statements (see MsgIDs.h).  The few exceptions are those functions that handle
              multiple different Names such as SEND_DISPLAY_MESSAGE.  SEND_MESSAGE has been
              changed to take ID as a parameter rather than the separate sub-fields.
 2014_05_10 CHECK_CURRENT and newCONTACTOR using event-based message handling.
 2014_05_25 all confirmation message handling changed to event driven (as had been done
              for SpeedSlow and Roboteq status and fault flag handling) so that I could get
              rid of while loops that have the potential of overloading receive buffers.  
              In startup, shutdown and sleep (as opposed to continuously looping) sections, these
              handshakes are in their own loops of the form:
                for (byte i = 1 ; i<=11; i++){ // 11 calls results in a bus error message to the display
                 // In general, confirmation is received within 1 or 2 cycles.
                 READ_RoboteqTIME (); // or CHECK_AMPS, BRAKES, CONTACTOR etc.
                 delay (1); // delay may not be needed, but does no harm during startup, shutdown 
                       and sleep/wake
                 RECEIVE_MESSAGE ();
                 if (GetLongDone){
                   RoboteqStartTime = RoboteqTime;
                   break;
                 }
               }
             In all other cases, the needed SEND_MESSAGE repetition is achieved by calling 
               procedures such as READ_RoboteqTIME once per loop () cycle.  In situations such
               as SEND_JOYSTICK_MESSAGE, where a sequence of actions are needed when starting
               chair movement, if constucts make sure that the first action is completed before
               later calls promote the second action, the first two are completed before the third
               initiates and so on.
             Many procedures have been changed or eliminated and some added, so change notes
               earlier than this one may be very confusing, though they do show the logic of
               how the program developed.  All message handling, except for Display Node 
               and Throttle and Steering messages, includes confirmation.  Display Node is so
               non critical that using bandwidth for confirmation is unreasonagle.  If Throttle
               and Steering values change, they are sent once per loop() cycle and they can change 
               between cycles so a confirmation return might not match anyway. If Throttle and 
               Steering are consistently not received, the Roboteq watchdog will not be satisfied, 
               so the motors will be stopped in any case.  The one exception to this is during
               startup where a loop similar to that above makes sure Throttle and Steering are 
               initially set to 0.
             Outgoing data are sent with SEND_BYTE or SEND_INT, requests for data are sent with 
               GET_INT or GET_LONG, and the return events are detected in a switch structure in 
               CK_INCOMING.
 2014_06_02 Added Calibration tab to allow joystick and speed pot re-calibration over CAN
              by new Programmer node.
 2014_07_17 Roboteq MCU relay now powered directly from Aux  DC-DC converter
              5V rather than digital pin so that Roboteq turns on simultaneously
              with Aux Node rather than waiting until Aux Node is rfully awake.
 2014_09_17 Added:
              SEND_MESSAGE (0, SetSlow_confirm, NULL); // confirm any pending 
                                                      setslow before sleeping
            in Aux_SLEEPWAKE to make sure Aux node does not sit waiting for
              a SetSlow confirmation
          //zz added to all Serial. lines because having an excess of Serial.print
             statements sometimes caused a CAN hang.  Delete "//zz " to restore
             those lines for diagnostic use.  Other Serial.print statements have 
             other // tags as well.  Need to sort through all Serial.print
             to see which can simply be deleted because no longer needed.
 2014_09_21 Added:
              lights off in GO_TO_SLEEP to let Aux sleep
              dummy messages in WAKE_UP to ensure that NonMaster nodes get CAN
                messages when waking up
 2014_10_14 RE-WRITTEN TO WORK WITH ROBOTEQ instead of emulator
            added check for power supply <19 volts in CALC_QUIESCENT_VOLTS
              so that it doesn't trigger low voltage SetSlow = SetSlow - 1
            Brakes and contactor now handled in Roboteq rather than Master
            boolean StartupError was not set anywhere and has been removed.
 2014_10_16 RoboteqWatchdog is now in Roborun configuration and retrieved from
              the controller with GET_INT rather than in user settings here and
              sent with SET_INT.
 2014_10_18 All repetitive blocking CAN functions removed from setup () and put in
              loop where they re-sent once per loop cycle until response received.
 2014_10_19 _DIGOUT used in TURN_OFF to keep system from turning off
              until motors, actuators and brakes are off and contactor is open.
            delay (2000) added near end of setup so that Roboteq has time
              to autostart script even if set to default 2 sec delay to autostart.
 2014_10_28 All GetIntDone changed to GetLongDone in places where CHECK_VOLTS 
              is called
            finding StartupVolts removed from EEPROM_RING () in startup and 
              put in if (deciVolts == 0){ section of Loop so that it is non-blocking
            GET_BYTE (DigOut_id, _CONMSG); & RoboteqContactorBit used to decide
              when to CHECK_VOLTS because _BATTAMPS detects only motor board current
            all seat actuator overload detection removed as it would only check
              relay coils AND Roboteq _BATTAMPS does not read current on DIGOUT pins anyway
            Robotek _VOLTS reads voltage downstream of the contactor so it reads
              B+ only when the contactor is closed, and B+ minus drop across pre-charge
              resistor when the contactor is open.  To read B+ and to detect a failed
              contactor, voltage reading functionality has been moved to Aux.
            Voltage dividers were added to contactor input and output pins on
              power distribution board and Aux programmed to analog read those
              pins.  Master calls Aux in GET_VOLTS both for finding quiescent
              volts and in CK_CONTACTOR where the difference between in and out
              is used to detect whether the contactor is open when it shouldn't be
              (i.e. Roboteq digout closes contactor, but coil has failed or Roboteq)
              opens contactor but it remains closed (fused contacts).
            Voltage dividers were also added to the brake connecton to Roboteq
              digout and monitored by Aux to detect open brake (i.e. coils activated
              = 0 Volts, if coil or wiring is open it will be B+ minus voltage drop
              across brake coil).
 2014_11_15 Reduced turn rate when reverse throttle set to kick in at < -150 instead of
              < 0 (see READ_JOYSTICK ())
 2014_11_19 elapsed for sending unchanged joystick changed to 100 msec from Heartbeat
              because Roboteq script sets motor values only when message received
              and this allows for quicker recovery from erroneous motor output states.
            Because Heartbeat is no longer used to calculate elapsed time, Watchdog
              is no longer needed so GetRoboteqWatchdog procedure and call to it as
              well as RoboteqWatchdog and Heartbeat globals have been deleted.
            CK_CONTACTOR routine is inefficient because it first calls CHECK_VOLTS ()
              and only then checks Roboteq digout pins.  Would be better to do the
              reverse, first check digout pins and then call CHECK_VOLTS only if needed,
              but my attempt to do this was flawed and I got tired of spending time
              trying to fix it.  
            Brake coil checking is handled better with two procedures CALL_CK_BRAKES () 
              that retrieves the Roboteq brake pin values, then calls CK_BRAKES to check 
              voltage and signal faults.
 2014_11_30 Message handshaking and handling in Roboteq script improved so that elapsed
              has been changed back to Heartbeat = 90% of Roboteq watchdog time.
 2014_12_17 Routines added in ProgrammerNode and Master to update user settings and 
              to verify match between user settings in Programmer and in Master
 2014_12_26 Programmer and Master coding changed so that Master's loop() is blocked
              while Programmer does its thing.  Calibration and User Settings updating
              then work without any dropouts or errors.
            However, connecting Programmer node caused failure of READ_RoboteqTIME 
              blocking bus.  Several things were tried in programming to eliminate this,
              among them:
           (1)   if (ID == Time_id) {
                   delay (10);
                   RECEIVE_MESSAGE ()
                   }
               added in GET_LONG, but NO HELP WHATSOEVER.
           (2) delays added in Programmer tab Next_step () and increased delays in 
             GET_UsrSettings Improved reliability of this messaging but didn't help 
             with READ_RoboteqTime.
           (3) Master's receive masks changed to allow only toMaster: doesn't help 
             with Programmer killing Master
 WHAT DOES COMPLETELY FIX THE PROBLEM IS ADDING DC-DC CONVERTER BOARD
 TO PROGRAMMER NODE!  Evidently, 4.5 V from USB leads to timing
 or other problem in CAN messaging.  Given that it's only a Roboteq
 message that fails, I suspect that the lower voltage allows the TI
 transceiver in the Roboteq to pull the bus too low.
 
2015_01_01   added:
               while ((!DisplayLive) || (!AuxLive) || (!RoboteqLive)){
                 RECEIVE_MESSAGE ();
               }
             and corresponding messaging in Aux, Display and Roboteq to hold Master
               in Setup until other nodes are live rather than using arbitrary delay.
             Handling of SEND_DISPLAY_MESSAGE0 (_STARTUP); modified to ensure
               Display gets this message: moved into Loop () after RoboteqStartTime
               has been set, then sent multiple times, with one-shot receive in Display.
2015_01_12 Added CalibrateVoltage () in Programmer node and corresponding functons in 
             Master to adjust calibration of voltage measured by voltage divider in Aux
             node.  See:
               case VoltCalib_id: // GeneralProcedures tab CK_INCOMING
               GET_VoltCalibration (void) // Programmer tab
               and SET_VoltCalibration (void) // "
2015_01_19 Rearranged calling order in Setup and added:
             SEND_MESSAGE (0, Aux_Dummy, NULL);
             SEND_MESSAGE (0, Display_Dummy, NULL);
           in  Setup's 
             while ((!DisplayLive) || (!AuxLive) || (!RoboteqLive)){
           and:
               SEND_MESSAGE (0,UsrSettings_ret,NULL);
           at end of Setup.
           to allow Programmer to resume if Master had not been turned on
             before programmer messaging was started
2015_02_03 code added to set Roboteq acceleration and motor compensation
             parameters from here and to adjust them using Programmer node
           backup TotalTime routine in Setup changed to swap TotalTime with
             backup value if backup value >= 8 hours earlier, and to swap 
             backup with TotalTime if TotalTime < backup TotalTime
2015_02_14 Re-did UpdateTotalTime from scratch with 
               if (RoboteqTime < LastRoboteqTime+43200)
             in void BlockReadRoboteqTime (void) to avoid recording
             bad data in arrival from Roboteq.  So far, seems that values\
             stored in EEPROM are OK.
           Constants.h, labels.h (Programmer node) and MsgIDs.h moved to WC_CAN library
           Diplay now #includes new TFT library from Arduino 1.6.0 instead of separate
             Adafruit libraries
           Syntax for data stored in PROGMEM (in Display and WC_CAN) changed to const as
             that is compatible with both Arduino 1.0.x and 1.6.0
           DO NOT UPLOAD WITH Arduino 1.6.0 -- TURN_OFF DOES NOT WORK
             ??? does not turn off NonMaster nodes, but does turn off Master ????
2015_02_27 SendRoboteqSettings () added in GeneralProcedures.  Called in Setup
             and anythime Programmer has changed UserSettings so that
             Roboteq gets updated without having to turn off and re-start chair.
2015_03_06 PotTurnWeight was in user settings but code to use it wasn't there.  PotRevTurnWeight
             wasn't even in user settings array.  Both of these have been corrected and coded in
             READ_JOYSTICK.  For now, there are lots of Serial.print statements in READ_JOYSTICK
             and where SEND_JOYSTICK is called in GeneralProcedures that should be take out
             once sure that things are working OK.
2015_03_09 User settings for joystick and speedpot changed to not use precentage weights, but to
            directly set:
                 SpeedPotFwdMin; // UserSettings[14] percent forward speed at lowest pot setting
                 SpeedPotFwdMax; // UserSettings[15] percent forward speed at highest pot setting
                 SpeedPotRevMin; // UserSettings[0] percent reverse speed at lowest pot setting
                 SpeedPotRevMax; // UserSettings[1] percent reverse speed at highest pot setting
                 TurnPotFwdMin; // UserSettings[2] percent forward turn at lowest pot setting
                 TurnPotFwdMax; // UserSettings[12] percent forward turn at highest pot setting
                 TurnPotRevMin; // UserSettings[3] percent turn in reverse at lowest pot setting
                 TurnPotRevMax; // UserSettings[13] percent turn in reverse at highest pot setting
            and added byte TurnPot for default and returned value of pot for turning
          Added Throttle = 0; and Steering = 0; at start of READ_JOYSTICK to make sure old value can't 
            be unintentionally repeated
2015_03_11 Robotq Watchdog timer is satisfied by any commands sent from script, hence is never
             invoked if CAN messaging is lost.  Added CANwatchdog timer, set to 100 msec, to 
             Roboteq script and in GeneralProcedures changed Elapsed for SEND_JOYSTICK_MESSAGE 
             from Heartbeat to 50 ms.  This ensures that any failure of Master will cause chair to
             stop moving rather than continuing at last received Throttle and Steering.
           Also added code in Roboteq script to avoid re-sending motor commands once motor power has
             reached commanded value.  Intent is to reduce time in (priority, but unneccessary) motor
             control to let script receive messages with less delay.
2015_04_06 Code added to inhibit driving while (deciVolts>(RechargeVolts+5) (i.e. with charger plugged in)
           Added LimpSpeed user setting for Roboteq so that a motor disconnect first causes 
             Throttle=Steering=0 until stick is centered and then allows LimpSpeed so that some power
             is available to help push the chair home.  (thanks to Mike Stewart for suggesting this.)
2015_04_14 Searched for and found several places where I had out-of-range array indices.  Having fixed
             these it runs properly when compiled with Arduino 1.6.2.  The changes are noted in:
                   case user_id:
                   SEND_TIME
                   SEND_MESSAGE
            However, this exposed a bug in nonMaster, Aux_SLEEPWAKE and Display_SLEEPWAKE where state was
              passed as boolean but can have 3 values: Sleep 0, Wake 1 or Shutoff 2.  Apparently with
              out-of-range pointer elsewhere these values were being passed despite the boolean spec.
              Changed to byte.

2015_05_09 in READ_UsrToggle, changed order to only do something if 
             UsrTogle != LastUsrToggle
           major simplification of CheckOON
           Added sending DisplayAHr and DisplayVolts messages to Display 
             after StartupMessageSent
           CHECK_CHARGING called each time through loop after system start 
             until charger is disconnected, then not called again until next
             system re-start
           Fixed bug in SEND_MESSAGE: was using if (Name==_XCVR), but now that
             message IDs are #defined, Name is only assigned in SendDisplayMessage.
             Must use if (ID==xxxSleepWake) to ensure that go to sleep messages get
             sent.
2015_05_10 EEPROM.write changed to EEPROM.update so that only changed user settings are
             written to EEPROM.  (New function in 1.6.2 EEPROM library.)
           When waking from sleep Charging = true; so that CHECK_CHARGING called to assure
             that not driving with charger connected during sleep (as well as at system start).
           Call to GetRoboteqWatchdog () commented out as RoboteqWatchdog is not being used.
             Procedure left in in case later needed.
2015_05_15 In if (!StartupMsgSent) { section of loop(), SEND_TIME (_TOTTIME, TotalHours, TotalMinutes); and
             SEND_DISPLAY_MESSAGE0 (_STARTUP); have to be sent more times (or perhaps with longer delays)
             when in Seat (and presumably Lights) mode than when in Driving modes or they are not seen by
             Display.
           CHECK_CHARGING fixed to CHECK_VOLTS each time called.  Otherwise, once charging detecte remains
             in an infinite loop.  In GO_TO_SLEEP, moved Charging = true from after interrupt to before MCU_SLEEP.
2015_05_19 AccelComp and DecelComp Roboteq parameters removed.
           Added minimal forward or backward throttle to help start turn in place.  Don't yet know if it helps.
            if ((Steering <= -80) || (Steering >= 80)){
              if ((Throttle >= 0) && (Throttle < 40)){
                Throttle = 40;
              }
              else if ((Throttle < 0) && (Throttle > -40)){
                Throttle = -40;
              }
            }
          10 repeats at 10 msec intervals are adequate for SEND_TIME (_TOTTIME, TotalHours, TotalMinutes); and
             SEND_DISPLAY_MESSAGE0 (_STARTUP); in Seat mode, so changed from 100 repeats at 5 msec.
2015_05_23 Added SEND_DISPLAY_MESSAGE1 (_OON, 1); in CheckOON so that Usr/Attendant toggle changes are displayed before
             posting _OON display message.  Gives visual indication of inadvertant changels of toggle rather than just 
             error text.
           Added   static int HighVolts; and else if ((deciVolts < RechargeVolts+12) || (deciVolts < HighVolts)) { to
             CHECK_CHARGING so that block is released as soon as voltage falls rather than having to wait till below float
             voltage.
           Added SEND_DISPLAY_MESSAGE1 (_MODE, UsrToggle<<3|Mode);
                 delay (10);
             near end of if (!StartupMsgSent) segment of loop () to force basic screen to paint when first started.  delay (10) is
             needed in Seat mode, but not in Drive mode.
2015_05_24 With delay (10) for sending SEND_TIME (_TOTTIME, TotalHours, TotalMinutes); and SEND_DISPLAY_MESSAGE0 (_STARTUP);
             during startup section at top of loop () there's no longer any need for repeated messages.
2015_06_03 pinMode (PowerOnPin, INPUT_PULLUP); is enough to hold ON-OFF board in on state -- (see setup() and TURN_OFF() )
           20-cell ThrottleStack[] & SteeringStack[] used for running average of Throttle and Steering; number of cells to average
             is set by Damping user setting.  Reduces effect of bouncing of joystick, especially while attendant driving with
             relative movement of hand and chair.  See DO_DRIVE ():
               if (UsrToggle == Attendant){
                 READ_JOYSTICK ();
            // if Damping > 1 use stacks to find running average
                 if (Damping > 1){
             // ThrottleStack and SteeringStack each hold up to 20 integers
             // first shift cells 0 to (Damping-2) into cells 1 to (Damping-1)
                   for (int i=(Damping-2); i>=0; i--){
                     ThrottleStack[i+1]=ThrottleStack[i];
                     SteeringStack[i+1]=SteeringStack[i];
                   }
                   int SumThrottle = 0;
                   int SumSteering = 0;
             // average cells from 1 to (NtoAvg-1) of stacks + new joystick readings, 
                   for (byte i=1; i<=(Damping-1); i++){
                     SumThrottle = SumThrottle + ThrottleStack[i];
                     SumSteering = SumSteering + SteeringStack[i];
                   }
                   SumThrottle = SumThrottle + Throttle;
                   SumSteering = SumSteering + Steering;
                   Throttle = SumThrottle/Damping;
                   Steering = SumSteering/Damping;
             // put the new averages in stack cells [0]
                   ThrottleStack[0] = Throttle;
                   SteeringStack[0] = Steering;
                }
              }
           Have experienced occasional blocked slow/very slow/stop state.  Need to check wiring on 
             power distribution main board and perhaps change voltage divider resistors R2 to R5 to
             smaller than 10k (2k2?).  Until then, I have temporarily defeated effect of SetSlow messages
             fro Aux to find out if this is why chair is halting.  see CK_INCOMING
                 case SetSlow_id:
                   SetSlow = 3; // always sets to full speed no matter value arriving from Aux
2015_06_08 moving average changed to use circular index rather than shifted array
         // put new values in arrays using circular index
             static byte i = 0;
             ThrottleStack[i]=Throttle;
             SteeringStack[i]=Steering;
             i = i + 1;
             if (i>=Damping-1){
               i = 0;
             }
       // average cells
             int SumThrottle = 0;
             int SumSteering = 0;
             for (byte i=0; i<=(Damping-1); i++){
               SumThrottle = SumThrottle + ThrottleStack[i];
               SumSteering = SumSteering + SteeringStack[i];
             }
             Throttle = SumThrottle/Damping;
             Steering = SumSteering/Damping;

           TURN_OFF now uses Sleep with noInterrupts to make sure MCU can't be doing anything
             while voltage is dropping during RC delay instead of more cumbersome previous code.
           Realized that adding Throttle for turn in place actually reduces total power, removed.
2015_07_27 added global StartTime.  Used to prevent TURN_OFF immediately after startup or after awakening
             to give time for OnOffPin to go high after pressing on/off pushbutton.  See Setup lines
             158-162, and loop lines 234 and 266.
             ON SECOND THOUGHT: StartTime lines commented out (later DELETED), but wait for OnOffPin to go HIGH
               added at end of setup (there's already a similar while already for
               waking form sleep).  See setup 159-164

2015_09_10 after re-wiring new Roboteq to completely isolate Roboteq MCU ground
             lead and to have all cable shields tied together and to chair
             frame, two problems were noted:
             (1) Master "thought" that Mode was sometimes pressed even when it
               wasn't.  Adding 10k pullup didn't help, but putting 10 uF ceramic
               cap between shield and gnd eliminated this problem.  Doing this,
               there is no low impedance connection of frame or shield to ground,
               but noise goes to ground.
             (2) When going from high joystick output to full low, the voltage 
               momentarily swings 150 mA or sometimes more below the calibrated
               minimum value. Therefore changed test for joystick fault from:
    if ((*RawThrottle < (ThrottleMinCal-50)) || (*RawThrottle > (ThrottleMaxCal+50))) {
                                 and
    if ((*RawSteering < (SteeringMinCal-50)) || (*RawSteering > (SteeringMaxCal+50))) {
            to:
    if ((*RawThrottle<(ThrottleMinCal/2)) || (*RawThrottle>(ThrottleMaxCal+(5000-ThrottleMaxCal)/2))) {
                                 and
    if ((*RawSteering<(SteeringMinCal/2)) || (*RawSteering>(SteeringMaxCal+(5000-SteeringMaxCal)/2))) {
               This seems to have resolved the problem.
2015_12_30 UserSettings changed to match new Roboteq script
           New UserSettings[] array has 1-byte values through UserSettings[43]
              and 2-byte values thereafter
           Added CK_UserEEPROM () to check for corrupted UserSettings stored in EEPROM
              and revert to using default values.  Sends EEPROMfault message to Display
              which logs it once and displays it repeatedly.
           Added CheckBackupTime () that cross checks that TotalTime in EEPROM is no more
              than 50 hours more than backup copy stored in EEPROM at last TURN_OFF.
              If so, backup numbers are used to EEPROM.update TotalTime.  
              Done at startup and within Update_Total_Time () before adding interval.
2016_01_30 Hysteresis added in CK_CONTACTOR by finding Contactor open or closed seperately
              if ContactorState > 0 or ContactorState == 0.
           If BackupTime (in EEPROM) > TotalTime (in EEPROM), the backup values are
              corrupted.  Re-set to TotalTime values
           In GET_CALIBRATION, changed criterion for using default values to 
              if <=0 and >=5000 to deal with how burning bootloader resets EEPROM
2016_02_09 Added procedure to send SpeedPot and TurnPot values to Roboteq.
           Fixed error in calculation of TurnPot
2016_02_28 Moved VoltageDividerOffset from Master to Aux
2016_03_18 added more space to UserSettings arrays, added additional user
           settings in Programmer, added SET_RoboteqCompDeadband (); procedure
2016_03_22 Changed VoltageDividerOffset from #define to EEPROM-stored global variable and
             added calibration of this to Programmer node.
           Added some Roboteq user settings to Programmer and expanded settings array(s)
             in Master and Programmer to include a number of unused cells for future 
             additions.  Among the additions are definitions of Roboteq pins and possiblity
             to reverse polarity of current sensors or motors or to swap motors. See SET_Roboteqxxxx
             procedures.
           Made Throttle and Steering curving apply only when driving, not for seat or lights
2016_04_19 Modified so that inhibits reduce only Throttle
           SlowAccel () added to reduce Accel and Decel if SetSlow = 1 or 2
           UserSettings parameters changed to match latest Roboteq script
2016_05_01 AutoStep () procedure and DoAutoStep global added to send programmed sequence
             of Throttle and Steering values to Roboteq. See:
           // ***********************************************************************
           // uncomment next line to send a sequence of motions defined in procedure AutoStep()
           //   DoAutoStep = true;
           // ***********************************************************************
             near end of Setup.
2016_05_16 Corrected log file month and year handling
2016_06_27 code added in case StartStop_id: of CK_INCOMING to force update
             of Roboteq settings after programming.  Avoids need to reset system
             for changes to take effect. 
2016_07_07 removed use of RoboteqTime for finding TotalTime and replaced with use of millis()
           minor change in DisplayVolts calculation when running on PC brick
2017_06_16 Roboteq brake digout pins now connected to STM VN5160STR-E high side switches
           instead of directly to brakes, so logic in Roboteq script is inverted and
           CK_BRAKES has undergone major changes: these switches have an open load detect pin so 
           voltage dividers are no longer needed and as open load is detected in both ON and OFF
           states there is no need to query digout byte of the Roboteq to interpret CK_BRAKE results.
           So there is a single call to Aux to get the state of those two pins and when brake_ret 
           arrives, Master interprets the results.

           Roboteq script has also been changed to send digout_ret every time the contactor changes state,
           so Master is always aware of the contactor state and there's no need to query digout for that
           either.  Various procedures, such as GET_CURRENT now use the information already returned in
           digout_ret instead of re-entrant call prone query/response procedures.  The Roboteq script transmits
           the digout_ret message twice, but CK_CONTACTOR ignores the second message if the first one actually
           arrives.  (With just one transmission, some messages were lost.  With two, there seem to be no
           dropouts so Master's ContactorState is always up-to-date. With this removed, there are 
           no longer any calls to GET_BYTE so it has been commented out, but left in the source in
           case it becomes needed later.

           Several more defines added to constants.h for SpeedSlow, ContactorError and BrakeError states, so
           these are all now referenced by name rather than hard-coded numbers.
2017_06_24 added:
             pinMode (OnOffPin, OUTPUT);
             digitalWrite (OnOffPin, LOW);
             before TURN_OFF

           re-introduced if (!ModeMsg) segment at start of DO_MODE with 250 msec
           screen-repaint in Attendant, 1000 msec in Driver and no Update of
           error messages to avoid blinking.  

           CK_BRAKES modified to require repeated brake open errors before 
           setting Throttle=0 & Joystick = 0; currently set to 3 repeats
2017_07_02 Removed obsolete settings for AccelMix and current sensors - no longer used
           Settings for boosting compensation at low PWM added: MinI = minimum assumed motor current at PWM < 20%
                      LowBoost = motor current multiplier at PWM = 0, declining to zero at PWM = 20%
2017_08_07 UserSettings changed to match parameters in new Roboteq script with
              MotorCompensation changed to use only estimated motor current.
2017_08_09 void SET_OffsetCalibration (void) re-written to repeat send Volts_id messageto avoid
              re-entrant calls to CK_VOLTS 
2017_09_22 UserSettings changed to match parameters in new Roboteq script with
              corrected MotorCompensation subroutine
2017_10_25 Current sensors added back with fault detect in Roboteq script and sensor fault
              message to Display
*/






