void SWITCH_DRIVING (void){
  byte Fwd = digitalRead (FwdPin);
  byte Rev = digitalRead (RevPin); 
  byte Lft = digitalRead (LftPin);
  byte Rt = digitalRead (RtPin);
  int FwdSpeed;
  int RevSpeed;
  int FwdTurnRate;
  int RevTurnRate;
  switch (Mode){
// define throttle and steering settings for switch driving modes
    case Drive1:
      FwdSpeed = Drive1FwdSpeed; // percent of max throttle
      RevSpeed = Drive1RevSpeed; //   "
      FwdTurnRate = Drive1FwdTurnRate; // percent of max steering
      RevTurnRate = Drive1RevTurnRate; //    "
      break;
    case Drive2:
      FwdSpeed = Drive2FwdSpeed;
      RevSpeed = Drive2RevSpeed;
      FwdTurnRate = Drive2FwdTurnRate;
      RevTurnRate = Drive2RevTurnRate;
      break;
    case Drive3:
      FwdSpeed = Drive3FwdSpeed;
      RevSpeed = Drive3RevSpeed;
      FwdTurnRate = Drive3FwdTurnRate;
      RevTurnRate = Drive3RevTurnRate;
      break;
  } // end of switch (Mode)
  FwdSpeed = FwdSpeed*10; // scale to 0-1000
  RevSpeed = -1*RevSpeed*10; // negative throttle 0-1000
  FwdTurnRate = FwdTurnRate*10; // scale to 0-1000
  RevTurnRate = RevTurnRate*10; //    "
// set Throttle and Steering values in accord with switch presses
  if ((Fwd==pressed)&&(Rev==not_pressed)&&(Lft==not_pressed)&&(Rt==not_pressed)){ //forward
    Throttle = FwdSpeed;
    Steering = 0;
  }
  else if ((Fwd==not_pressed)&&(Rev==pressed)&&(Lft==not_pressed)&&(Rt==not_pressed)){ //reverse
    Throttle = RevSpeed;
    Steering = 0;
  }
  else if ((Fwd==not_pressed)&&(Rev==not_pressed)&&(Lft==pressed)&&(Rt==not_pressed)){ //left
    Throttle = 0;
    Steering = -1*FwdTurnRate;
  }
  else if ((Fwd==not_pressed)&&(Rev==not_pressed)&&(Lft==not_pressed)&&(Rt==pressed)){ //right
    Throttle = 0;
    Steering = FwdTurnRate;
  }
  else if ((Fwd==pressed)&&(Rev==not_pressed)&&(Lft==pressed)&&(Rt==not_pressed)){ //fwd-lft
    Throttle = FwdSpeed;
    Steering = -1*FwdTurnRate;
  }
  else if ((Fwd==pressed)&&(Rev==not_pressed)&&(Lft==not_pressed)&&(Rt==pressed)){ //fwd-rt
    Throttle = FwdSpeed;
    Steering = FwdTurnRate;
  }
  else if ((Fwd==not_pressed)&&(Rev==pressed)&&(Lft==pressed)&&(Rt==not_pressed)){ //rev-lft
    Throttle = RevSpeed;
    Steering = -1*RevTurnRate;
  }
  else if ((Fwd==not_pressed)&&(Rev==pressed)&&(Lft==not_pressed)&&(Rt==pressed)){ //rev-rt
    Throttle = RevSpeed;
    Steering = RevTurnRate;
  }
  else {
  // no switches pressed, or invalid combinations Fwd+Rev or Lftt+Rt
    Throttle = 0;
    Steering = 0;
  }
// ********************************************  
// add calculation of PercentThrottle here and SendByte it to Roboteq
// ********************************************  
  byte PercentThrottle = abs(Throttle*100)/1000;
  newByte = PercentThrottle;
  SEND_BYTE (PThrottle_id, 0);
} // end of SWITCH_DRIVING

void DO_SW_SEAT (void){
// lastSeatMove: 0=null, 1=forward, 2=back, 3=down, 4=up
  byte Forward = digitalRead (RtPin);
  byte Back = digitalRead (LftPin);
  byte Up = digitalRead (FwdPin);
  byte Down = digitalRead (RevPin);
  boolean Movement = ((Forward + Back + Up + Down) != 4);
  boolean Tilting = ((Forward + Back) != 2);
  boolean Lifting = ((Up + Down) != 2);
  if (!Movement){
    if (lastSeatMove != none){
      SEAT_MOVE (none);
    }
  }
  else { // Movement
// send messages only if switch states have changed and passed through "none"
    if (Forward == pressed){
      if (lastSeatMove != forward){
        SEAT_MOVE (forward);
      }
    }
    else if (Back == pressed){
      if (lastSeatMove != back){
        SEAT_MOVE (back);
      }
    }
    else if (Up == pressed){
      if (lastSeatMove != up){
        SEAT_MOVE (up);
      }
    }
    else if (Down == pressed){
      if (lastSeatMove != down){
        SEAT_MOVE (down);
      }
    }
  } // end else Movement
  RECEIVE_MESSAGE ();
} // end of DO_SW_SEAT

void DO_SW_LIGHTS (void){
  byte HeadLights = digitalRead (FwdPin);
  byte FourWay = digitalRead (RevPin);
  byte LeftTurn = digitalRead (LftPin);
  byte RightTurn = digitalRead (RtPin);
  
  static boolean SetLightsCalled = false;

  lastAmpsTime = millis(); // prevent CHECK_CURRENT from triggering an error
  
  if (SetLightsCalled && !SendByteDone){
    SET_LIGHTS ();
  }
  else if (SetLightsCalled && SendByteDone){
    SetLightsCalled = false;
    NSEWArmed = false;
  }
  else if (!SetLightsCalled) {
    if (!NSEWArmed) { // NSEWArmed
      if ((HeadLights + FourWay + LeftTurn +  RightTurn)==4){ // all switches not pressed
        if (SendByteDone){
          NSEWArmed = true;
          SetLightsCalled = false;
        }
      }
    } // arm NSEW if SendByteDone and JS near neutral
  
    else if ((HeadLights + FourWay + LeftTurn +  RightTurn)!=4){ // NSEWArmed & a switch pressed
      if (HeadLights == pressed){
        if (bitRead (LightsFlags, HeadlightBit) == OFF){
          bitWrite (LightsFlags, HeadlightBit,ON);
        }
        else if (bitRead (LightsFlags, HeadlightBit) == ON){
          bitWrite (LightsFlags, HeadlightBit,OFF);
        }
      } // end if Headlights pressed
      else if (FourWay == pressed){
        if (bitRead (LightsFlags, FourWayBit) == OFF){
          bitWrite (LightsFlags, FourWayBit,ON);
        }
        else if (bitRead (LightsFlags, FourWayBit) == ON){
          bitWrite (LightsFlags, FourWayBit,OFF);
        }
      } // end if FourWay pressed
      else if (RightTurn == pressed){
        if (bitRead (LightsFlags, RightTurnBit) == OFF){
          bitWrite (LightsFlags, RightTurnBit,ON);
          bitWrite (LightsFlags, LeftTurnBit,OFF);
        }
        else if (bitRead (LightsFlags, RightTurnBit) == ON){
          bitWrite (LightsFlags, RightTurnBit,OFF);
        }
      } // end if RightTurn pressed
      else if (LeftTurn == pressed){
        if (bitRead (LightsFlags, LeftTurnBit) == OFF){
          bitWrite (LightsFlags, LeftTurnBit,ON);
          bitWrite (LightsFlags, RightTurnBit,OFF);
        }
        else if (bitRead (LightsFlags, LeftTurnBit) == ON){
          bitWrite (LightsFlags, LeftTurnBit,OFF);
        }
      } // end if LeftTurn pressed
      SET_LIGHTS ();
      SetLightsCalled = true;
      if (SendByteDone){
        NSEWArmed = false;
        SetLightsCalled = false;
      }
    } // end NSEWArmed & a switch pressed
  } // end if (SetLightsCalled && !SendByteDone) else if (!SetLightsCalled) else if (!SetLightsCalled)
  RECEIVE_MESSAGE ();
} // end of DO_SW_LIGHTS

























