#ifndef __Globals__
#define __Globals__

// for testing use only
boolean DoAutoStep = false;
unsigned long StepTime;
byte Step = 1;


// GENERAL USE GLOBAL VARIABLES
byte UserSettings[98];
boolean DisplayLive = false;
boolean AuxLive = false;
boolean RoboteqLive = false;
boolean StartupLogged = false;
byte RoboteqForwardPin;
byte RoboteqBackPin;
byte RoboteqUpPin;
byte RoboteqDownPin;
byte RoboteqContactorPin;
byte RoboteqBrake1Pin;
byte RoboteqBrake2Pin;
byte RoboteqForwardBit;
byte RoboteqBackBit;
byte RoboteqUpBit;
byte RoboteqDownBit;
byte RoboteqContactorBit;
byte RoboteqBrake1Bit;
byte RoboteqBrake2Bit;
byte RoboteqBrake1State;
byte RoboteqBrake2State;
byte RoboteqSeatForward;
byte RoboteqSeatBack;
byte RoboteqSeatUp;
byte RoboteqSeatDown;
boolean RoboteqSettingsSent = false;
boolean SensorsSet = false;
boolean AccelSet = false;
boolean BoostsSet = false;
// boolean LimitsSet = false;
boolean MotorCompSet = false;
boolean MotorComp2Set = false;
boolean Pins1Set = false;
boolean Pins2Set = false;
boolean MotorsSet = false;

int TotalHours;
int TotalMinutes;

// for testing receipt of UserSettings from Programmer node
boolean ProgrammerRunning = false;
boolean StartGetUsrSettings = false;
boolean UsrDownloadDone = false;
boolean FirstUsrReceived = false;
// to let Programmer know that Master is awake
unsigned long AwakeTime = millis ();
boolean NoPrinting = true; // change to false to turn on diagnostic Serial.print lines
boolean SetupJustRun = true;
unsigned int LoopCycles = 0; // for timing of reading of speed pot
byte ContactorState = 1;  // 1 = contactor closed on, 0 = open
boolean NotCalibrated = false;
int Throttle = 0;
int Steering = 0;
int ThrottleStack [20];
int SteeringStack [20];
int Throttle0Cal = 0; // millivolts
int Steering0Cal = 0; // millivolts
int ThrottleFwdCal = 0;
int ThrottleRevCal = 0;
int SteeringLftCal = 0;
int SteeringRtCal = 0;
int ThrottleMaxCal = 0;
int ThrottleMinCal = 0;
int ThrottleSense = 1; // -1 to swap forward and reverse
int SteeringMaxCal = 0;
int SteeringMinCal = 0;
int SteeringSense = 1; // -1 to swap left and right
int PotMaxCal = 0;
int PotMinCal = 0;
byte DisplaySpeedPot = 0;
byte Mode = Drive1; // 1,2,3 = drive modes, 4 = seat, 5 = lights
byte ModeAddress; // address in EEPROM where last Mode stored
byte halfAHr = 0; // half AmpHours
byte DisplayVolts; // quiescent volts scaled for Display Node
byte AHrAddress; // address in EEPROM where AHr is stored
/* STOP is a push on/push off toggle to fix Throttle and Joystick
at 0 and set all switches to 1 (open) when in one state, and let them 
run in the other. */
byte STOPswitch;
boolean STOParmed = true; // StopPin pulled high after having been pulled low
int lastThrottle = 0; // for sending JoystickMessage only when changed
int lastSteering = 0; //     "
byte SetSlow = FullSpeed; // Stop, VerySlow, Slow, FullSpeed
byte RoboteqFaultFlag = 0;
byte RoboteqStatusFlag = 0b10000001; // Serial mode with script running
unsigned long lastCANMessageTime;
unsigned long lastJoystickMessageTime; // to send JoystickMessage at 100 msec intervals
                                       // if Throttle and Steering unchanged
unsigned long lastCkContactorTime = millis ();
unsigned long lastCkBrakesTime = millis ();
unsigned long RunningTime; // for new Update_Total_Time procedure
unsigned long lastMoveTime; // to send BRAKES (Set) after not moving
unsigned long lastAmpsTime; // for retrieving Roboteq battery current
unsigned long lastXcvrTime; // to measure how long transceiver has been a
byte lastSeatMove = none; // none = actuators off, 1=forward, 2=back, 3=up, 4=down
boolean NSEWArmed = true; // prevent sending of switch messages if no change
// UsrTogglePin is a physical toggle to switch between user and attendant.
byte UsrToggle = Attendant;
byte lastUsrToggle = Attendant;
boolean ToggleChangeInSleep = false; // use to force display update if toggle changed while asleep
boolean ModeMsg; // so that CAN message sent only if mode changes
boolean RUNstate = true; // not stopped
boolean MODEarmed = true; // ModePin pulled high after having been pulled low
boolean CkContactorArmed = false;
// CAN frame ID for use in sending CAN messages
unsigned long SendFrame_id;
// CAN frame ID of arriving CAN messages
unsigned long ReceiveFrame_id;
byte length,rx_status,filter,ext,Category,Name,Target;
byte frame_data[8]; // this is a buffer, a frame may have fewer bytes
boolean MsgReceived;
byte ContactorError = ContactorOK;
byte BrakeError = BrakesOK;
boolean XcvrSleep = false;
boolean JustAwakened = false;
// to prevent ckOON in READ_UsrToggle if toggled while in sleep
boolean SleepUsrToggle = false;
byte old_ADCSRA;
unsigned long Interval = 0;
boolean OONmsgSent = false;
boolean SwMsgSent = false;
// for JoystickCalibration
byte ModePressed = not_pressed;
byte FwdPressed = not_pressed;
boolean CalibrationRunning = false;
boolean VoltCalibrationSet = false;
boolean OffsetCalibrationSet = false;

// state variables and return values for CAN messaging
boolean Charging = true;
boolean CheckAmpsDone = false;
boolean CheckVoltsDone = false;
boolean StartingVoltsDone = false;
int deciAmps;
boolean VoltsRead = false;
int deciVolts = 0;
int InVolts = 0;
int OutVolts = 0;
int VoltageDividerOffset = 0;
int VoltCalibration = 275; // set low to avoid CHARGING! during first startup
boolean DigOutReceived = false;

// used for Contactor and Seat
byte DigFlags = 0;
byte newDigFlags = 0; // sent in SET_FLAGS as newFlags if different from DigFlags;
// could be eliminated but improves readability

// used for Lights
byte LightsFlags = 0b1111; // OFF = 1
byte oldLightsFlags = 0b1111; // OFF = 1

// used for all flag and single byte value setting routines
byte Byte = 0;
byte newByte = 0; // send in SET_FLAGS if different from Flags
byte DigOutFlags = 0; // value returned in DigSet_ret message specific
                      // to Roboteq DigOout flags.
byte BrakeStatus = 0b11; // value returned in brakes_ret message, start with both coils OK
boolean SendByteDone = false;

// used for sending 2 byte (INT)
int Int = 0;
int newInt = 0;
int returnedInt = 0;
boolean IntReturned = false;
boolean SendIntDone = false;

// to retrieve bytes
boolean GetByteDone =  false;
boolean ByteReceived = false;
byte ReturnedByte;

// to retrieve 16 bit integers
boolean GetIntDone = false;
boolean IntReceived = false;
int ReturnedInt;

// to retrieve 32 bit integers (e.g. RoboteqTIME)
boolean GetLongDone = false;
boolean LongReceived = false;
boolean VoltsReceived = false;
unsigned long ReturnedLong;

// used only in RoboteqNeutral ()
boolean JoystickConfirmed = false;

#endif













