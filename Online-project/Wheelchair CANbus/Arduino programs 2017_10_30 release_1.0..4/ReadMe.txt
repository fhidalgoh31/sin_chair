After extracting the zip file:

(1) copy the subfolders in the "Libraries to put in Arduino user Libraries folder"
to wherever you are keeping libraries not furnished withe the Arduino IDE.

and

(2) copy the other folders into wherever you are keeping your Arduino sketchbook.

MasterNode, AuxNode, DisplayNode and ProgrammerNode are
the programs for the CANbus modules.

The files in the WC_CAN library are used by all of the above.

Monitor_0b10 and Monitor_0b1000 are simple programs for
checking new hardware for proper CAN communication.

DS3231_RTC_reset2 is used in place of the DisplayNode
program to set the time of the real time clock.

The settings included in this copy of ProgrammerNode are
conservative.  They have to be changed to match your chair
and tuned for your driving style as well as the chair's
characteristics.  Tuning the parameters that affect Motor
Compensation are particularly critical and setting them
too high can produce very unsafe behavior.  A suggested
tuning protocol is:

(1) First, increase MotorResistance in small increments
testing by moving straight fore and aft at moderate speed.
(If you already know the rated resistance of your motors,
you can start with a value about 10 to 20% under that, and 
then make small increments).  BE READY TO HIT THE EMERGENCY
SHUTOFF IF NEEDED WHILE YOU ARE DOING this. Continue until
you feel the first signs of skittishness, and then back off
by about 15%.  Increase again with smaller increments and 
once there's any sign of instability, back off by 10%.  Now, 
test fore - aft movement from moderate to high speed and with 
one wheel climbing over an obstacle and adding some turns while
moving.  If there is any sign of mis-behavior, lower the value a bit.  
DO NOT PAY ATTENTION TO BEHAVIOR AT VERY LOW SPEEDS.

(2) If you are NOT using current sensors, that is with
UseCurrentSensors = NO, adjust LowILimit so that you get some
movement at small Throttle movement.  You want the minimum value
that lets the chair start moving OK, but don't exaggerate or
you will lose load compensation over too wide a speed range.

(3) Now, try a turn-in-place after moving straight forward and
after moving straight back.  Depending on center of gravity,
caster geometry and caster tires it may be difficult to start a
turn-in-place; worse after going forward in a rear wheel drive, 
worse after going backwards in a front wheel drive.  If so, start
SLOWLY increasing CompTurnBoost until you get a reasonable start of
turn-in-place.

(4) NOT FOR BRUSHLESS MOTORS.  (This is not a MotorCompensation 
adjustment, but something that uses MotorCompensation so you 
have to adjust this last.)  You may find that acceleration is 
relatively sluggish at low speeds with Accel and Decel settings
that work well at mid and high speeds.  If so, start increasing
LoadBoost_D in small steps until this evens out.  You may want to
then lower Accel and Decel a bit and then repeat the adjustment
of LoadBoost_D.

(5) If you have done the above with UseCurrentSensors = YES the same
settings will work when UseCurrentSensors = NO except for LowIBoost
that only has meaning when UseCurrentSensors = NO.  So now, switch to
UseCurrentSensors = NO and adjust LowILimit so that you get some
movement at small Throttle movement.  You want the minimum value
that lets the chair start moving OK, but don't exaggerate or
you will lose load compensation over too wide a speed range.

For all of these settings, too low a value may give a chair that's
not very comfortable to drive, BUT TOO HIGH A VALUE may make it
unsafe.  Please be cautious.