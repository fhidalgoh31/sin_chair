#ifndef __MsgIDs__
#define __MsgIDs__

// << 5 done for all IDs in CAN.cpp;
// IDs with "ret" suffix are values returned from another node in response to an "id" query
// IDs with "confirm" suffix are confirmations of exchanges initiated by an "id" message from another node

#define NodeLive_id (uint16_t) (((GetValue << 6 | _STARTUP) << 3) | toMaster)
#define SetSlow_id (uint16_t) (((SetCommand << 6 | _SLOW ) << 3) | toMaster)
#define SetSlow_confirm (uint16_t) (((SetCommand << 6 | _SLOW ) << 3) | toAux)
#define RoboteqFault_id (uint16_t) (((GetValue << 6 | _FLTFLAG ) << 3) | toMaster)
#define RoboteqFault_confirm (uint16_t) (((GetValue << 6 | _FLTFLAG ) << 3) | toRoboteq)
#define RoboteqStatus_id (uint16_t) (((GetValue << 6 | _STFLAG ) << 3) | toMaster)
#define RoboteqStatus_confirm (uint16_t) (((GetValue << 6 | _STFLAG ) << 3) | toRoboteq)
#define BatAmps_id (uint16_t) (((GetValue << 6 | _BATAMPS ) << 3) | toRoboteq)
#define BatAmps_ret (uint16_t) (((GetValue << 6 | _BATAMPS ) << 3) | toMaster)
#define Volts_id (uint16_t) (((GetValue << 6 | _VOLTS ) << 3) | toAux)
#define Volts_ret (uint16_t) (((GetValue << 6 | _VOLTS ) << 3) | toMaster)
#define Brakes_id (uint16_t) (((GetValue << 6 | _BRAKES ) << 3) | toAux)
#define Brakes_ret (uint16_t) (((GetValue << 6 | _BRAKES ) << 3) | toMaster)
#define Seat_id (uint16_t) (((SetCommand << 6 | _SEATCMD ) << 3) | toRoboteq) // not used
#define Seat_ret (uint16_t) (((SetCommand << 6 | _SEATCMD ) << 3) | toMaster) // not used
#define Lights_id (uint16_t) (((SetCommand << 6 | _LIGHTSCMD ) << 3) | toAux)
#define Lights_ret (uint16_t) (((SetCommand << 6 | _LIGHTSCMD ) << 3) | toMaster)
#define DigSet_id (uint16_t) (((SetCommand << 6 | _DSET ) << 3) | toRoboteq)
#define DigSet_ret (uint16_t) (((SetCommand << 6 | _DSET ) << 3) | toMaster)
#define Time_id (uint16_t) (((GetValue << 6 | _TIME ) << 3) | toRoboteq)
#define Time_ret (uint16_t) (((GetValue << 6 | _TIME ) << 3) | toMaster)
#define AuxSleepWake_id (uint16_t) (((Config << 6 | _XCVR ) << 3) | toAux)
#define DisplaySleepWake_id (uint16_t) (((Config << 6 | _XCVR ) << 3) | toDisplay)
#define Aux_Dummy (uint16_t) (((Config << 6 | 0 ) << 3) | toAux)
#define Display_Dummy (uint16_t) (((Config << 6 | 0 ) << 3) | toDisplay)
#define DigOut_id (uint16_t) (((GetValue << 6 | _DIGOUT ) << 3) | toRoboteq)
#define DigOut_ret (uint16_t) (((GetValue << 6 | _DIGOUT ) << 3) | toMaster)
#define GetWatchdog_id (uint16_t) (((Config << 6 | _RWD ) << 3) | toRoboteq)
#define GetWatchdog_ret (uint16_t) (((Config << 6 | _RWD ) << 3) | toMaster)
// 4 bytes for LimpSpeed, UseCurrentSensors, L&R sensor polarity
#define Sensors_id (uint16_t) (((Config << 6 | _SENSOR ) << 3) | toRoboteq)
#define Sensors_ret (uint16_t) (((Config << 6 | _SENSOR ) << 3) | toMaster)
// 2 bytes for _MAC and 2 bytes for _MDEC (i.e. Accel and Decel)
#define Accel_id (uint16_t) (((Config << 6 | _MAC ) << 3) | toRoboteq)
#define Accel_ret (uint16_t) (((Config << 6 | _MAC ) << 3) | toMaster)
// 4 bytes for AccelMix, SpeedBoost, TurnBoost and IntBoost
#define Boosts_id (uint16_t) (((Config << 6 | _BOOSTS ) << 3) | toRoboteq)
#define Boosts_ret (uint16_t) (((Config << 6 | _BOOSTS ) << 3) | toMaster)
// 4 bytes for BoostLimit, MidBoostLimit, SpeedTurnBalance and BackStickBraking
#define Limits_id (uint16_t) (((Config << 6 | _LIMITS ) << 3) | toRoboteq)
#define Limits_ret (uint16_t) (((Config << 6 | _LIMITS ) << 3) | toMaster)
// 4 bytes for MotorResistance, MotorCompBoost and BoostCurving
#define MotorComp_id (uint16_t) (((Config << 6 | _MCOMP ) << 3) | toRoboteq)
#define MotorComp_ret (uint16_t) (((Config << 6 | _MCOMP) << 3) | toMaster)
// 3 bytes for MotorCompDeadband, TurnCompBoost & TurnAccelBoost
#define MotorComp2_id (uint16_t) (((Config<<6|_MCOMP2)<<3)|toRoboteq)
#define MotorComp2_ret (uint16_t) (((Config<<6|_MCOMP2)<< 3)|toMaster)
// 4 bytes for seat tilt and lift digout pins
#define Pins1_id (uint16_t) (((Config << 6 | _PINS1 ) << 3) | toRoboteq)
#define Pins1_ret (uint16_t) (((Config << 6 | _PINS1) << 3) | toMaster)
// 4 bytes for current sensor analog inputs and brake digout pins
#define Pins2_id (uint16_t) (((Config << 6 | _PINS2 ) << 3) | toRoboteq)
#define Pins2_ret (uint16_t) (((Config << 6 | _PINS2 ) << 3) | toMaster)
// 4 bytes for SwapMotors, M1 & M2 Polarity, Contactor digout pin
#define Motors_id (uint16_t) (((Config << 6 | _MOTORS ) << 3) | toRoboteq)
#define Motors_ret (uint16_t) (((Config << 6 | _MOTORS ) << 3) | toMaster)

#define Joystick_id (uint16_t) (((SetCommand << 6 | _GO ) << 3) | toRoboteq)
#define Joystick_ret (uint16_t) (((SetCommand << 6 | _GO ) << 3) | toMaster)
#define PThrottle_id (uint16_t) (((SetCommand << 6 | _PTHROTTLE ) << 3) | toRoboteq)
#define PThrottle_ret (uint16_t) (((SetCommand << 6 | _PTHROTTLE ) << 3) | toMaster)
#define Pot_id (uint16_t) (((SetCommand << 6 | _POT ) << 3) | toRoboteq)
#define Pot_ret (uint16_t) (((SetCommand << 6 | _POT ) << 3) | toMaster)
// for Programmer node messaging
#define StartStop_id (uint16_t) (((Config << 6 | _StartStop ) << 3) | toMaster)
#define Calibration_id (uint16_t) (((Config << 6 | _CAL ) << 3) | toMaster)
#define Calibration_ret (uint16_t) (((Config << 6 | _CAL ) << 3) | toProgrammer)
#define CkJS_id  (uint16_t) (((Config << 6 | _CkJS ) << 3) | toMaster)
#define CkJS_ret  (uint16_t) (((Config << 6 | _CkJS ) << 3) | toProgrammer)
#define Value_ret (uint16_t) (((Config << 6 | _CalValue ) << 3) | toProgrammer)
#define UsrSettings_id (uint16_t) (((Config << 6 |_USR) << 3) | toMaster)
// temporary addition
#define ProgrammerMsg_ret (uint16_t) (((Config << 6 | _Msg ) << 3) | toProgrammer)
// use UsrSettings_ret to let Programmer know that Master is awake
#define UsrSettings_ret (uint16_t) (((Config << 6 |_USR) << 3) | toProgrammer)
#define GetLogFile_id (uint16_t) (((Config << 6 |_LOG) << 3) | toDisplay)
#define LogFile_ret (uint16_t) (((Config << 6 |_LOG) << 3) | toProgrammer)
#define Usr_id (uint16_t) (((Config << 6 | _UsrRet ) << 3) | toMaster)
#define Usr_ret (uint16_t) (((Config << 6 | _UsrRet ) << 3) | toProgrammer)
#define VoltCalib_id (uint16_t) (((Config << 6 | _VOLTS ) << 3) | toMaster)
#define VoltCalib_ret (uint16_t) (((Config << 6 | _VOLTS ) << 3) | toProgrammer)


#endif










