#ifndef __Labels__
#define __Labels__

// store User Setting labels in PROGMEM
// JOYSTICK AND SPEED POT SETTINGS:
const char usr0[] PROGMEM = "SpeedPotRevMin";
const char usr1[] PROGMEM = "SpeedPotRevMax";
const char usr2[] PROGMEM = "TurnPotFwdMin";
const char usr3[] PROGMEM = "TurnPotRevMin";
const char usr4[] PROGMEM = "TurnAtFullSpeed";
const char usr5[] PROGMEM = "ThrottleCurving";
const char usr6[] PROGMEM = "SteeringCurving";
const char usr7[] PROGMEM = "Deadband";
const char usr8[] PROGMEM = "Damping";
const char usr9[] PROGMEM = "JswitchTrigger";
const char usr10[] PROGMEM = "SpeedPotInstalled";
const char usr11[] PROGMEM = "SpeedPot";
const char usr12[] PROGMEM = "TurnPotFwdMax";
const char usr13[] PROGMEM = "TurnPotRevMax";
const char usr14[] PROGMEM = "SpeedPotFwdMin";
const char usr15[] PROGMEM = "SpeedPotFwdMax";
const char usr16[] PROGMEM = "SpeedPotLowFault";
const char usr17[] PROGMEM = "SpeedPotHighFault";
const char usr18[] PROGMEM = ""; // unused
// SWITCH DRIVING SETTINGS:
const char usr19[] PROGMEM = "Drive1FwdSpeed";
const char usr20[] PROGMEM = "Drive1RevSpeed";
const char usr21[] PROGMEM = "Drive1FwdTurnRate";
const char usr22[] PROGMEM = "Drive1RevTurnRate";
const char usr23[] PROGMEM = "Drive2FwdSpeed";
const char usr24[] PROGMEM = "Drive2RevSpeed";
const char usr25[] PROGMEM = "Drive2FwdTurnRate";
const char usr26[] PROGMEM = "Drive2RevTurnRate";
const char usr27[] PROGMEM = "Drive3FwdSpeed";
const char usr28[] PROGMEM = "Drive3RevSpeed";
const char usr29[] PROGMEM = "Drive3FwdTurnRate";
const char usr30[] PROGMEM = "Drive3RevTurnRate";
// CHAIR SETUP:
const char usr32[] PROGMEM = "TimeToSleep";
// SPEED REDUCTIONS
const char usr33[] PROGMEM = "Slow1";
const char usr34[] PROGMEM = "Slow2";
const char usr35[] PROGMEM = "LimpSpeed";
// Roboteq SETTINGS
const char usr31[] PROGMEM = "LoadBoostCurving";
const char usr36[] PROGMEM = ""; // obsolete AccelMix
const char usr37[] PROGMEM = ""; // obsolete LoadBoost_I
const char usr38[] PROGMEM = ""; // obsolete SpeedBoost
const char usr39[] PROGMEM = "TurnBoost";
const char usr40[] PROGMEM = ""; // obsolete MinPWM
const char usr41[] PROGMEM = "BackStickBraking";
const char usr42[] PROGMEM = "LowIBoost";
const char usr43[] PROGMEM = ""; // obsolete TransitionPWM
const char usr44[] PROGMEM = "UseCurrentSensors";
const char usr45[] PROGMEM = "LeftSensorPin";
const char usr46[] PROGMEM = "RightSensorPin";
const char usr47[] PROGMEM = "LeftSensorReversePolarity";
const char usr48[] PROGMEM = "RightSensorReversePolarity";
const char usr49[] PROGMEM = "TiltForwardPin";
const char usr50[] PROGMEM = "TiltBackPin";
const char usr51[] PROGMEM = "LiftUpPin";
const char usr52[] PROGMEM = "LiftDownPin";
const char usr53[] PROGMEM = "Brake1Pin";
const char usr54[] PROGMEM = "Brake2Pin";
const char usr55[] PROGMEM = "ContactorPin";
const char usr56[] PROGMEM = "SwapMotors";
const char usr57[] PROGMEM = "Motor1ReversePolarity";
const char usr58[] PROGMEM = "Motor2ReversePolarity";
const char usr59[] PROGMEM = "LoadBoost_D";
const char usr60[] PROGMEM = "CompDeadband";
const char usr61[] PROGMEM = "CompTurnBoost";
const char usr62[] PROGMEM = "LoadBoost_P";
const char usr63[] PROGMEM = ""; // unused
const char usr64[] PROGMEM = ""; // unused
const char usr65[] PROGMEM = ""; // unused
const char usr66[] PROGMEM = ""; // unused
const char usr67[] PROGMEM = ""; // unused
const char usr68[] PROGMEM = ""; // unused
const char usr69[] PROGMEM = ""; // unused
const char usr70[] PROGMEM = ""; // unused
const char usr71[] PROGMEM = ""; // unused
const char usr72[] PROGMEM = ""; // unused
const char usr73[] PROGMEM = ""; // unused
const char usr74[] PROGMEM = ""; // unused
const char usr75[] PROGMEM = ""; // unused
// DISPLAY MESSAGING
const char usr76[] PROGMEM = "VoltsFull";
const char usr77[] PROGMEM = "VoltsFull";
const char usr78[] PROGMEM = "VoltsEmpty";
const char usr79[] PROGMEM = "VoltsEmpty";
const char usr80[] PROGMEM = "AHrFull";
const char usr81[] PROGMEM = "AHrFull";
const char usr82[] PROGMEM = "AHrEmpty";
const char usr83[] PROGMEM = "AHrEmpty";
// MORE Roboteq SETTINGS
const char usr84[] PROGMEM = "Accel";
const char usr85[] PROGMEM = "Accel";
const char usr86[] PROGMEM = "Decel";
const char usr87[] PROGMEM = "Decel";
const char usr88[] PROGMEM = ""; // unused
const char usr89[] PROGMEM = ""; // unused
const char usr90[] PROGMEM = ""; // unused
const char usr91[] PROGMEM = ""; // unused
const char usr92[] PROGMEM = "RechargeVolts";
const char usr93[] PROGMEM = "RechargeVolts";
const char usr94[] PROGMEM = "LowVoltLimit";
const char usr95[] PROGMEM = "LowVoltLimit";
const char usr96[] PROGMEM = "MotorResistance";
const char usr97[] PROGMEM = "MotorResistance";

const char *const UsrSettingLabels[] PROGMEM = 
{ 
  usr0, usr1, usr2, usr3, usr4, usr5, usr6, usr7, usr8, usr9,
  usr10, usr11, usr12, usr13, usr14, usr15, usr16, usr17, usr18, usr19,
  usr20, usr21, usr22, usr23, usr24, usr25, usr26, usr27, usr28, usr29,
  usr30, usr31, usr32, usr33, usr34, usr35, usr36, usr37, usr38, usr39,
  usr40, usr41, usr42, usr43, usr44, usr45, usr46, usr47, usr48, usr49,
  usr50, usr51, usr52, usr53, usr54, usr55, usr56, usr57, usr58, usr59,
  usr60, usr61, usr62, usr63, usr64, usr65, usr66, usr67, usr68, usr69,
  usr70, usr71, usr72, usr73, usr74, usr75, usr76, usr77, usr78, usr79,
  usr80, usr81, usr82, usr83, usr84, usr85, usr86, usr87, usr88, usr89,
  usr90, usr91, usr92, usr93, usr94, usr95, usr96, usr97
};

char LabelBuffer[30]; // was 22
#endif




