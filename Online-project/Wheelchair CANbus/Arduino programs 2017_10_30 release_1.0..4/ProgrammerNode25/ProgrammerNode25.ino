  /*
===== MAKE SURE TO SWAP CAN LIBRARY SO THAT IT MATCHES CURRENT MODULES =====
***** TESTED WITH Arduino 1.8.0.  NOT tested with later versions *****
*/

#include <CAN.h>
#include  <avr/pgmspace.h> // PROGMEM (flash) handling
#include <EEPROM.h>
#include <SPI.h>
#include <Constants.h>
#include <MsgIDs.h>
#include <Labels.h>
#include "Globals.h"

// pin A2 #defined as RESET_PIN in CAN.h & used for MCP2151 reset
// PowerOnPin not used because Programmer is turned off by disconnecting from CAN bus.  
// #define PowerOnPin A1 // OUTPUT HIGH to turn DC-DC converter OFF
#define MSG_IN_BUFFER0 (rx_status & 0x40)
#define MSG_IN_BUFFER1 (rx_status & 0x80)
#define UsrAddress 100 // first EEPROM address for storing user settings
#define YES 1
#define Yes 1
#define yes 1
#define NO 0
#define No 0
#define no 0

void setup (void) {
  Serial.begin(115200);
  Serial.println(__FILE__ " - " __DATE__ " " __TIME__);
  Serial.println ();
  MasterSettings ();
  RoboteqSettings ();
  UsrToEEPROM (); // load ProgrammerUserSettings array and write to EEPROM
  Check_Ranges (); // check for out of range user settings
  if (!UsrFault){
    Serial.println (F("************************************************************"));
    Serial.println (F("  WHEELCHAIR MUST BE TURNED ON AND AWAKE BEFORE CONTINUING  "));
    Serial.println (F("                send any key to continue"));
    Serial.println (F("************************************************************"));
    Serial.println ();
    StartTime = millis ();
    do {
      delay (1);
    } while (!Serial.available());
    if (Serial.available ()){
      SerialInput = Serial.read ();
      SETUP_CAN ();
      delay (10);
      StartTime = millis ();
      boolean InstructionsSent = false;
      Serial.print (F("Starting"));
      delay (100);
      while (!SystemOn) {
        do {
          RECEIVE_MESSAGE ();
          int interval = (millis () - StartTime);
          if ((interval%500 == 0)&&!InstructionsSent){
            Serial.print (F(" ."));
            delay (1);
          }
          if (interval >= 5000){
            if (!InstructionsSent){
              Serial.println ();
              Serial.println (F("Startup is taking too long: "));
              Serial.println (F("  Is the Programmer plugged in to a CAN socket?"));
              Serial.println (F("  Is WheelchairCAN asleep?"));
              Serial.println (F("  Did you forget to turn WheelchairCAN ON before sending a keystroke?"));
              Serial.println (F("----- Just plug it in, wake it up or turn it on and programming will continue -----"));
              InstructionsSent = true;
              ProgrammerRestart = true;
            }
            SETUP_CAN ();
            delay (50);
            RECEIVE_MESSAGE ();
          }
        } while (!MasterAwake);
        if (MasterAwake){
          SystemOn = true;
        }
      } // end while (!SystemOn)
    } // end if (Serial.available ())
    if (ProgrammerRestart){
      Serial.println ();
      Serial.println (F("Programmer node is resuming"));
      for (byte i = 1; i <= 10; i++){
        Serial.print (F(" ."));
        delay (200);
      }
      ProgrammerRestart = false;
    }
    StartStop = false;
    for (byte i = 1; i<=5; i++){
      StartStop_Programmer (); // exits any  left over blocking 
                               // READ_MESSAGE loop in Master
      delay (10);
    }
    do {
      Serial.read ();
    } while (Serial.available ());
    StartStop = true;
    StartStop_Programmer (); // starts a READ_MESSAGE loop in Master
                             // to keep Master from entering its Loop ().
    Serial.println ();
    CheckNewMCU ();
    CalibrateJoystick ();
    CalibrateVoltage ();
    CalibrateOffset ();
    InitializeUserSettings ();
    VerifyUserSettings ();
    ReadLogFiles ();
    StartStop = false;
    StartStop_Programmer (); // exits blocking READ_MESSAGE loop in Master
                             // so that it reneters its normal Loop ().
    Serial.println (F("*********************************************"));
    Serial.println (F("              -- END -- "));
    Serial.println ();
    CAN.setMode(CONFIGURATION);
  } // end if (!UsrFault)
} // end setup ()

void loop (void) {
} // end loop

void RECEIVE_MESSAGE (void){
  MsgReceived = false;
  byte pairs;
  boolean check = true;
  // clear receive buffers, just in case.
  for (byte i = 0; i <= 7; i++){
    frame_data[i] = 0x00;
  }
  ReceiveFrame_id = 0x00;
  rx_status = CAN.readStatus();
  if (MSG_IN_BUFFER0){
    CAN.readDATA_ff_0(&length,frame_data,&ReceiveFrame_id, &ext, &filter);
    //    printBuf(rx_status, length, ReceiveFrame_id, filter, 0, frame_data, ext);
    pairs = length/2;
    for (byte j=0; j<=(pairs-1); j++){
      if ((frame_data[j] ^ frame_data[j+pairs]) != 0xFF){
        // following works, BUT ~frame_data[j] returns unsigned int unless (byte) used
        //      if ((byte)~frame_data[j] == frame_data[j+pairs]){
        check = false;
        break;
      }
    }
  }
  /* in original code from which I lifted this, the following was
   "if" rather than "else if".  Doing that means that a lower-priority
   (buffer 1) message could replace a higher priority (buffer 0) message
   if both buffers were full.  Using "else if" will cause buffer 1 to be
   read on next pass if another buffer 0 message has not arrived in the
   meantime.
   */
  else if (MSG_IN_BUFFER1){
    CAN.readDATA_ff_1(&length,frame_data,&ReceiveFrame_id, &ext, &filter);
    //    printBuf(rx_status, length, ReceiveFrame_id, filter, 1, frame_data, ext); 
    pairs = length/2;
    for (byte j=0; j<=(pairs-1); j++){
      if ((frame_data[j] ^ frame_data[j+pairs]) != 0xFF){
        check = false;
        break;
      }
    }
  }
  if ((MSG_IN_BUFFER0) || (MSG_IN_BUFFER1)){
    if (check){
      MsgReceived = true;
      CK_INCOMING ();
    } // end check for corrupted message
    else {
      MsgReceived = false;
    }
  } // filtered message in receive buffer
  else {
    MsgReceived = false;
  } // no filtered message in receive buffer
} // end RECEIVE_MESSAGE ()

void CK_INCOMING (void) {
  byte index;
  byte setting;
  int Value;
  switch (ReceiveFrame_id){
  case CkJS_ret:
    if (!CkJSDone){
      NotCalibrated = frame_data[0];
      CkJSDone = true;
    }
    break;
  case Calibration_ret:
    switch (frame_data[0]){
    case 1: //Throttle0Cal = step 1
      Serial.print (F("  stored Throttle0 was "));
      break;
    case 2:
      Serial.print (F("  new Throttle0 is "));
      break;
    case 3: // go to Steering0 without text
      Serial.println ();
      break;
    case 10:  // Steering0Cal = step 4
      Serial.print (F("  stored Steering0 was "));
      break;
    case 11:
      Serial.print (F("  new Steering0 is "));
      break;
    case 12:
      Serial.println ();
      Serial.println (F("Hold joystick in full forward and press Mode switch to start ThrottleFwd calibration"));
      if (!NotCalibrated){
        Serial.println (F("Press Forward switch to skip this step"));
      }
      break;
    case 4: // ThrottleFwdCal = step 2
      Serial.print (F("  stored ThrottleFwd was "));
      break;
    case 5:
      Serial.print (F("  new ThrottleFwd is "));
      break;
    case 6:
      Serial.println ();
      Serial.println ();
      Serial.println (F("Hold joystick in full reverse and press Mode switch to start ThrottleRev calibration"));
      if (!NotCalibrated){
        Serial.println (F("Press Forward switch to skip this step"));
      }
      break;
    case 7: // ThrotteRevCal = step 3
      Serial.print (F("  stored ThrottleRev was "));
      break;
    case 8:
      Serial.print (F("  new ThrottleRev is "));
      break;
    case 9:
      Serial.println ();
      Serial.println ();
      Serial.println (F("Hold joystick full left and press Mode switch to start SteeringLft calibration"));
      if (!NotCalibrated){
        Serial.println (F("Press Forward switch to skip this step"));
      }
      break;
    case 13: // SteeringLftCal = step 5
      Serial.print (F("  stored SteeringLft was "));
      break;
    case 14:
      Serial.print (F("  new SteeringLft is "));
      break;
    case 15:
      Serial.println ();
      Serial.println ();
      Serial.println (F("Hold joystick full right and press Mode switch to start SteeringRt calibration"));
      if (!NotCalibrated){
        Serial.println (F("Press Forward switch to skip this step"));
      }
      break;
    case 16: // SteeringRtCal = step 6
      Serial.print (F("  stored SteeringRt was "));
      break;
    case 17:
      Serial.print (F("  new SteeringRt is "));
      break;
    case 18:
      Serial.println ();
      Serial.println ();
      Serial.println (F("Set speed pot fully counterclockwise and press Mode switch to start SpeedPotMin calibration"));
      if (!NotCalibrated){
        Serial.println (F("Press Forward switch to skip this step"));
      }
      break;
    case 19: // PotMinCal = step 7
      Serial.print (F("  stored SpeedPotMin was "));
      break;
    case 20:
      Serial.print (F("  new SpeedPotMin is "));
      break;
    case 21:
      Serial.println ();
      Serial.println ();
      Serial.println (F("Set speed pot fully clockwise and press Mode switch to start SpeedPotMax calibration"));
      if (!NotCalibrated){
        Serial.println (F("Press Forward switch to skip this step"));
      }
      break;
    case 22: // PotMaxCal = step 8
      Serial.print (F("  stored SpeedPotMax was "));
      break;
    case 23:
      Serial.print (F("  new SpeedPotMax is "));
      break;
    case 24:
      Serial.println ();
      Serial.println ();
      Serial.println (F("*********************"));
      Serial.println (F("CALIBRATION COMPLETED"));
      Serial.println (F("*********************"));
      CalibrationDone = true;
      NotCalibrated = false;
    } // end switch (frame_data[0]
    break; // case (Calibration_ret)
  case VoltCalib_ret:
    Value = (int)(frame_data[0]<<8)|(int)frame_data[1];
    if (Value != 0){
      if (!VoltCalibrationReceived){
        VoltCalibration = Value;
        VoltCalibrationReceived = true;
        deciVolts = (int)(frame_data[2]<<8)|frame_data[3];
      }
    }
    else if (!OffsetCalibrationReceived){
      Offset = (int)(frame_data[2]<<8)|frame_data[3];
      OffsetCalibrationReceived = true;
    }
    break;
  case Value_ret:
    Value = (int)frame_data[0]<<8 | frame_data[1];
    Serial.print (Value);
    Value = 0;
    break;
  case UsrSettings_ret:
    MasterAwake = true;
    break;
  case Usr_ret:
    // user settings sent from Master node
    index = frame_data[0];
    if (index == sizeof(MasterUserSettings)){
      UsrSettingsDone = true;
      if (frame_data[1] != 0b1010101) {
        NewMCU = true;
      }
    }
    else if (index <= sizeof(MasterUserSettings)-1){// added if clause to protect from bad frame_data[0]
      setting = frame_data[1];
      MasterUserSettings[index] = setting;
      EEPROM.update (UsrAddress+index, MasterUserSettings[index]);
    }
    break;
  case LogFile_ret:
    // characters sent from Display node
    if ((frame_data[0]==0xFF)&&(frame_data[1]==0xFF)
      &&(frame_data[2]==0xFF)&&(frame_data[3]==0xFF)){
      LogFileDone = true;
      Serial.println ();
    }
    else {
      char c0 = frame_data[0];
      char c1 = frame_data[1];
      char c2 = frame_data[2];
      char c3 = frame_data[3];
      Serial.print (c0);
      Serial.print (c1);
      Serial.print (c2);
      Serial.print (c3);
    }
    break;
  } // end switch (ReceiveFrame_id)
} // end CK_INCOMING

void SEND_MESSAGE (byte Number, unsigned int ID, byte *DATA){
  SendFrame_id = ID;
  if (!NoPrinting){
    Serial.println (F(""));
    Serial.print (F("SendFrame_id = "));
    Serial.print (SendFrame_id);
    Serial.print (F(" = 0x"));
    Serial.print (SendFrame_id, HEX);
    Serial.print (F(" = 0b"));
    Serial.println (SendFrame_id, BIN);
    Serial.println (F("data bytes:"));
    Serial.print (F("0b"));
    for (byte i = 0; i<(Number-1); i++){
      Serial.print (DATA[i], BIN);
      Serial.print (F("   0b"));
    }
//    Serial.println (DATA[Number], BIN); // this is printing 1 byte past DATA
    Serial.println ();
  }
  /* void CANClass::load_ff_g(byte lengtange priorities. 
   Lower priority messages can be sent by calling e.g. load_ff_0  
   
   NOTE 2012_1_6: despite this, only 1st message is sent to tx buffer 2, all others are 
   sent to buffer 0.  FIXED, off by 1 bit in MCP 2151 READ STATUS byte
   
   2012_1_9: transmission on buffers 0 and 1 work fine, but on burrer
   2 only the ID seems to be sent, but data received are all 0x0 and 
   complements = 0xFF?  For now, CAN.load__ff_g changed to send only to
   buffers 0 and 1.
   */
  CAN.load_ff_g(Number,&SendFrame_id,DATA, false);
  // CAN.load_ff_1(Number,&SendFrame_id,DATA, false);
} // end SEND_MESSAGE

void SETUP_CAN (void) {
  // initialize CAN bus class
  // this class initializes SPI communications with MCP2515
  CAN.begin();
  CAN.setMode(CONFIGURATION);  // set to "NORMAL" for standard communications
  CAN.baudConfig(BUS_SPEED);
  /*
   The lower the ID value, the higher the priority.
   Highest priority messages go first to buffer 0, then to buffer 1
   if buffer 0 is full (depends on MCP2151 register setting)
   or if buffer 0 filter not matched.
   If 2 messages arrive at the same time, higher priority msg goes to
   buffer 0, lower priority msg goes to buffer 1.
   "Category" in 2 msb of ID determines priority of Roboteq message groups: 
   0b00=SetCommand, 0b01=GetValue, 0b10 and 0b11 = configuration.
   Within a catagory, "Name" in next 6 bits determines priority.
   Thus, SetCommand is highest priority group, and _G (motor output setting = 0)
   has absolute highest priority.
   */
  CAN.setMaskOrFilter(MASK_0,   0b00000000, 0b11100000, 0b00000000, 0b00000000);
  CAN.setMaskOrFilter(FILTER_0, 0b00000000, 0b00000000, 0b00000000, 0b00000000); // toProgrammer
  CAN.setMaskOrFilter(FILTER_1, 0b00000000, 0b00000000, 0b00000000, 0b00000000); // toProgrammer

  CAN.setMaskOrFilter(MASK_1,   0b00000000, 0b11100000, 0b00000000, 0b00000000);
  CAN.setMaskOrFilter(FILTER_2, 0b00000000, 0b00000000, 0b00000000, 0b00000000); // toProgrammer
  CAN.setMaskOrFilter(FILTER_3, 0b00000000, 0b00000000, 0b00000000, 0b00000000); // toProgrammer
  //  CAN.setMaskOrFilter(FILTER_4, 0b00000000, 0b11100000, 0b00000000, 0b00000000); // toDisplay
  CAN.setMaskOrFilter(FILTER_5, 0b00000000, 0b00000000, 0b00000000, 0b00000000); // toProgrammer
  CAN.setMode(NORMAL);  // set to "NORMAL" for standard com
} // end SETUP_CAN

void printBuf(byte rx_status, byte length, uint32_t ReceiveFrame_id, byte filter, byte buffer, byte *frame_data, byte ext) {
  Serial.print (F("[Rx] Status:"));
  Serial.print (rx_status,HEX);
  Serial.print (F(" Len:"));
  Serial.print (length,HEX);
  Serial.print (F(" Frame:0x"));
  Serial.print (ReceiveFrame_id,HEX);
  Serial.print (F(" EXT?:"));
  Serial.print (ext==1,HEX);
  Serial.print (F(" Filter:"));
  Serial.print (filter,HEX);
  Serial.print (F(" Buffer:"));
  Serial.print (buffer,HEX);
  Serial.print (F(" Data:[0x"));
  for (int i=0;i<8;i++) {
    if (i>0){
      Serial.print (F(" 0x"));
    }
    Serial.print(frame_data[i],HEX);
  }
  Serial.println (F("]"));
} // end printBuf

void UsrToEEPROM (void){
  // JOYSTICK AND SPEED POT SETTINGS:
  ProgrammerUserSettings[0] = SpeedPotRevMin;
  ProgrammerUserSettings[1] = SpeedPotRevMax;
  ProgrammerUserSettings[2] = TurnPotFwdMin;
  ProgrammerUserSettings[3] = TurnPotRevMin;
  ProgrammerUserSettings[4] = TurnAtFullSpeed;
  ProgrammerUserSettings[5] = ThrottleCurving;
  ProgrammerUserSettings[6] = SteeringCurving;
  ProgrammerUserSettings[7] = Deadband;
  ProgrammerUserSettings[8] = Damping;
  ProgrammerUserSettings[9] = JswitchTrigger;
  ProgrammerUserSettings[10] = SpeedPotInstalled;
  ProgrammerUserSettings[11] = SpeedPot;
  ProgrammerUserSettings[12] = TurnPotFwdMax;
  ProgrammerUserSettings[13] = TurnPotRevMax;
  ProgrammerUserSettings[14] = SpeedPotFwdMin;
  ProgrammerUserSettings[15] = SpeedPotFwdMax;
  ProgrammerUserSettings[16] = SpeedPotLowFault;
  ProgrammerUserSettings[17] = SpeedPotHighFault;
  ProgrammerUserSettings[18] = 0; //obsolete SpeedPotFilter now in Defines.h
  // SWITCH DRIVING SETTINGS:
  ProgrammerUserSettings[19] = Drive1FwdSpeed;
  ProgrammerUserSettings[20] = Drive1RevSpeed;
  ProgrammerUserSettings[21] = Drive1FwdTurnRate;
  ProgrammerUserSettings[22] = Drive1RevTurnRate;
  ProgrammerUserSettings[23] = Drive2FwdSpeed;
  ProgrammerUserSettings[24] = Drive2RevSpeed;
  ProgrammerUserSettings[25] = Drive2FwdTurnRate;
  ProgrammerUserSettings[26] = Drive2RevTurnRate;
  ProgrammerUserSettings[27] = Drive3FwdSpeed;
  ProgrammerUserSettings[28] = Drive3RevSpeed;
  ProgrammerUserSettings[29] = Drive3FwdTurnRate;
  ProgrammerUserSettings[30] = Drive3RevTurnRate;
  ProgrammerUserSettings[31] = LoadBoostCurving;
  // CHAIR SETUP:
  ProgrammerUserSettings[32] = TimeToSleep;
  // SPEED REDUCTIONS
  ProgrammerUserSettings[33] = Slow1;
  ProgrammerUserSettings[34] = Slow2;
  ProgrammerUserSettings[35] = LimpSpeed;
  ProgrammerUserSettings[36] = 0; // obsolete AccelMix
  ProgrammerUserSettings[37] = 0; // obsolete LoadBoost_I
  ProgrammerUserSettings[38] = 0; // obsolete SpeedBoost
  ProgrammerUserSettings[39] = TurnBoost; // multiplied *10 in Roboteq script
  ProgrammerUserSettings[40] = 0; // obsolete MinPWM
  ProgrammerUserSettings[41] = BackStickBraking; // multiplied *10 in Roboteq script
  ProgrammerUserSettings[42] = LowIBoost;
  ProgrammerUserSettings[43] = 0; // obsolete TransitionPWM
  // Roboteq connections
  ProgrammerUserSettings[44] = UseCurrentSensors;
  ProgrammerUserSettings[45] = LeftSensorPin;
  ProgrammerUserSettings[46] = RightSensorPin;
  ProgrammerUserSettings[47] = LeftSensorReversePolarity;
  ProgrammerUserSettings[48] = RightSensorReversePolarity;
  ProgrammerUserSettings[49] = TiltForwardPin;
  ProgrammerUserSettings[50] = TiltBackPin;
  ProgrammerUserSettings[51] = LiftUpPin;
  ProgrammerUserSettings[52] = LiftDownPin;
  ProgrammerUserSettings[53] = Brake1Pin;
  ProgrammerUserSettings[54] = Brake2Pin;
  ProgrammerUserSettings[55] = ContactorPin;
  ProgrammerUserSettings[56] = SwapMotors;
  ProgrammerUserSettings[57] = Motor1ReversePolarity;
  ProgrammerUserSettings[58] = Motor2ReversePolarity;
  ProgrammerUserSettings[59] = LoadBoost_D;
  ProgrammerUserSettings[60] = CompDeadband;
  ProgrammerUserSettings[61] = CompTurnBoost;
  ProgrammerUserSettings[62] = LoadBoost_P;
  for (byte i = 63;i<=75;i++) {
    ProgrammerUserSettings[i] = 0; // unused
  }
// DISPLAY MESSAGING
  ProgrammerUserSettings[76] = highByte(VoltsFull);
  ProgrammerUserSettings[77] = lowByte(VoltsFull);
  ProgrammerUserSettings[78] = highByte(VoltsEmpty);
  ProgrammerUserSettings[79] = lowByte(VoltsEmpty);
  ProgrammerUserSettings[80] = highByte(AHrFull);
  ProgrammerUserSettings[81] = lowByte(AHrFull);
  ProgrammerUserSettings[82] = highByte(AHrEmpty);
  ProgrammerUserSettings[83] = lowByte(AHrEmpty);
// Roboteq SETTINGS
  ProgrammerUserSettings[84] = highByte(Accel);
  ProgrammerUserSettings[85] = lowByte(Accel);
  ProgrammerUserSettings[86] = highByte(Decel);
  ProgrammerUserSettings[87] = lowByte(Decel);
  ProgrammerUserSettings[88] = 0; // unused
  ProgrammerUserSettings[89] = 0; // unused
  ProgrammerUserSettings[90] = 0; // unused
  ProgrammerUserSettings[91] = 0; // unused
  ProgrammerUserSettings[92] = highByte(RechargeVolts);
  ProgrammerUserSettings[93] = lowByte(RechargeVolts);
  ProgrammerUserSettings[94] = highByte(LowVoltLimit);
  ProgrammerUserSettings[95] = lowByte(LowVoltLimit);
  ProgrammerUserSettings[96] = highByte(MotorResistance);
  ProgrammerUserSettings[97] = lowByte(MotorResistance);
  for (byte i = 0; i <= sizeof(ProgrammerUserSettings)-1; i++){
    EEPROM.update (UsrAddress+i, ProgrammerUserSettings[i]);
  }
}  // end UsrToEEPROM

void StartStop_Programmer (void){
  byte DATA[2];
  DATA[0] = StartStop;
  DATA[1] = ~DATA[0];
  // send message repeatedly to make sure it is received by Master
  for (byte i=0 ; i<=5 ; i++){
    SEND_MESSAGE (2, StartStop_id, DATA);
    delay (10);
  }
} // end StartStop_Programmer

void CheckNewMCU (void){
  GET_UsrSettings (); // retrieve MasterUserSettings from Master node
  Serial.println (F("Done getting User Settings from Master node"));
  Serial.println (F("**************************************************"));
  Serial.println ();
  if (NewMCU) {
    Serial.println (F("Master node has fresh or corrupted EEPROM"));
    Serial.println (F("*** sending UserSettings values from Programmer node ***"));
    // CODE IN MASTER WRITES Programmer node User Settings TO Master's EEPROM,
    // TO UserSetting VARIABLES, AND NewMCUFlag in EEPROM IS SET to 0b1010101
    SEND_UsrSettings ();
    Serial.println (F("Done sending Programmer User Settings to Master"));
    Serial.println (F("***********************************************"));
    Serial.println ();
  }
} // end CheckNewMCU

void CalibrateJoystick (void){
  // Calibrate joystick and speed pot
  static boolean CalibrationSkip = false;
  if (NotCalibrated){ // initial calibration not yet done
    for (byte i=1 ; i<=5 ; i++){
      SEND_MESSAGE (0, CkJS_id, NULL);
      delay (10);
    }
    do {
      RECEIVE_MESSAGE ();
    } while (!CkJSDone);
  }
  if (NotCalibrated){
    Serial.println (F("INITIAL JOYTICK CALIBRATION NOT YET DONE"));
    delay (200);
    RUN_Calibration ();
  }
  else { // initial calibration already run
    Serial.println (F("INITIAL JOYSTICK CALIBRATION ALREADY RUN"));
    if (!CalibrationSkip && !CalibrationDone){ // ensures that this option only appears once
      Serial.println (F("Do You Want to Re-Calibrate? Y/N "));
      do {
        delay (10);
      } while (!Serial.available());

      if (Serial.available ()){
        SerialInput = Serial.read ();
      }
      if ((SerialInput == 121)||(SerialInput == 89)){
        RUN_Calibration ();
      }
      else {
        CalibrationSkip = true;
        Serial.println (F(" ***** CALIBRATION SKIPPED *****"));
        Serial.println ();
      }
    }
  }
} // end CalibrateJoystick

void CalibrateVoltage (void) {
  char sVoltCalibration[5];
  int newVoltCalibration;
  boolean finished = false;
  do { // while (!finished): i.e. until no more change of calibration factor needed
    GET_VoltCalibration ();
    VoltCalibrationReceived = false;
    do {
      RECEIVE_MESSAGE ();
    } while (!VoltCalibrationReceived);
    VoltCalibrationReceived = true;
    Serial.print (F("Indicated deciVolts = "));
    Serial.print (deciVolts);
    Serial.print (F("   calibration factor = "));
    Serial.println (VoltCalibration);
    Serial.println (F("-------------------------------------------------------"));
    Serial.println (F("If adjustment is needed, enter a new calibration factor"));
    Serial.println (F("If correct, enter any non-numeric key"));
    Serial.println ();
    do {
      delay (10);
    } while (!Serial.available());
    for (byte i=0; i<=4; i++){ // fill calibration buffer with up to 4 digits + null character
      sVoltCalibration[i] = NULL;
      if (Serial.available ()){
        sVoltCalibration[i] = Serial.read ();
      }
    }// end filling sVoltCalibration
    newVoltCalibration = atoi (sVoltCalibration);
    if ((newVoltCalibration!=0) && (newVoltCalibration!=VoltCalibration)){
      VoltCalibration = newVoltCalibration;
      SEND_VoltCalibration ();
      GET_VoltCalibration ();
      VoltCalibrationReceived = false;
      do {
        RECEIVE_MESSAGE ();
      } while (!VoltCalibrationReceived);
      VoltCalibrationReceived = true;
    }
    else {
      finished = true;
      Serial.println (F("*********************************************"));
    }
  } while (!finished);
  Serial.println (F("end of CalibrateVoltage"));
} // end CalibrateVoltage

void CalibrateOffset (void){
  Serial.println ();
  Serial.println (F("Do You Want to Re-Calibrate contactor voltage offset? Y/N "));
  Serial.println (F(" ****** brakes will be OFF while doing this ******"));
  do {
    delay (10);
  } while (!Serial.available());
  if (Serial.available ()){
    SerialInput = Serial.read ();
  }
  if ((SerialInput == 121)||(SerialInput == 89)){
    OffsetCalibrationReceived = false;
    RUN_OffsetCalibration ();
    do {
      RECEIVE_MESSAGE ();
    } while (!OffsetCalibrationReceived);
    Serial.print (F("Contactor in vs. out voltage offset set to: "));
    Serial.println (Offset);
    Serial.println ();
  }
  else {
    Serial.println (F(" ***** OFFSET CALIBRATION SKIPPED *****"));
    Serial.println ();
  }
} // end CalibrateOffset ()

void RUN_OffsetCalibration (void){
  byte DATA[6];
  DATA[0] = 3;
  DATA[1] = 0;
  DATA[2] = 0;
  for (byte i=0; i<=2; i++){
    DATA[i+3] = ~DATA[i];
  }
  for (byte i=1 ; i<=5 ; i++){
    SEND_MESSAGE (6, VoltCalib_id, DATA);
    delay (10);
  }
} // end RUN_OffsetCalibration ()

void GET_VoltCalibration (void){
  byte DATA[6];
  DATA[0] = 1;
  DATA[1] = 0;
  DATA[2] = 0;
//  for (byte i=0; i<=3; i++){ // this is putting DATA[3] in DATA[6] which is out of range
  for (byte i=0; i<=2; i++){
    DATA[i+3] = ~DATA[i];
  }
  for (byte i=1 ; i<=5 ; i++){
    SEND_MESSAGE (6, VoltCalib_id, DATA);
    delay (10);
  }
} // end GET_VoltCalibration

void SEND_VoltCalibration (void) {
  byte DATA[6];
  DATA[0] = 2;
  DATA[1] = highByte (VoltCalibration);
  DATA[2] = lowByte (VoltCalibration);
//  for (byte i=0; i<=3; i++){ // this is putting DATA[3] in DATA[6] which is out of range
  for (byte i=0; i<=2; i++){
    DATA[i+3] = ~DATA[i];
  }
  for (byte i=1 ; i<=5 ; i++){
    SEND_MESSAGE (6, VoltCalib_id, DATA);
    delay (10);
  }
} // end SEND_VoltCalibration

void InitializeUserSettings (void){
  Serial.println ();
  // initialize MasterUserSettings
  static boolean InitializeSkip = false;
  if (!InitializeSkip&&!UsrInitDone){ // ensures that this option only appears once
    Serial.println (F("Replace all UserSettings in Master node if "));
    Serial.println (F("corrupted EEPROM is suspected"));
    Serial.println (F("*********************************************"));
    Serial.print (F("Reset all User Settings in Master node? Y/N "));
    do {
      delay (10);
    } while (!Serial.available());
    if (Serial.available ()){
      SerialInput = Serial.read ();
    }
    Serial.println ();
    if ((SerialInput == 121)||(SerialInput == 89)){
      Serial.println (F("*******************************************"));
      Serial.println (F("Re-writing Master User Settings"));
      SEND_UsrSettings ();
      Serial.println (F("All Master User Settings have been replaced"));
      Serial.println (F("*******************************************"));
      Serial.println ();
      // CODE IN MASTER WRITES Programmer node User Settings TO Master's EEPROM,
      // TO UserSettins VARIABLES, AND NewMCUFlag in EEPROM IS SET to 0b1010101
    }
    else {
      InitializeSkip = true;
      Serial.println (F(" ***** Initialization of Master User Settings SKIPPED *****"));
      Serial.println ();
      // check for differences between ProgrammerUserSettings and MasterUserSettings
      // and update if desired.
      Serial.println (F("*********************************************"));
      Serial.println (F("Comparing Master and Programmer User Settings"));
      Serial.println ();
      delay (100);
      COMPARE_Settings ();
    }
  } // if (!InitializeSkip && !UsrInitDone)
} // end InitializeUserSettings

void VerifyUserSettings (void){
  Serial.println (F("Verifying Master and Programmer User Settings"));
  delay (10);
  GET_UsrSettings ();
  boolean VerifyOK = VERIFY_Settings ();
  if (VerifyOK){
    Serial.println (F("Verify OK - no differences between Master and Programmer User Settings"));
  }
  else {
    Serial.println (F("***** USER SETTINGS DIFFERENT IN MASTER AND PROGRAMMER *****"));
    Serial.println ();
    Serial.println (F("If this was not intended, close serial monitor and re-open it"));
    Serial.println (F("            to re-start Programmer node"));
  }
  Serial.println (F("*********************************************"));
  Serial.println ();
} // end VerifyUserSettings

void ReadLogFiles (void){
  Serial.print (F("Read latest log file? (Y/N) "));
  do {
    delay (10);
  } while (!Serial.available());
  if (Serial.available ()){
    SerialInput = Serial.read ();
  }
  if ((SerialInput == 121)||(SerialInput == 89)){
    GET_LogFile (0,0);
  }
  else {
    Serial.println ();
  }
  Serial.println (F("*********************************************"));
  Serial.println ();
  Serial.println (F("Do you want to read an earlier log file? (Y/N) "));
  unsigned long LogStartTime = millis ();
  SerialInput = SerialTime (LogStartTime);
  if (Serial.available ()){
    SerialInput = Serial.read ();
  }
  if ((SerialInput == 121)||(SerialInput == 89)){
    boolean DateValid = false;
    boolean SkipLogFile = false;
    int Year, Month;
    char sMonth[4]; // 2-digit month + '/' + null character
    char sYear[5]; // 4-digit year + null character
    while (!DateValid){
      Serial.println (F("  Please enter file date as mm/yyyy, X to abort"));
      do {
        delay (10);
      } while (!Serial.available());
      for (byte i = 0; i<=2; i++){ // fill sMonth = 2 digits+'/'
        if (Serial.available ()){
          sMonth[i] = Serial.read ();
          if ((sMonth[i]=='X') || (sMonth[i]=='x')){
            Serial.println (F("Retrieving log file aborted"));
            Serial.println ();
            SkipLogFile = true;
            DateValid = true;
          } // X or x entered to abort reading log file
        }
      } // end filling sMonth[]
      for (byte i = 0; i<=3; i++){ // fill sYear
        if (Serial.available ()){
          sYear[i] = Serial.read ();
          if ((sYear[i]=='X') || (sYear[i]=='x')){
            Serial.println (F("Retrieving log file aborted"));
            Serial.println ();
            SkipLogFile = true;
            DateValid = true;
          } // X or x entered to abort reading log file
        }
      } // end filling sYear[]
      if (!SkipLogFile){
        Month = atoi (sMonth);
        Year = atoi (sYear);
        if (((Month>12)||(Month<1)) ||((Year>=2100)||(Year<0))){
          Serial.println (F("Date not valid: please try again"));
        }
        else {
          DateValid = true;
        }
      }
    } // end while (!DateValid)
    if (!SkipLogFile){
      Serial.print (F("Reading "));
      Serial.print (Month);
      Serial.print (F("/"));
      Serial.print (Year);
      Serial.println (F(" log file"));
      GET_LogFile (Month,Year);
    }
  } // y or Y was entered to read another log file
  else { // key !y and !Y entered
    Serial.println ();
    StartStop = false;
    StartStop_Programmer (); // exits blocking READ_MESSAGE loop in Master
                         // so that it reenters its normal Loop ().
    delay (500);
  }
} // end ReadLogFiles

void RUN_Calibration (void){
  Serial.println (F(" ***** JOYSTICK AND SPEEDPOT CALIBRATION BEGUN *****"));
  Serial.println ();
  SEND_MESSAGE (0, Calibration_id, NULL);
  Serial.println (F("Leave Joystick in neutral and press Mode to start center calibration"));
  delay (10);
  if (!NotCalibrated){
    Serial.println (F("Press Forward switch to skip this step"));
  }
  do {
    RECEIVE_MESSAGE ();
  } while (!CalibrationDone);
} // end RUN_Calibration

void SEND_UsrSettings (void){
  byte DATA [4];
  // send array of ProgrammerUserSettings to Master
  for (byte i = 0; i<= sizeof(ProgrammerUserSettings)-1; i++){
    DATA[0] = i;
    DATA[1] = ProgrammerUserSettings[i];
    DATA[2] = ~DATA[0];
    DATA[3] = ~DATA[1];
    SEND_MESSAGE (4, Usr_id, DATA);
    delay (10);
    if (i==0){ // send first setting a second time as it also
      // serves to start receiving loop in Master
      SEND_MESSAGE (4, Usr_id, DATA);
      delay (5);
    }
  }
  // send an extra message to tell master that all have been sent
  // repeat several times to make sure message gets through
  DATA[0] = sizeof (ProgrammerUserSettings);
  DATA[1] = 0b1010101; // placed in EEPROM(NewMCUFlag) in Master
  DATA[2] = ~DATA[0];
  DATA[3] = ~DATA[1];
  for (byte i=1 ; i<=4 ; i++){
    SEND_MESSAGE (4, Usr_id, DATA);
    delay (15);
  }
  UsrInitDone = true;
} // end SEND_UsrSettings

void GET_UsrSettings (void){
  byte DATA[2];
  UsrSettingsDone = false;
  Serial.println (F(" ***** Reading Master node User Settings *****"));
  // trigger Master to start sending User Settings array
  DATA[0] = 1;
  DATA[1] = ~DATA[0];
  // send message to Master to initiate sending data
  SEND_MESSAGE (2, UsrSettings_id, DATA);
  delay (10);
  do { // this will block until entire array has been retrieved
    // as Usr_ret messages.
    RECEIVE_MESSAGE ();
  } while (!UsrSettingsDone);
} // end GET_UsrSettings

void COMPARE_Settings (void){
  byte DATA [4];
  boolean DifferenceFound = false;
  // single byte User Settings
  for (byte i = 0; i <= 75; i++){
// uncomment following section to see entire list of user settings
/*
    strcpy_P(LabelBuffer, (char*)pgm_read_word(&(UsrSettingLabels[i])));
    Serial.print(i);
    Serial.print (F(": "));
    Serial.print (LabelBuffer);
    Serial.print (F(": Master = "));
    Serial.print (MasterUserSettings[i]);
    Serial.print (F("   Programmer = "));
    Serial.println (ProgrammerUserSettings[i]);
*/
    if (MasterUserSettings[i] != ProgrammerUserSettings[i]){
      if (!DifferenceFound){
        Serial.println (F("The following User Settings are different in Master and Programmer"));
        Serial.println (F("__________________________________________________________________"));
        DifferenceFound = true;
      }
      strcpy_P(LabelBuffer, (char*)pgm_read_word(&(UsrSettingLabels[i])));
      Serial.print (LabelBuffer);
      Serial.print (F(": Master = "));
      Serial.print (MasterUserSettings[i]);
      Serial.print (F("   Programmer = "));
      Serial.println (ProgrammerUserSettings[i]);
      Serial.print (F("   Update Master to new value? (Y/N) "));
      do {
        delay (10);
      } while (!Serial.available());
      if (Serial.available ()){
        SerialInput = Serial.read ();
      }
      if ((SerialInput == 121)||(SerialInput == 89)){
        // send this Programmer user setting to Master
        DATA[0] = i;
        DATA[1] = ProgrammerUserSettings[i];
        DATA[2] = ~DATA[0];
        DATA[3] = ~DATA[1];
        SEND_MESSAGE (4, Usr_id, DATA);
        delay (1);
        Serial.println (F("-- updated"));
      }
      else {
        Serial.println (F("-- update skipped"));
      }
    }
  } // end for (byte i = 0; i <= 37; i++)
  // two byte User Settings
  for (byte i = 76; i <= sizeof(MasterUserSettings)-2; i = i+2){
// uncomment following section to see entire list of user settings
/*
    strcpy_P(LabelBuffer, (char*)pgm_read_word(&(UsrSettingLabels[i])));
    Serial.print(i);
    Serial.print (F(": "));
    Serial.print (LabelBuffer);
    Serial.print (F(": Master = "));
    Serial.print ((unsigned int)MasterUserSettings[i]<<8|MasterUserSettings[i+1]);
    Serial.print (F("   Programmer = "));
    Serial.println ((unsigned int)ProgrammerUserSettings[i]<<8|ProgrammerUserSettings[i+1]);
*/
    if ((MasterUserSettings[i]!=ProgrammerUserSettings[i])||(MasterUserSettings[i+1]!=ProgrammerUserSettings[i+1])){
      if (!DifferenceFound){
        Serial.println (F("The following User Settings are different in Master and Programmer"));
        Serial.println (F("__________________________________________________________________"));
        DifferenceFound = true;
      }
      strcpy_P(LabelBuffer, (char*)pgm_read_word(&(UsrSettingLabels[i])));
      Serial.print (LabelBuffer);
      Serial.print (F(": Master = "));
      Serial.print ((unsigned int)MasterUserSettings[i]<<8|MasterUserSettings[i+1]);
      Serial.print (F("   Programmer = "));
      Serial.println ((unsigned int)ProgrammerUserSettings[i]<<8|ProgrammerUserSettings[i+1]);
      Serial.print (F("   Update Master to new value? (Y/N) "));
      do {
        delay (10);
      } while (!Serial.available());
      if (Serial.available ()){
        SerialInput = Serial.read ();
      }
      if ((SerialInput == 121)||(SerialInput == 89)){
        // send this Programmer user setting to Master
        DATA[0] = i;
        DATA[1] = ProgrammerUserSettings[i];
        DATA[2] = ~DATA[0];
        DATA[3] = ~DATA[1];
        SEND_MESSAGE (4, Usr_id, DATA);
        delay (1);
        DATA[0] = i+1;
        DATA[1] = ProgrammerUserSettings[i+1];
        DATA[2] = ~DATA[0];
        DATA[3] = ~DATA[1];
        SEND_MESSAGE (4, Usr_id, DATA);
        delay (1);
        Serial.println (F("-- updated"));
      }
      else {
        Serial.println (F("-- update skipped"));
      }
    }
  } // end for (byte i = 38; i <= sizeof(MasterUserSettings)-2; i = i+2)
  if (!DifferenceFound){
    Serial.println (F("No differences found between Master and Programmer User Settings"));
  }
  Serial.println (F("******************************************************************"));
  Serial.println ();
} // end COMPARE_Settings

void GET_LogFile (int Month, int Year){
  byte DATA[8];
  LogFileDone = false;
  if ((Month == 0) && (Year == 0)){
    Serial.println ();
    Serial.println (F("***** Reading Latest Log File *****"));
  }
  // trigger Display to start sending LogFile contents
  DATA[0] = 1;
  DATA[1] = Month;
  DATA[2] = highByte(Year);
  DATA[3] = lowByte(Year);
  for (byte i = 0; i<=3; i++){
    DATA[i+4] =  ~DATA[i];
  }
  // send message to Display to initiate sending data
  SEND_MESSAGE (8, GetLogFile_id, DATA);
  delay (10);
  do { // this will block until entire array has been retrieved
    // as Usr_ret messages.
    RECEIVE_MESSAGE ();
  } while (!LogFileDone);
} // end GET_LogFile

boolean VERIFY_Settings (void){
  boolean VerifyOK = true;
  // single byte User Settings
  for (byte i = 0; i <= sizeof(MasterUserSettings)-1; i++){
    if (MasterUserSettings[i] != ProgrammerUserSettings[i]){
      VerifyOK = false;
      break;
    }
  }
  return VerifyOK;
} // end VERIFY_Settings

char SerialTime (unsigned long StartTime){
  char result;
  int interval;
  do {
    interval = (millis () - StartTime);
    if (interval%1000 == 0){
     Serial.print (F(" ."));
     delay (1);
    }
    if (interval >= 10000){
      Serial.println ();
      result = 'n';
      break;
    }
  } while (!Serial.available());
  return result;
}

void Check_Ranges (void) {
  for (byte i = 0; i <= sizeof(ProgrammerUserSettings) - 1; i++) {
    UsrValue = ProgrammerUserSettings[i];
    if ((i==0) || (i==2) || (i==3) || (i==14)) {
    // SpeedPotRevMin, TurnPotFwdMin, TurnPotRevMin, SpeedPotFwdMin
      if ((UsrValue >= 80) || (UsrValue == 0)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if (i==1) { // SpeedPotRevMax
        if ((UsrValue > 100) || (UsrValue < ProgrammerUserSettings[0])) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if (i==4) { // TurnAtFullSpeed
      if ((UsrValue <= 20) || (UsrValue > 100)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if ((i==5) || (i==6) || (i==31)) {
  // ThrottleCurving, SteeringCurving, LoadBoostCurving
      if ((UsrValue > 4) || (UsrValue < 0)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if (i==7) { // Deadband
      if ((UsrValue >= 40) || (UsrValue < 0)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if (i==8) { // Damping
      if ((UsrValue == 0) || (UsrValue > 20)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if (i==9) { // JswitchTrigger
      if ((UsrValue < 10) || (UsrValue > 80)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
else if ((i==10) || (i==44) || (i==47) || (i==48) || ((i>=56 ) && (i<=58))){ 
// SpeedPotInstalled, UseCurrentSensors, LeftSensorReversePolarity, RightSensorReversePolarity
// SwapMotors, Motor1ReversePolarity, Motor2ReversePolarity
      if ((UsrValue != 0) && (UsrValue != 1)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
        else if (i==11) { // SpeedPot
      if ((UsrValue < 40) || (UsrValue > 100)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if (i==12) { // TurnPotFwdMax
      if ((UsrValue > 100) || (UsrValue < ProgrammerUserSettings[2])) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if (i==13) { // TurnPotRevMax
      if ((UsrValue > 100) || (UsrValue < ProgrammerUserSettings[3])) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if (i==15) { // SpeedPotFwdMax
      if ((UsrValue > 100) || (UsrValue < ProgrammerUserSettings[14])) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if ((i==16) || (i==17)) { // SpeedPotLowFault, SpeedPotHighFault
      if ((UsrValue > 100) || (UsrValue < 0)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if ((i>=19) && (i<=30)) { // switch driving settings
      if ((UsrValue <= 0) || (UsrValue > 100)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if (i==32) { // TimeToSleep
      if ((UsrValue <= 0) || (UsrValue >= 150)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if ((i>=33) && (i<=35)) { // Slow1,Slow2,LimpSpeed
      if ((UsrValue > 100) || (UsrValue < 0)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
      else if (i==62){ // LoadBoost_P
        if ((UsrValue > 100) || (UsrValue < 0)) {
          UsrFaultPrint (i);
          UsrFault = true;
        }
      }
      else if (i==41) { // BackStickBraking
        if ((UsrValue > 15) || (UsrValue < 0)) {
          UsrFaultPrint (i);
          UsrFault = true;
        }
      }
      else if (i==61) { // CompTurnBoost
        if ((UsrValue > 200) || (UsrValue < 0)) {
          UsrFaultPrint (i);
          UsrFault = true;
        }
      }
    else if (i==39){ // TurnBoost
      if ((UsrValue > 20) || (UsrValue < 1)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if (i==42){ // LowIBoost
      if ((UsrValue > 100) || (UsrValue < 0)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if (i==61){ // CompTurnBoost
      if ((UsrValue > 200) || (UsrValue < 0)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if (i==59){ // LoadBoost_D
      if ((UsrValue > 255) || (UsrValue < 0)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if (i==60) { // CompDeadband
      if ((UsrValue > 40) || (UsrValue < 0)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if ((i>=49) && (i<=55)){ // Roboteq pin assignments
      if ((UsrValue == 0) || (UsrValue >= 15)) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if ((i==76) || (i==78) || (i==92) || (i==94) || (i==82) || (i==96)) {
// highByte VoltsFull, VoltsEmpty, RechargeVolts, LowVoltLimit, AHrEmpty, MotorResistance
      if (UsrValue > 0b10) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if (i==80) { // highByte(AHrFull)
      if (UsrValue > 0b100) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
    else if ((i==84) || (i==86)) { // highByte(Accel),highByte(Decel)
      if (UsrValue > 0b100111) {
        UsrFaultPrint (i);
        UsrFault = true;
      }
    }
  } // end for (byte i = 0; i <= sizeof(ProgrammerProgrammerUserSettings)-1; i++)
  if (UsrFault){ // use Default user settings if 30 values changed
                    // or absurd value in an altered parameter
    Serial.println (F("***********************************************************"));
    Serial.println (F("Correct out of range settings and re-load before proceeding"));
    Serial.println (F("***********************************************************"));
  }
} // end Check_Ranges

void UsrFaultPrint (byte i) {
  Serial.print (F("UserSetting "));
  Serial.print (i);
  Serial.print (F(" = "));
  strcpy_P(LabelBuffer, (char*)pgm_read_word(&(UsrSettingLabels[i])));
  Serial.print (LabelBuffer);
  Serial.println (F("  OUT OF RANGE"));
} // end UsrFaultPrint





