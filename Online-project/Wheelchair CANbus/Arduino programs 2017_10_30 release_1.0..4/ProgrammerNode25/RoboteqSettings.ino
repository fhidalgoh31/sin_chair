void RoboteqSettings (){

// ********************************************************************************
// USER SETTINGS FOR Roboteq MOTOR CONTROLLER
// to be sure that these take effect, turn system off and then back on

// See MasterSettings tab settings for all other Wheelchair CANbus nodes
// ********************************************************************************

// ACCELERATION AND DECELERATION SETTINGS
/* Base Acceleration and Deceleration rates are set with the Accel and Decel parameters.  A value of 2000
(internally treated as 200.0 in the Roboteq controller) will give an accel/decel of 200 RPM change per
second (for brushless motors) or 20% of PWM change per second (for brushed motors). */
Accel = 2100; // max = 10239
Decel = 3100; // max = 10239

/*  Acceleration and Deceleration rates are dynamically varied to accomplish three things:
/* (1) When simultaneously changing speed and turn rate, accel/decel is boosted to give a snappy turn, 
and speed is changed in increments so fore-aft acceleration of the whole vehicle is not boosted.  TurnBoost
sets how many times faster the turn rate changes. 2 gives adequate mixing, 3 is a bit snappier and still pleasant,
use over 3 for sport */
TurnBoost = 2; // range = 1 - 20 (tested through 20 - should be more than enough even for hockey)

/* (2) (NOT needed for brushless) accel/decel are made more proportionate to actual RPM changes than the PWM change 
per unit time that Roboteq uses for brushed motors.  The algorithm uses motor current (MotorComp values)and change
of motor current to do this. Four parameters are provided to give flexibility in setting this  for very different
chairs/motor/weights/voltages etc.  However, so far only two of these (LoadBoost_D and LoadBoostCurving)
have proven to be needed. */
// P term of PID
LoadBoost_P = 0; // accel/decel boosted proportionate to motor current range 0 - 100
//                  Causes load-boosted acceleration to be stronger and last longer
// *************
// *************
// I term of PID
// LoadBoost_I = 0; // moving average of last 'n' MotorComp values, range 0 - 100, but use very small values
//   Causes boosted acceleration to last longer
//   Obsolete, no longer in Roboteq script
// *************
// *************
// D term of PID
LoadBoost_D =0;
//   increases accel/decel in proportion to change in motor current, range = 0 - 255
// *************
LoadBoostCurving = 4; // accepts values of 0=linear, 1, 2, 3, 4=exponential
//   exponential curving makes accel/decel boost disappear more quickly as load decreases

/* (3) For panic stops, BackStickBraking allows for stronger accel/decel if the stick is moved from forward of neutral 
to aft of neutral. */
BackStickBraking = 5; // increases braking if stick moved past 0, range = 0 - 15, 
                      // multiplied by 10 in Roboteq so 15 = Decdel+150%
/* A user seated in the chair would probably want a relatively gentle base 
deceleration setting and a relatively large value for BackStickBraking (e.g.15).
Someone walking alongside and using an attendant joystick would want a stronger 
base deceleration and less BackStickBraking (e.g. 5).  Values much greater 
than 15 will almost immediately lock the wheels, smoking the tires and causing a skid. */

// MOTOR COMPENSATION SETTINGS
/* Sensor-measured or Roboteq-estimated motor currents are used to compensate for increased back EMF when 
loading increases.  The main setting is MotorResistance which should be set somewhat lower than 
measured motor resistance (in milliohms) to avoid this positive feedback causing instability (or 
even a runaway condition).  CompDeadband sets a small deadband to avoid movement if the chair
is jostled or there is joystick noise while not moving. */
MotorResistance = 50; // milliohms, range = 0 - 767
CompDeadband = 20; // range 0 - 50, 20 = 2% of 100% joystick output
/* Because heavy loading of casters on some chairs can make it difficult to 
start a turn when not moving, CompTurnBoost provides extra compensation in 
that situation.  This extra boost disappears as soon as current drops when 
the caster starts to swivel.  The effect of this boost also decreases as 
fore-aft power increases, and becomes zero at 5% fore-aft PWM which is enough 
fore-aft movement to straighten even heavily-loaded, difficult to swivel casters.*/
CompTurnBoost = 0; // 100 range 0 - 200, added milliohms MotorResistance for turn in place

/* USING Roboteq-ESTIMATED MOTOR CURRENTS INSTEAD OF CURRENT SENSORS:
If not using motor current sensors, motor compensation relies on
motor current estimated from battery current.  The script has some
improvements of this estimation that requires setting one parameter
here, and one (for now) in the Roboteq script itself.

The Roboteq reads battery current in discrete steps - for the HDC2450 this 
is 0.5 Amps  (as this is characteristic of the controller and never needs "tuning"
it is set as MinI in the Roboteq script).  To get motor compensation at low 
PWM if battery current = 0, the program assumes that battery current is 
proportionate to PWM up the PWM at which a stalled motor draws MinI battery 
current.  The result of this is a constant value of assumed motor current 
at very low power.  This is set with LowIBoost. */
LowIBoost = 10; // 40 for Rachi, 23 for mobot
          // Range = 0 to 100
/* Roboteq controllers also increasingly underestimate motor current as PWM 
falls below 20%.  The Roboteq script extends accurate estimation down to PWM = 5%. */
// *************

LimpSpeed = 80; // 50 Roboteq precentage of Throttle and Turn if motor disconnect detected range = 1 - 100
                // OBSOLETE - cannot use motor disconnect without current sensors
// Roboteq connections
UseCurrentSensors = YES;

 LeftSensorPin = 5; //analog in
 RightSensorPin = 6; //analog in

 LeftSensorReversePolarity = YES;
 RightSensorReversePolarity = YES;

TiltForwardPin = 5; // digital out
TiltBackPin = 4; // digital out

LiftUpPin = 1; // digital out
LiftDownPin = 2; // digital out

Brake1Pin = 7; // digital out
Brake2Pin = 8; // digital out

ContactorPin = 3; // digital out
SwapMotors = NO;
Motor1ReversePolarity = NO;
Motor2ReversePolarity = NO;

}






