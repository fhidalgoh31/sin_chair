void MasterSettings (){

// USER SETTINGS FOR CANbus NODES
// See RoboteqSettings tab for Motor Controller settings
// ********************************************************************************
// general settings
// ********************************************************************************
TimeToSleep = 3 ; // no. of 10s intervals without sending command until sleep.; 60 = 10 min
                  // 0 = never go to sleep range = 1 - 149
                  // 3 = 30 sec is useful for testing, 60 = 10 min, for example, is 
                  // better for real use
// ********************************************************************************
// for joystick driving
// ********************************************************************************
ThrottleCurving = 0; // 0 exponential: 0=none, 1=low, 2=mild, 3=mid, 4=strong
SteeringCurving = 0; // exponential: 0=none, 1=low, 2=mild, 3=mid, 4=strong
Deadband = 5; // 10 % of stick throw; use larger number if poor hand control,
                    // 0 = no deadband and will jitter with no joystick input
                    // max = 39
Damping = 5; // 3 5 10 8 number of joystick reads to include in moving average,
                  // use larger number if tremor (range = 1 to 20; at 20 movement will
                  // continue after releasing sick)
// for controlling lights/seat from joystick
JswitchTrigger = 40; // range = 10 - 80, percent of JS throw needed to emulate a switch,
// if less than 60 there's sometimes a transient signal if js is "popped"
// ********************************************************************************
// for speed pot
// ********************************************************************************
SpeedPotInstalled = YES; // set to NO if not using a speed pot,
SpeedPot = 90; //default if SpeedPot not installed; 90 allows some headroom for steer/mixing, range = 40 - 100
// speed pot and turn pot settings adjusted for 4.80 X 8 wheels: 30% larger diameter
SpeedPotFwdMax = 100; //percent speed at highest pot setting, set <100 for lower top
                       // speed or if headroom needed for turning, range = SpeedPotFwdMin - 100
SpeedPotFwdMin = 27; // 35, 30, range = 1 - 79
SpeedPotRevMax = 46; // 60, 70, range = SpeedPotRevMin - 100
SpeedPotRevMin = 19; // 25, 20 range = 1 - 79
TurnPotFwdMax = 17; // 14 18 22 28 range = TurnPotFwdMin - 100
TurnPotFwdMin = 12; // 13 15 12 16 18 14 range = 1 - 79
TurnPotRevMax = 16; // 12 15 20 24 range = TurnPotRevMin - 100
TurnPotRevMin = 11; // 13 9 11 10 12 7 range = 1 - 79
TurnAtFullSpeed = 80; // 100=no reduction of turn rate with speed, 0=turn rate goes to 0 at full speed, range = 20 - 100
//                        particularly useful for front wheel drive chairs
SpeedPotLowFault = 20; // chair speed used if pot fails giving too low voltage, max = 100
SpeedPotHighFault = 70; // chair speed used if pot fails giving too high voltage, max = 100
// ********************************************************************************
// for switch driving
// ********************************************************************************
Drive1FwdSpeed = 10; // range = 1 - 100
Drive1RevSpeed = 7; // range = 1 - 100
Drive1FwdTurnRate = 12; // range = 1 - 100
Drive1RevTurnRate = 10; // range = 1 - 100
Drive2FwdSpeed = 20; // range = 1 - 100
Drive2RevSpeed = 15; // range = 1 - 100
Drive2FwdTurnRate = 16; // range = 1 - 100
Drive2RevTurnRate = 14; // range = 1 - 100
Drive3FwdSpeed = 30;  // range = 1 - 100
Drive3RevSpeed = 20; // range = 1 - 100
Drive3FwdTurnRate = 18; // range = 1 - 100
Drive3RevTurnRate = 15; // range = 1 - 100
// ********************************************************************************
// speed reductions, if wanted, for different actuator positions, or for shock/stability sensor
// ********************************************************************************
Slow1 = 40; // percentage max speed for strong slowdown inhibit // range = 1 - 100
Slow2 = 80; // percentage max speed for weak slowdown inhibit //  range = 1 - 100
// ********************************************************************************
// battery settings
// ********************************************************************************
VoltsFull = 250; // Odyssey 100% SOC = message value 100, max = 767
VoltsEmpty = 234; // Odyssey 10% SOC = message value 0 max = 511
RechargeVolts = 258; // value > this implies that battery has just been taken off charger  max = 767
LowVoltLimit = 236; // 80% DOD for Odyssey, reduce speed and set warning on Display  max = 511
AHrFull = 570; // tenths of AHr: Odyssey PC1500 5hr rate = AHr remaining message 100 max = 1279
AHrEmpty = 57; // tenths of AHr: Odyssey 10% of full = AHr remaining message 0 max = 511

// ******************* SEE RoboteqSettings tab for Motor Controller settings *******************

}





