#ifndef __Globals__
#define __Globals__

boolean SystemOn = false;
unsigned long StartTime;
boolean MasterAwake = false;
boolean ProgrammerRestart = false;
byte MasterUserSettings[98]; // byte-long entries for user settings
                             // NOTE: some settings are uint_16t and stored in 2 bytes
byte ProgrammerUserSettings[sizeof(MasterUserSettings)]; // byte-long entries for user settings
byte UsrValue;
boolean UsrFault = false;
boolean NewMCU = false; // NewMCU = true if Master UserSettings EEPROM not yet written
boolean NotCalibrated = true;
boolean CkJSDone = false;
boolean StartStop = true;
boolean VoltCalibrationReceived = false;
boolean OffsetCalibrationReceived = false;
int Throttle0 = 0; // millivolts
int Steering0 = 0; // millivolts
int ThrottleFwd = 0;
int ThrottleRev = 0;
int SteeringLft = 0;
int SteeringRt = 0;
int PotMax = 0;
int PotMin = 0;
int deciVolts = 0;
int VoltCalibration;
int Offset;
unsigned long ReceiveFrame_id;
unsigned long SendFrame_id;
byte length,rx_status,filter,ext,Category,Name,Target;
byte frame_data[8]; // this is a buffer, frame may have fewer bytes
boolean MsgReceived;
boolean NoPrinting = true; // change to false to turn on diagnostic Serial.print lines
boolean CalibrationDone = false;
boolean UsrSettingsDone = false;
boolean UsrInitDone = false;
boolean LogFileDone = false;
char SerialInput = '0';

// ********************************************
// declarations of types for UserSettings
// ********************************************
byte TimeToSleep; // UserSettings[32]
byte ThrottleCurving; // UserSettings[5] 
byte SteeringCurving; // UserSettings[6]
byte Deadband; // UserSettings[7]
byte Damping; // UserSettings[8]
byte JswitchTrigger; // UserSettings[9]
byte SpeedPotInstalled; // UserSettings[10]
byte SpeedPot; // UserSettings[11]
byte SpeedPotFwdMax; // UserSettings[15]
byte SpeedPotFwdMin; // UserSettings[14]
byte SpeedPotRevMax; // UserSettings[1]
byte SpeedPotRevMin; // UserSettings[0]
byte TurnPotFwdMax; // UserSettings[12]
byte TurnPotFwdMin; // UserSettings[2]
byte TurnPotRevMax; // UserSettings[13]
byte TurnPotRevMin; // UserSettings[3]
byte TurnAtFullSpeed; // UserSettings[4]
byte SpeedPotLowFault; // UserSettings[16]
byte SpeedPotHighFault; // UserSettings[17]
byte Drive1FwdSpeed; // UserSettings[19]
byte Drive1RevSpeed; // UserSettings[20]
byte Drive1FwdTurnRate; // UserSettings[21]
byte Drive1RevTurnRate; // UserSettings[22]
byte Drive2FwdSpeed; // UserSettings[23]
byte Drive2RevSpeed; // UserSettings[24]
byte Drive2FwdTurnRate; // UserSettings[25]
byte Drive2RevTurnRate; // UserSettings[26]
byte Drive3FwdSpeed; // UserSettings[27]
byte Drive3RevSpeed; // UserSettings[28]
byte Drive3FwdTurnRate; // UserSettings[29]
byte Drive3RevTurnRate; // UserSettings[30]
byte Slow1; // UserSettings[33]
byte Slow2; // UserSettings[34]
unsigned int VoltsFull;  // UserSettings[76-77]
unsigned int VoltsEmpty; // UserSettings[78-79]
unsigned int RechargeVolts; // UserSettings[92-93]
unsigned int LowVoltLimit; // UserSettings[94-95]
unsigned int AHrFull; // UserSettings[80-81]
unsigned int AHrEmpty; // UserSettings[82-83]
// acceleration and deceleration settings
unsigned int Accel; // UserSettings[84-85]
unsigned int Decel; // UserSettings[86-87]
byte TurnBoost; // UserSettings[39]
byte BackStickBraking; // UserSettings[41]
// MotorCompensation settings
unsigned int MotorResistance; // UserSettings[96-97]
byte CompTurnBoost; // UserSettings[61]
byte CompDeadband; // UserSettings[60]
// estimating motor current at low PWM and battery current < MinPWM
// byte MinPWM; // UserSettings[40] OBSOLETE
byte LowIBoost; // UserSettings[42]
// byte TransitionPWM; // UserSettings[43] OBSOLETE
// *******************
// acceleration linearization settings
// P term of PID
byte LoadBoost_P; // UserSettings[62]
// *******************
// *******************
// I term of PID
// byte LoadBoost_I; // UserSettings[37] OBSOLETE
// *******************
// *******************
// D term of PID
byte LoadBoost_D; // UserSettings[59]
// *******************
byte LoadBoostCurving; // UserSettings[31]
byte LimpSpeed; // UserSettings[35]
byte UseCurrentSensors; // UserSettings[44]
byte LeftSensorPin; // UserSettings[45]
byte RightSensorPin; // UserSettings[46]
byte LeftSensorReversePolarity; // UserSettings[47]
byte RightSensorReversePolarity; // UserSettings[48]
byte TiltForwardPin; // UserSettings[49]
byte TiltBackPin; // UserSettings[50]
byte LiftUpPin; // UserSettings[51]
byte LiftDownPin; // UserSettings[52]
byte Brake1Pin; // UserSettings[53]
byte Brake2Pin; // UserSettings[54]
byte ContactorPin; // UserSettings[55]
byte SwapMotors; // UserSettings[56]
byte Motor1ReversePolarity; // UserSettings[57]
byte Motor2ReversePolarity; // UserSettings[58]

#endif






