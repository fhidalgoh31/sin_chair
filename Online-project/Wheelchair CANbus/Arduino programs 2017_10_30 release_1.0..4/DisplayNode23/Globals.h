#ifndef __Globals__
#define __Globals__

unsigned long ReceiveFrame_id;
// unsigned long SendFrame_id;
byte length,rx_status,filter,ext,Category,Name,Target;
byte frame_data[8]; // this is a buffer, frame may have fewer bytes
boolean MsgReceived;
boolean XcvrSleep = false;
byte DisplayStart[2];

// DECLARE HERE BUT INITIALIZE IN SETUP AFTER ROTATING SCREEN
// for placement of graphics
int CtrX, CtrY;
// for Speed Pot graphic
int PtrTriangleApexRadius, PtrTriangleBase, PtrTriangleBaseRadius;
byte LastSpeedPot = 50; // to reset after repainting screen
byte OONflag = 0; // 0=erase, 1=Joystick OON, 2=Switch OON
boolean WaitForPaint = false;

byte Mode = None;
byte LastMode = None;
// set LastMode = None to force re-paint of base image
byte UsrToggle = None;
byte LastUsrToggle = None;
int RunHours = 0;
byte RunMinutes = 0;
int TotalHours = 0;
byte TotalMinutes = 0;

// variables for SpeedPot graphic
int TrApX, TrApY, TrBaseX, TrBaseY;
int TrLtX, TrLtY, TrRtX, TrRtY;
// variables for Volt and AHr meters
int Volts = 100;
int LastVolts = 100;
int AHr = 100;
int LastAHr = 100;
// variables for painting inhibit or slow symbols
byte SetSlow = 3;
byte LastSetSlow = 3;
// variables for Joystick marker
byte Throttle = 0;
byte Steering = 0;
boolean StartupLogged = false;

int InVolts;
int OutVolts;

SdFile logFile;
SdFile msgFile;

#endif





