*.sch and *.pcb files are in DesignSpark format.  Some of them will not
open in versions of DesignSpark older than V8.

*.pcb designs include a copper pour area for ground, but for readability that area does
not have the copper poured.  Remember to pour the copper before creating gerber files.

Two versions of the basic CAN board are included.  One includes four LED and associated
resistors to show input and output activity.  The other does not have these, and has components
on only one side of the board.  The LEDs are of limited diagnostic value (see manual) so 
can be omitted on the board that includes the pads, or you can use the board design that doesn't
have them at al.

The hi-side switch is used to PWM control TFT screen backlight. Original connection to
Vdd is interrupted to install this and components are mounted either on a tiny scrap board
or glued directly to the TFT board and then hand wired.  The main purpose of this
is to let the Aux node dim the screen when the system is in sleep.